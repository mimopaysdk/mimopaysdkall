package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: ForestIndoNiaga
	// stagingEncryptedText: 1QvNRZ7WLFR/FHA4NVf96Q==
	public static String msKeyValueStaging = "IzYvTC52HkNFVTRSXEoPJFlRYCRyKiJtakAkLW5CeTk=";
	// gatewayEncryptedText: BBSZFxHxpmoSXj6ucFohaw==
	public static String msKeyValueGateway = "GlZKe29TBntLS0F7MS9eEHt0dUNfSBUdIxFWeAN7IHs=";
}
