package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: TopFun_1
	// stagingEncryptedText: C5QRmfLEnm43K5AlaUC11g==
	public static String msKeyValueStaging = "OEt+Ai0FUQ1ze3FsdggqRxtaPGp2P3QvYQIiEFMoTwQ=";
	// gatewayEncryptedText: 0roOvTVV31pAKfkfff6Olg==
	public static String msKeyValueGateway = "dAYWHB1DDRAOYl8TOztBGA8cTW4kH14OMAJNHBhvVVk=";
}
