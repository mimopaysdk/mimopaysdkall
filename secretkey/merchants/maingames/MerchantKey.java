package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: maingames
	// stagingEncryptedText: 1/zw0T/wYs3bFTFmKPaYIA==
	public static String msKeyValueStaging = "WiVcNAgQUAZXK38RIzcfO14qGXIKFUBAZSsKTzF0DnM=";
	// gatewayEncryptedText: ljmNEEG9cnO2Ucyuq5ElsA==
	public static String msKeyValueGateway = "YEJPWG9jcU05P00eJmIpFQ8KYyYVfyQIXkMWCRpPLXQ=";
}
