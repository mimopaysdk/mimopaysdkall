package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: PT_Wing
	// stagingEncryptedText: GL/5u04U6WaD9B8eyW6JjA==
	public static String msKeyValueStaging = "Hm9TcVxUNUNhYWJuF2wFDFcsAW5gJ1ocHWofWycyfX4=";
	// gatewayEncryptedText: QXnfcgpvSEY+LdpS7MvH8Q==
	public static String msKeyValueGateway = "OXRaGxpdCRU3PiplE2NbZDksOEUoSyloZF5RRgE6eVk=";
}
