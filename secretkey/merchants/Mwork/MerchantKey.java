package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Mwork
	// stagingEncryptedText: 3JIwsQ/lRoDHtZczHjpCDw==
	public static String msKeyValueStaging = "OS1pNGogAU4DYVUCXEliCDRQFBMBQ2IBBhZaYRN5Tic=";
	// gatewayEncryptedText: MCebw3YpQJZ0JX5ATD82zA==
	public static String msKeyValueGateway = "d0IpHEtbJkESNG1MIARaeX58Gjl9KlBefkcmG3Jyfwg=";
}
