package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Westlake_Immortal
	// stagingEncryptedText: EslVYnYPRdTZ/MQ5onBZ1Q==
	public static String msKeyValueStaging = "BVA2bDtlXUQUV3B+UXwGN0ZdUB5/K00tf21vSXkWZV4=";
	// gatewayEncryptedText: XuoVV73y4FhXGfV1KIVaYQ==
	public static String msKeyValueGateway = "FHQMJltOIkUoLElIeA9wHR54JygpH0w3LT8mGl01NX8=";
}
