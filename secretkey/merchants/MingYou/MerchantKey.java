package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: MingYou
	// stagingEncryptedText: 90sE6ZaRP1ECEtbXZq7q1Q==
	public static String msKeyValueStaging = "VC5zISgVZz5EWU5UMUJCTiMbZTscJzVvU10cSFsRSDw=";
	// gatewayEncryptedText: w/bwVr1Q5bI1geswaZ491w==
	public static String msKeyValueGateway = "OmFeLAwwYX0IQCcMGVYcRzMJQmJfB0gBVhQbAxJBOT0=";
}
