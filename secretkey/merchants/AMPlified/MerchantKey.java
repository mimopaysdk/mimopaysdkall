package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: AMPlified
	// stagingEncryptedText: pKMhell/VhhTbpXa8uaEQw==
	public static String msKeyValueStaging = "aH8IUV9yfCgfH18faCcydDgJJl9LfQYKREcDS3dKeTc=";
	// gatewayEncryptedText: jdkPvSpgU0rkacALylMzgQ==
	public static String msKeyValueGateway = "fU9geUYXQzkeSyZcOhBeRV82eR4pZFI/BHZSI0QoLlU=";
}
