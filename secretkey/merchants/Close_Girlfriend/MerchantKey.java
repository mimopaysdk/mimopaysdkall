package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Close_Girlfriend
	// stagingEncryptedText: ZkYUxUlmPaPGc4f68jYVUQ==
	public static String msKeyValueStaging = "KxwRFVhhJX51NAVFejMeEAVuJRVPUBhjVxckI34hPRg=";
	// gatewayEncryptedText: 5Js8kwA8WLg2YJwpzTIYNFKG6HyfsgoQfeUJPycOTwI=
	public static String msKeyValueGateway = "JGwgZzNtf3wvUQVpIwFnexI2IRI9dA9NewRJeDxEEUE=";
}
