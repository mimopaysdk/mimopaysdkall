package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: HiRichClub
	// stagingEncryptedText: 80bPi2oVAJ3gXh9Y3T45OA==
	public static String msKeyValueStaging = "ayJXPW9IEH96OAV7YyRrCDMNbCFiJ2I5Jg91EwwtOEU=";
	// gatewayEncryptedText: E+CfBqsvmryD6ttAVOxUBw==
	public static String msKeyValueGateway = "BTA3PxANLg1bUX44b0YrYnpoWhEgMB5fLgEzHhJ1MjQ=";
}
