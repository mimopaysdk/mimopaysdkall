package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: HaloPay
	// stagingEncryptedText: cgJ6oF7KikzR9OGLD17sHQ==
	public static String msKeyValueStaging = "TC1HaXVjO1EDHE1bMTU0LhxuZh9iH0s3NEIxex4UZQ4=";
	// gatewayEncryptedText: OgkUnr57mdGeK3FYQvUC+g==
	public static String msKeyValueGateway = "fBx5EExQH3RqfndDBj5RPwZcAjtHNVpXCAVvIkt+OXM=";
}
