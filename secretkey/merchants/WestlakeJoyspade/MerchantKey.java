package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: WestlakeJoyspade
	// stagingEncryptedText: YvWtIAmedgGA2/KT5qGU5w==
	public static String msKeyValueStaging = "MkZGEzd5GWpEajQLPBtRbghZTk58a2swK2cvZQ9TRi4=";
	// gatewayEncryptedText: 7l7+ywXOpgpUu9asdhNycQ==
	public static String msKeyValueGateway = "R0EvGjhAIGlkF09bPQUzKxkyTS89YAiAZWUfAnp6JDI=";
}
