package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: gamespark
	// stagingEncryptedText: ADaBgJqL/WPl//Q3yD4uTw==
	public static String msKeyValueStaging = "aScGbA5oJxpZHn8Tay5cQxQoWksSM04ORSdNbighdAE=";
	// gatewayEncryptedText: jrvQP7uVSJCPlGA/hiy1iA==
	public static String msKeyValueGateway = "NhaAM0SAFE4KQwZ4O0tzUGFJLkcRQko7GQ0aYlALc2Y=";
}
