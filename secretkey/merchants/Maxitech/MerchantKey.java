package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Maxitech
	// stagingEncryptedText: L1urjBMaKzjLSHj6NkRIIA==
	public static String msKeyValueStaging = "ClI/Rw6AGBp5cnIvHCZRFl1iI0wqVA96HEtlfVEsKXA=";
	// gatewayEncryptedText: 6mereDLnJopCgfQpdIA19A==
	public static String msKeyValueGateway = "ITtKIDV9HzFWVG8QVGI7J0dGJw4+AwkEf0ZYYnFQAUM=";
}
