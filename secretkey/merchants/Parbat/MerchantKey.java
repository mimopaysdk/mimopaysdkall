package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Parbat
	// stagingEncryptedText: vYJIXn+gGSuoL3JYlhNa8A==
	public static String msKeyValueStaging = "gHYkfiQgVVpZIHckYVhoT24ZYkBFAlJ+NHxlRFEzXQI=";
	// gatewayEncryptedText: f6Zol+J+NcfzGW/YwtcuIQ==
	public static String msKeyValueGateway = "c2NFKF1LWARQTWJpBll8LSl4IXcqcn89TRRIR22AG34=";
}
