package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Get5
	// stagingEncryptedText: PQVQfZpM24lGCuwnch3oGA==
	public static String msKeyValueStaging = "KGBAbVksa2QqMzdnRT54UGcQTFIaVnYfVTc5dDYqHzQ=";
	// gatewayEncryptedText: 8u+l4221xtnzKxpRUs0nuw==
	public static String msKeyValueGateway = "Pk0cJV1abAQgHk09QzQyIRAjZ1E+XhMDdhULSjU9JSg=";
}
