package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Baidu_91
	// stagingEncryptedText: d/8HC3/DDtvtz/izK3HQ8Q==
	public static String msKeyValueStaging = "ODldPihwSlsNZxkCDx4XZ2QaNFJsNmZ8WEwtCHYrOQk=";
	// gatewayEncryptedText: J0aGgeezeZ34RMX11wIeGA==
	public static String msKeyValueGateway = "XHowRkhUNl1ebiAdfwZrSEoWC2MMaS14F2Z9WUkOETA=";
}
