package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: 5iSocial
	// stagingEncryptedText: HYI2nED/yAnoTkb3GHcTZQ==
	public static String msKeyValueStaging = "XlAgTzowV2NqEAlZLzcUBwESMylAOUsYWTM7NXIUH1s=";
	// gatewayEncryptedText: +2oSvxG7FqiCkGfFWV9s+Q==
	public static String msKeyValueGateway = "K045QA5gWClFIlRATlJxIB9HWitXVXwRGRIHCEpbbnw=";
}
