package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Hexaus
	// stagingEncryptedText: aHfBtVuH3Rl8yILreKjA6g==
	public static String msKeyValueStaging = "Ozw4RV5xOTh0BDWAcykfDiR+KylmQi1cLCQFRxQsb3c=";
	// gatewayEncryptedText: uQr3INQHSrjBnUpSqsxhFA==
	public static String msKeyValueGateway = "Z0Mabyh3TXVIb3U4TTYRRxN9e3BHBTJfGhwNdXIWMg8=";
}
