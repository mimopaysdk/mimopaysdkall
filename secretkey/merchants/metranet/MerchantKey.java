package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: metranet
	// stagingEncryptedText: 7i3M8/jMbyl/LdKOZBbICQ==
	public static String msKeyValueStaging = "J1IwZUIeUD4rPSZpZFZ4ZGoFREoSA3pNcXUPBTYybWQ=";
	// gatewayEncryptedText: BvjKPIY2sFfA9Q0iwfV34Q==
	public static String msKeyValueGateway = "KyBIZCRXO2EPMDN5TUggXVNdBRV8CwcdXUk7WTBuHGQ=";
}
