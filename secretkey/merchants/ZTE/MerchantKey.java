package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: ZTE
	// stagingEncryptedText: v5R8J36ZE3G4rq7kQCGmZg==
	public static String msKeyValueStaging = "YwRVbXxaVGF8ClgtWiBgUEoZU3JCXwpBViFdDkMbHQ4=";
	// gatewayEncryptedText: MVr5dvm3cYiP5eUc0wz0eQ==
	public static String msKeyValueGateway = "RBQORBQdV24DXHVFNSJ8Nj9CLB5STjAGMjVkSkQ4WhY=";
}
