package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Gameview
	// stagingEncryptedText: Xb2wI+9GLgb9flREPlkbvA==
	public static String msKeyValueStaging = "W2RmF2c2Rid5HRtRUHgFcgkNNx0HdEEdDyRxXHR3QUU=";
	// gatewayEncryptedText: SJURrqERTHjA27h5yhiVMA==
	public static String msKeyValueGateway = "YgpgKScJR10Mc0U1EUQ2EDUPZR0wcAkPATQGOywibDo=";
}
