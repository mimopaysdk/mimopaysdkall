package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Efun
	// stagingEncryptedText: 4tTsipBPxeckg+g9BhiqTw==
	public static String msKeyValueStaging = "TA1VaXdqDSUKVV00LEtvFVYQYR1yUAhsZgJYZwooEwM=";
	// gatewayEncryptedText: 6hLjZKDIUekj8ldLo9fXtg==
	public static String msKeyValueGateway = "E0QHY2wpBycQRVQiISwJVlQQHBB6IE8gSGA9KBAdaUE=";
}
