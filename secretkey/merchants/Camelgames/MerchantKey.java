package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Camelgames
	// stagingEncryptedText: QzVDpLPgypSLUMUex2gJaw==
	public static String msKeyValueStaging = "OjZRbj0UIhAgd2djJDV4bHZ9Egw1Gwp3EEBVeAU2URw=";
	// gatewayEncryptedText: Rx2kktFNqmFBAc+qLfqWmg==
	public static String msKeyValueGateway = "fSkLQRUcdhNWVT04UUt5Tz5rUzVROhZrbn0HFVF9gCI=";
}
