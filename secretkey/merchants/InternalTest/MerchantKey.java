package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: InternalTest
	// stagingEncryptedText: zLdLLbLX7xi2E4zxcbGMPg==
	public static String msKeyValueStaging = "fBMtVB4NaTYzAmczPmpLRSZ4KxF1e345CXNoSl92OQ8=";
	// gatewayEncryptedText: 5aSkczdhkk4ukFsZEHykkA==
	public static String msKeyValueGateway = "CCxNeSYySjQcBUxQLnBfGCZ9FC4/KSJoLgw+am19gBw=";
}
