package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Payssion
	// stagingEncryptedText: WjJxwYWu37aMtUEVwCXd/Q==
	public static String msKeyValueStaging = "CnVYgA4+HlFpKRtrcQVSKAVWJiUQGyo3LBREXAQca1o=";
	// gatewayEncryptedText: tV1ujih7J9zFr3elwsPsoQ==
	public static String msKeyValueGateway = "V0sxJws3CXRyYFoNfGg/CXoNVR9XCEcRBCRRfAtuKUk=";
}
