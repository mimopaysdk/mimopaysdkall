package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: 75Wan
	// stagingEncryptedText: koLqb4GlZmu5g32DkYhQRA==
	public static String msKeyValueStaging = "dEJXJQoabR9aJgMZWXpoFBYvW3l1GjE7H3VrWGEzGhM=";
	// gatewayEncryptedText: I0VRGvvvvgwxc9GlwB4iWg==
	public static String msKeyValueGateway = "fmg3KwpadHRnDmFqXhAjeG93LDxyQjAUcyMXc11hGQI=";
}
