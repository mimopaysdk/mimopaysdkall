

This secretkey source code contains as below
1. directory java
2. directory android
3. makekeygen script
4. directory merchants

to generate key, you need to use script makekeygen. for example:

./makekeygen "ID-9999" SomeMerchant "qwertyuiop" "asdfghjkl"

param1: merchant code
param2: merchant name
param3: staging secretkey
param4: gateway (production) secretkey

to run it, make sure you have installed as below:

1. Install java SDK
	http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
2. Install Java Cryptography Extension (JCE)
	http://www.oracle.com/technetwork/java/javase/downloads/jce8-download-2133166.html
	extract and copy paste file US_export_policy.jar & local_policy.jar to 
	> Mac: /Library/Java/JavaVirtualMachines/jdk1.8.0_72.jdk/Contents/Home/jre/lib/security/
	> Ubuntu: /usr/lib/jvm/jdk1.8.0_72/jre/lib/security/
3. Install android SDK
	> download the sdk from http://developer.android.com/sdk/index.html
	> at the moment, the source code using android SDK level 21. you may check to project.properties in library directory.
	> open terminal, cd <your_android_sdk_folder>/bin/android/sdk/tools/
	> run android manager by typing: ./android &
		make sure to checked android SDK level 21 & update it
4. Install ant.
	Mac: brew install ant
	Ubuntu: sudo apt-get install ant
5. You may try to build java to check all build tools are set. go to folder 'java' & build it
	cd java/
	./go build
6. 