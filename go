#!/bin/bash

###############################################
if [[ ${1:0:1} == "-" ]]; then
###############################################

	## clean
	if [[ "$1" == *c* ]]; then
		"$0" clean
		if [ $? -gt 0 ]; then
			echo "ERROR! when try to build"
			exit 1
		fi
	fi

	## build
	if [[ "$1" == *b* ]]; then
		"$0" build
		if [ $? -gt 0 ]; then
			echo "ERROR! when try to build"
			exit 1
		fi
	fi

	## run
	if [[ "$1" == *r* ]]; then
		"$0" run
		if [ $? -gt 0 ]; then
			echo "ERROR! when try to run"
			exit 1
		fi
	fi

	## debug
	if [[ "$1" == *d* ]]; then
		"$0" debug
		if [ $? -gt 0 ]; then
			echo "ERROR! when try to run"
			exit 1
		fi
	fi

###############################################
elif [ "$1" = "clean" ]; then
###############################################

	echo "" && echo "Cleaning library..." && echo ""
	cd library/
	jandroid -c
	echo "" && echo "Cleaning sample..." && echo ""
	cd ../ogremesh
	jandroid -c
	cd ..

###############################################
elif [ "$1" = "build" ]; then
###############################################

	echo "" && echo "Building library..." && echo ""
	cd library/
	jandroid -b
	if [ $? -gt 0 ]; then
		exit 1
	fi
	echo "" && echo "Building sample..." && echo ""
	cd ../ogremesh
	jandroid -bs
	cd ..

###############################################
elif [ "$1" = "run" ]; then
###############################################

	#clear
	echo "Running sample..."
	cd ogremesh
	jandroid -ir
	cd ..

###############################################
elif [ "$1" = "debug" ]; then
###############################################

	#clear
	echo "Debugging..."
	cd ogremesh
	jandroid -d
	cd ..

###############################################
else
	echo "use param please"
fi
