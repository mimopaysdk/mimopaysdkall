package com.mimopay;

import android.util.Log;
import android.os.Environment;
import android.content.Context;

import java.util.ArrayList;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

final class MimopayStuff
{
	public static String sPaymentMethod = "";
	public static String sChannel = "";

	public static Context mContext = null;

	public static String sEmailOrUserId = "";
	public static String sMerchantCode = "";
	public static String sProductName = "";
	public static String sTransactionId = "";
	public static String sSecretKey = "";
	public static String sSecretKeyStaging = "";
	public static String sSecretKeyGateway = "";
	public static String sCurrency = "";
	public static String sAmount = "";
	
	public static boolean mEnableLog = false;
	public static boolean mIsStaging = false;
	public static String mMimopayUrlPoint = "staging.mimopay.com";

	public static String mMimopayLang = "ID";
	public static String[] sCustomLang = null;

	public static ArrayList<String> alsLastDpoint = null;

	public static String sUpointItem = "";

	public static void saveLastResult(String fname, ArrayList<String> als)
	{
		if(MimopayStuff.mContext == null) {
			Log.d("JimBas", "MimopayStuff'context null");
			return;
		}
		try {
			FileOutputStream out = MimopayStuff.mContext.openFileOutput(fname, Context.MODE_PRIVATE);
			ObjectOutputStream oos = new ObjectOutputStream(out);
			oos.writeObject(als);
			out.close();
		} catch(Exception e) {
			if(MimopayStuff.mEnableLog) Log.d("JimBas", "MimopayStuff'saveLastResult'e: " + e.toString());
		}
	}

	@SuppressWarnings("unchecked")
	public static ArrayList<String> loadLastResult(String fname)
	{
		if(MimopayStuff.mContext == null) {
			Log.d("JimBas", "MimopayStuff'contextnull");
			return null;
		}
		ArrayList<String> als = null;
		FileInputStream fis;
		try {
			fis = MimopayStuff.mContext.openFileInput(fname);
			ObjectInputStream ois = new ObjectInputStream(fis);
			als = (ArrayList) ois.readObject();
			fis.close();
		} catch(Exception e) {
			if(MimopayStuff.mEnableLog) Log.d("JimBas", "MimopayStuff'loadLastResult'e: " + e.toString());
		}

		return als;
	}
}

