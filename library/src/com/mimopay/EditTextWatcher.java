package com.mimopay;

import android.os.Build;
import android.text.TextWatcher;
import android.widget.EditText;
import android.text.Editable;

public class EditTextWatcher implements TextWatcher {
	EditText mEditText;
	int mnInt = 0;

	public EditTextWatcher(EditText edittext)
	{
		super();
		this.mEditText = edittext;
	}

	public EditTextWatcher(EditText edittext, int n)
	{
		this(edittext);
		mnInt = n;
	}

	@Override public void afterTextChanged(Editable arg0) {}
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
}

