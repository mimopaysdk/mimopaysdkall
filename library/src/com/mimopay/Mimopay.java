package com.mimopay;

import android.util.Log;
//import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.os.Handler;

// java
import java.util.ArrayList;

// json
import org.json.JSONArray;
import org.json.JSONException;

public class Mimopay
{
	//////////////////////////////////////////////////////////////////////////////////////////
	// CHANGES NOTES
	//
	// v1.0
	//		initial release
	//
	// v1.1
	//		1. fix bug, upoint result not reported to onReturn
	//		2. avoid fix integer value on mAlsActiveIdx to avoid error
	//		3. centralized mimopay stuff, create new class MimopayStuff
	//		4. keep UnsupportedPaymentMethod, should update the document
	//		5. introduce new error, UnspecifiedChannelRequest
	//		6. fix unproportional UI with different device screen size
	//		7. custom radio buttons, works under froyo to jellybean
	//		8. add marquee effect on device that has small screen size
	//		9. allow wifi-only device keep continue with UPoint transaction
	//		10. new scenario on UPoint. allow user to SMS with other device
	//		11. add autosendsms flag to UPoint quiet mode
	//		12. add save last result
	//		13. disable my own debug log, provide method enableLogcat is someone
	//			wants to see it
	//
	// v1.2
	//		1. remove 'msg' in topup onResultUI, which is actually unused
	//		2. fix bug when start enableGateway :(
	//		3. fix onReturn when _recreate
	//		4. fix onReturn of upoint & xl
	//		5. Now supoort XL, airtime and voucher
	//		6. introduce new error, ErrorHTTP404NotFound, error return 404
	//		7. problem solved, entities-base.properties dan entities-full.properties
	//		8. encrypted secretKey provided
	//
	// v1.2.1
	//		1. fix bug when SDK running without storage (sdcard). Since it unable to download the logo
	//			then all logos replaced by text
	//
	// v1.2.2
	//		1. add scrollable to all UI form
	//
	// v1.2.3
	//		1. fix next button, when some denominations are overlap screen size
	//
	// v1.2.4
	//		1. fix BadTokenException
	//		2. ATM working, but UI mode only
	//
	// v1.2.5
	//		1. UI improved
	//		2. All words translated to bahasa
	//		3. Add description on top up channels, including XL voucher
	//
	// v1.2.6
	//		1. ATMs (BCA & Bersama) Quiet Mode is now available
	//
	// v1.2.7
	//		1. All logos will be shown, even without SDCARD
	//
	// v1.2.8
	//		1. remove unused debug info
	//		2. fix bug on XL, for android honeycomb above
	//
	// v1.2.9
	//		1. build-in UI improved.
	//		2. standardize onReturn's info, all in capital letter
	//
	// v1.3.0
	//		1. fix minor bug
	//		2. In the sample, rename lib to libs.
	//		3. increase connection time-out
	//		4. remove retry
	//
	// v1.3.1
	//		1. re-increase connection time-out, twice then before
	//
	// v1.3.2
	//		1. last result saved to internal
	//
	// v1.3.3
	//		1. atm bersama working now
	//
	// v1.3.4
	//		1. logcat other important vars for troubleshooting purpose
	//		2. denom values, voucher codes, and phone number validation check
	//
	// v2.0
	//		1. improve a lot of things: it looks (UI), process speed, and size
	//
	// v2.1
	//		1. error handling on phone number
	//		2. normalize amount value
	//		3. document, highlight
	//
	// v2.2
	//		1. on auto SMS send, LANJUT -> Kirim SMS
	//		2. support dual sim, by switch to stock message app, upoint & XL
	//		3. minor bug fix on XL airtime
	//		4. Last used phone number, user no need to re-type. It remember the last number type
	//		5. auto disable log when switch to gateway (production)
	//
	// v2.3
	//		1. now suppor custom language, see CustomLang.java
	//		2. add mpoint airtime payment method (maxis)
	//		3. add dpoint airtime payment method (digi)
	//		3. several minor bug fixed
	//
	// v2.4
	//		1. fix minor bug, on topup payment methods return
	//
	// v2.5
	//		1. standardize all onReturn 'info' and 'params'. Please refer to the document
	//
	// v2.6
	//		1. add new function: executeUPointAirtime(String amount)
	//		2. add new function: executeXLAirtime(String amount)
	//
	// v2.7
	//		1. allow dev to get the http status code or java exception status
	//		2. bug fix on generating APIkey on the final URL
	//		3. add 2 functions for complete payment for Digi (DPoint)
	//		4. add filter on productName field to have safe URL
	//
	// v2.8
	//		1. bug fixed, ErrorInvalidPhoneNumber raised because of the total digit input of phone number
	//		2. for digi, it allow to use country code however still should not use any non-numeric character such as '+' character
	//
	// v2.8.1
	//		1. minor change on displaying fixed denom on airtime and atm
	//
	// v2.9
	//		1. new payment method, upoint voucher
	//		2. new payment method, celcom airtime
	//		3. add fix amount for all airtime payment
	//		4. add atm also with fix amount
	//		5. bug fix, exclude 10% on all non-upoint payment methods
	//
	// v2.9.1
	//		1. make sure it use https on production
	//
	// v2.9.2
	//		1. add new function, executeUPointAirtime(String amount, String phoneNumber, boolean autosendsms, String upointItem)
	//
	// v3.0
	//		1. add new payment method, Vietnam telco, VnTelco
	//		2. improve UI look
	//		3. add new error, ErrorChannelIsNotReady, issued when channelStatus = 0
	//
	// v3.1
	//		1. improve UI look
	//		2. remove ErrorChannelIsNotReady, actually it is maintenance mode
	//
	// v3.2
	//		1. re-improve UI look. all logo not used anymore
	//		2. improved speed also
	//		3. fix minor bug on vietnam telco payment method
	//
	// v3.2.1
	//		1. fix minor bug on tsel upoint
	//
	// v3.2.2
	//		1. fix dpoint on receiving sms shortcode
	//		2. add new payment method indosat airtime 
	//
	// v3.2.3
	//		1. additional return info, at index-4, payment submit stage
	//		2. fix layout
	//		3. add denom amount. denomAmount just for show, but keep submit denomValue
	//		4. add disclaimer info from backend
	//		5. fix error message sentences. remove the extra technical info like: length=0 index=0
	//		6. fix bug on enableGateway on the first initialization
	//		7. dynamic UP. now can be updated on backend
	//
	//////////////////////////////////////////////////////////////////////////////////////////
	// GITHUB Credentials:
	//////////////////////////////////////////////////////////////////////////////////////////
	// Username for 'https://github.com': jimmy@mimopay.com
	// Password for 'https://jimmy@mimopay.com@github.com': pr0gressivmedia
	//////////////////////////////////////////////////////////////////////////////////////////	
	private String mSdkVersion = "v3.2.3";
	//////////////////////////////////////////////////////////////////////////////////////////

	private void jprintf(String s)
	{
		if(!MimopayStuff.mEnableLog) return;
		Log.d("JimBas", "Mimopay: " + s);
	}

	private static void sjprintf(String s)
	{
		if(!MimopayStuff.mEnableLog) return;
		Log.d("JimBas", "Mimopay: " + s);
	}

	private String mPhoneNumber = null;
	private String mAirtimeValue = null;
	private String msAmount = null;
	private String sCode = null;

	private MimopayQuietModeCore mCore = null;
	public static MimopayInterface mMi = null;

	private char toHex(int ch) { return (char) (ch < 10 ? '0' + ch : 'A' + ch - 10); }
	private boolean isSafe(char ch) { return ((ch>='A' && ch<='Z') || (ch>='a' && ch<='z') || (ch>='0' && ch<='9') || "-_.~".indexOf(ch)>=0); }
	private String escapeSpecialCharacters(String input)
	{
		StringBuilder resultStr = new StringBuilder();
		for(char ch : input.toCharArray()) {
			if(isSafe(ch)) {
				resultStr.append(ch);
			} else {
				resultStr.append('%');
				resultStr.append(toHex(ch / 16));
				resultStr.append(toHex(ch % 16));                   
			}
		}
			return resultStr.toString();
	}

	public Mimopay(Context context, String emailOrUserId, String merchantCode, String productName, 
		String transactionId, String secretKeyStaging, String secretKeyGateway, String currency, MimopayInterface mi)
	{
		MimopayStuff.sPaymentMethod = "";
		MimopayStuff.sChannel = "";
		enableGateway(false);
		// MimopayStuff.mEnableLog = true;
		MimopayStuff.mIsStaging = true;
		// MimopayStuff.mMimopayUrlPoint = "staging.mimopay.com";
		MimopayStuff.mMimopayLang = "ID";
		MimopayStuff.sCustomLang = null;

		MimopayStuff.mContext = context;
		MimopayStuff.sEmailOrUserId = emailOrUserId;
		MimopayStuff.sMerchantCode = merchantCode;
		MimopayStuff.sProductName = escapeSpecialCharacters(productName);
		MimopayStuff.sTransactionId = transactionId;
		MimopayStuff.sSecretKeyStaging = secretKeyStaging;
		MimopayStuff.sSecretKeyGateway = secretKeyGateway;
		MimopayStuff.sSecretKey = secretKeyStaging;
		MimopayStuff.sCurrency = currency;
		MimopayStuff.sAmount = "0";
		MimopayStuff.sUpointItem = "";
		
		mMi = mi;
	}

	private boolean isVoucherCodeInvalid(String vouchercode)
	{
		if(vouchercode.length() < 7) return true;
		if(vouchercode.length() > 16) return true;
		int i,j;
		char[] charr = vouchercode.toCharArray();
		j = charr.length;
		for(i=0;i<j;i++) {
			if(!Character.isDigit(charr[i])) return true;
		}
		return false;
	}

	public static boolean isPhoneNumberInvalid(String sphonenum)
	{
		int i,j;

		char[] charr = sphonenum.toCharArray();
		j = charr.length;

		sjprintf("phonenumber: " + sphonenum + " length: " + j);
		int phn = 10;
		if(MimopayStuff.sChannel.equals("dpoint") || MimopayStuff.sChannel.equals("mpoint") || MimopayStuff.sChannel.equals("celcom")) phn = 9;
		if(j < phn) {
			sjprintf("Phone number is less than " + Integer.toString(phn) + " digits");
			return true;
		}

		for(i=0;i<j;i++) {
			if(!Character.isDigit(charr[i])) return true;
		}
		return false;
	}

	private boolean isDenomValueInvalid(String sjsonarr, String sdenomval)
	{
		String s;
		int i=0,j=0;

		try {
			JSONArray jarr = new JSONArray(sjsonarr);
			j = jarr.length();
			for(i=0;i<j;i++) {
				s = jarr.getString(i);
				if(sdenomval.equals(s)) return false;
			}
		} catch(JSONException e) {}
		return true;
	}

	private String normalizeAmountValue(String amount)
	{
		float famount = 0.0f;
		try {
			famount = Float.parseFloat(amount);
		} catch(Exception e) {
			jprintf("normalizeAmountValue'exception: " + e.toString());
			ArrayList<String> als = new ArrayList<String>();
			als.add("ErrorInvalidAmountValue");
			if(mMi != null) mMi.onReturn("ERROR", als);
		}
		int namount = (int) famount;
		String sres = Integer.toString(namount);
		return sres;
	}

	private class MimopayQuietModeCore extends MimopayCore
	{
		@Override public void onError(String serr)
		{
			String info = "ERROR";
			ArrayList<String> als = new ArrayList<String>();
			als.add(serr);
			//als.add(Integer.toString(mnRespondStatusCode));
			//if(mnRespondStatusCode > 0) {
			als.add(Integer.toString(mnRespondStatusCode));
			if(mnRespondStatusCode == -1) {
				als.add(msExceptionMessage);
			}
			if(mMi != null) {
				mMi.onReturn(info, als);
			}
		}

		@Override public void onMerchantPaymentMethodRetrieved()
		{
			String s;
			ArrayList<String> als;

			jprintf(String.format("onMerchantPaymentMethodRetrieved: %s %s %d", MimopayStuff.sPaymentMethod, MimopayStuff.sChannel, mCore.mAlsActiveIdx));
			/////////////////////////////////////////////////////////////////////
			// smartfren, sevelin
			if(MimopayStuff.sPaymentMethod.equals("Topup")) {
				// verify voucher code
				mCore.setChannelActiveIndex(MimopayStuff.sChannel);
				if(isVoucherCodeInvalid(sCode)) {
					als = new ArrayList<String>();
					als.add("ErrorInvalidVoucherCode");
					if(mMi != null) mMi.onReturn("ERROR", als);
					return;
				}
				jprintf("voucher code valid");
				// okay, its now safe to continue
				mCore.mHttppostCode = sCode;
				mCore.executeBtnAction();
			/////////////////////////////////////////////////////////////////////
			// upoint
			} else if(MimopayStuff.sPaymentMethod.equals("Airtime")) {
				mCore.mAlsActiveIdx = 0;
				s = mCore.alsDenomKey.get(mCore.mAlsActiveIdx);
				// verify denom value
				if(isDenomValueInvalid(s, mAirtimeValue)) {
					als = new ArrayList<String>();
					als.add("ErrorInvalidDenomValue");
					if(mMi != null) mMi.onReturn("ERROR", als);
					return;
				}
				jprintf("denom value valid");
				// verify phone number
				if(isPhoneNumberInvalid(mPhoneNumber)) {
					als = new ArrayList<String>();
					als.add("ErrorInvalidPhoneNumber");
					if(mMi != null) mMi.onReturn("ERROR", als);
					return;
				}
				jprintf("phone number okay");
				// okay, its now safe to continue
				mCore.mHttppostCode = mAirtimeValue;
				mCore.mHttppostPhoneNumber = mPhoneNumber;
				mCore.executeBtnAction();
			/////////////////////////////////////////////////////////////////////
			// atm bca, bersama
			} else if(MimopayStuff.sPaymentMethod.equals("ATM")) {
				if(MimopayStuff.sAmount.equals("0")) {
					s = mCore.alsDenomKey.get(0);
					// verify denom value
					if(isDenomValueInvalid(s, msAmount)) {
						als = new ArrayList<String>();
						als.add("ErrorInvalidDenomValue");
						if(mMi != null) mMi.onReturn("ERROR", als);
						return;
					}
					jprintf("denom value valid");
					MimopayStuff.sAmount = msAmount;
					mCore.reset();
					mCore.retrieveMerchantPaymentMethod();
				} else {
					mCore.setChannelActiveIndex(MimopayStuff.sChannel);
					mCore.mHttppostCode = mCore.alsDenomKey.get(mCore.mAlsActiveIdx);
					mCore.executeBtnAction();
				}
			/////////////////////////////////////////////////////////////////////
			// XL
			} else if(MimopayStuff.sPaymentMethod.equals("XL")) {
				mCore.setChannelActiveIndex(MimopayStuff.sChannel);
				if(MimopayStuff.sChannel.equals("xl_airtime")) {
					// verify denom value
					s = mCore.alsDenomKey.get(mCore.mAlsActiveIdx);
					if(isDenomValueInvalid(s, mAirtimeValue)) {
						als = new ArrayList<String>();
						als.add("ErrorInvalidDenomValue");
						if(mMi != null) mMi.onReturn("ERROR", als);
						return;
					}
					jprintf("denom value valid");
					// verify phone number
					if(isPhoneNumberInvalid(mPhoneNumber)) {
						als = new ArrayList<String>();
						als.add("ErrorInvalidPhoneNumber");
						if(mMi != null) mMi.onReturn("ERROR", als);
						return;
					}
					jprintf("phone number okay");
					mCore.mHttppostCode = mAirtimeValue;
					mCore.mHttppostPhoneNumber = mPhoneNumber;
				} else if(MimopayStuff.sChannel.equals("xl_hrn")) {
					if(isVoucherCodeInvalid(sCode)) {
						als = new ArrayList<String>();
						als.add("ErrorInvalidVoucherCode");
						if(mMi != null) mMi.onReturn("ERROR", als);
						return;
					}
					jprintf("voucher code valid");
					mCore.mHttppostCode = sCode;
				}
				jprintf(String.format("%s %s %d", MimopayStuff.sPaymentMethod, MimopayStuff.sChannel, mCore.mAlsActiveIdx));
				mCore.executeBtnAction();
			/////////////////////////////////////////////////////////////////////
			// Vietnam Telco
			} else if(MimopayStuff.sPaymentMethod.equals("VnTelco")) {
				mCore.setChannelActiveIndex("topup_vn");
				mCore.mHttppostCode = sCode;
				mCore.executeBtnAction();
			}
		}

		@Override public void onResultUI(String channel, ArrayList<String> params)
		{
			String info = "UserCanceled";
			ArrayList<String> als = null;

			jprintf("channel: " + channel);
			if(channel.equals("smartfren") || channel.equals("sevelin") || channel.equals("xl_hrn")  || channel.equals("upoint_hrn") || channel.equals("dpoint_airtime_charge") || channel.equals("topup_vn")) {
				final String fstopupres = params.get(0);
				final String fstransid = params.get(1);
				info = "SUCCESS";
				als = new ArrayList<String>();
				als.add(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
				als.add(fstopupres);
				als.add(fstransid);
				if(mMi != null) {
					mMi.onReturn(info, als);
				}
				if(channel.equals("dpoint_airtime_charge")) {
					MimopayStuff.alsLastDpoint = null;
				}
			} else if(channel.equals("atm_bca") || channel.equals("atm_bersama")) {
				final String fcompanyCode = params.get(0);
				final String ftotalBill = params.get(1);
				final String ftransId = params.get(2);
				info = "SUCCESS";
				als = new ArrayList<String>();
				als.add(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
				als.add(fcompanyCode);
				als.add(ftotalBill);
				als.add(ftransId);
				if(mMi != null) {
					mMi.onReturn(info, als);
				}
			} else if(channel.equals("upoint_airtime")) {
				// v1.0.1
				final String fsmsContent = params.get(0);
				final String fsmsNumber = params.get(1);
				info = "SUCCESS";
				als = new ArrayList<String>();
				als.add(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
				als.add(fsmsContent);
				als.add(fsmsNumber);
				if(mMi != null) {
					mMi.onReturn(info, als);
				}
			} else if(channel.equals("xl_airtime")) {
				info = "SUCCESS";
				als = new ArrayList<String>();
				als.add(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
				int i,j = params.size();
				for(i=0;i<j;i++) {
					als.add(params.get(i));
				}
				if(mMi != null) {
					mMi.onReturn(info, als);
				}
			} else if(channel.equals("mpoint_airtime")) {
				info = "SUCCESS";
				als = new ArrayList<String>();
				als.add(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
				int i,j = params.size();
				for(i=0;i<j;i++) {
					als.add(params.get(i));
				}
				if(mMi != null) {
					mMi.onReturn(info, als);
				}
			} else if(channel.equals("dpoint_airtime")) {
				MimopayStuff.alsLastDpoint = new ArrayList<String>();
				int i,j=params.size();
				for(i=0;i<j;i++) MimopayStuff.alsLastDpoint.add(params.get(i));
				info = "SUCCESS";
				als = new ArrayList<String>();
				als.add(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
				als.add("OK"); als.add("SMS CODE is comming in");
				if(mMi != null) {
					mMi.onReturn(info, als);
				}
			} else if(channel.equals("celcom_airtime")) {
				info = "SUCCESS";
				als = new ArrayList<String>();
				als.add(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
				int i,j = params.size();
				for(i=0;i<j;i++) {
					als.add(params.get(i));
				}
				if(mMi != null) {
					mMi.onReturn(info, als);
				}
			}
		}
		@Override public void onSmsCompleted(boolean success, ArrayList<String> alsSms)
		{
			if(success) {
				return;
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	// Topup

	//private void doDefaultUI(String paymentMethod, String channel)
	private void doDefaultUI(String paymentMethod, String channel, String lang)
	{
		// ini akan nampilin form topup yg main
		Intent mimopayIntent = new Intent(MimopayStuff.mContext, MimopayActivity.class);

		MimopayStuff.mMimopayLang = lang;
		MimopayStuff.sPaymentMethod = paymentMethod;
		MimopayStuff.sChannel = channel;

		fixMimopayUrlPoint(MimopayStuff.mIsStaging);

		if(mPhoneNumber != null) {
			mimopayIntent.putExtra("PhoneNumber", mPhoneNumber);
			mimopayIntent.putExtra("AirtimeValue", mAirtimeValue);
		}

		mimopayIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		MimopayStuff.mContext.startActivity(mimopayIntent);
	}

	//channel: smartfren, sevelin
	// bypass form main, langsung ke ui masukkan kode voucher
	public void executeTopup(String channel)
	{
		doDefaultUI("Topup", channel, "ID");
	}

	//channel: smartfren, sevelin
	// bypass semuanya, langsung onReturnTopup
	public void executeTopup(String channel, String code)
	{
		mCore = new MimopayQuietModeCore();

		MimopayStuff.sPaymentMethod = "Topup";
		MimopayStuff.sChannel = channel;
		sCode = new String(code);
		mCore.executePayment();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	// ATMs

	//channel: atm_bca, atm_bersama
	public void executeATMs(String channel)
	{
		doDefaultUI("ATM", channel, "ID");
	}

	// private boolean mbActiveATMsUI = false;
	// public void setActiveATMsUI(boolean builtin)
	// {
	// 	mbActiveATMsUI = builtin;
	// }

	//channel: atm_bca, atm_bersama
	public void executeATMs(String channel, String amount)
	{
		// if(mbActiveATMsUI) {
		MimopayStuff.sAmount = normalizeAmountValue(amount);
		doDefaultUI("ATM", channel, "ID");
		// mbActiveATMsUI = false;
		// 	return;
		// }
		// mCore = new MimopayQuietModeCore();
		// MimopayStuff.sPaymentMethod = "ATM";
		// MimopayStuff.sChannel = channel;
		// msAmount = normalizeAmountValue(amount);
		// MimopayStuff.sAmount = "0"; //amount;
		// mCore.executePayment();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	// UPoint

	public void executeUPointHrn()
	{
		doDefaultUI("Topup", "upoint_hrn", "ID");
	}

	public void executeUPointHrn(String code)
	{
		mCore = new MimopayQuietModeCore();

		MimopayStuff.sPaymentMethod = "Topup";
		MimopayStuff.sChannel = "upoint_hrn";
		sCode = new String(code);
		mCore.executePayment();
	}

	public void executeUPointAirtime()
	{
		doDefaultUI("Airtime", "upoint", "ID");
	}

	public void executeUPointAirtime(String amount)
	{
		MimopayStuff.sAmount = normalizeAmountValue(amount);
		doDefaultUI("Airtime", "upoint", "ID");
	}
	
	public void executeUPointAirtime(String amount, String phoneNumber, boolean autosendsms)
	{
		mAirtimeValue = normalizeAmountValue(amount);
		mPhoneNumber = phoneNumber;
		if(autosendsms) {
			doDefaultUI("Airtime", "upoint", "ID");
		} else {
			mCore = new MimopayQuietModeCore();
			MimopayStuff.sPaymentMethod = "Airtime";
			MimopayStuff.sChannel = "upoint";
			mCore.executePayment();
		}
	}
	
	public void executeUPointAirtime(String amount, String phoneNumber, boolean autosendsms, String upointItem)
	{
		MimopayStuff.sUpointItem = upointItem;
		executeUPointAirtime(amount, phoneNumber, autosendsms);
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////
	// XL

	public void executeXLAirtime()
	{
		doDefaultUI("XL", "xl_airtime", "ID");
	}

	public void executeXLHrn(String code)
	{
		mCore = new MimopayQuietModeCore();

		MimopayStuff.sPaymentMethod = "XL";
		MimopayStuff.sChannel = "xl_hrn";
		sCode = new String(code);
		mCore.executePayment();
	}

	public void executeXLHrn()
	{
		doDefaultUI("XL", "xl_hrn", "ID");
	}

	public void executeXLAirtime(String amount, String phoneNumber, boolean autosendsms)
	{
		mAirtimeValue = normalizeAmountValue(amount);
		mPhoneNumber = phoneNumber;
		if(autosendsms) {
			doDefaultUI("XL", "xl_airtime", "ID");
		} else {
			mCore = new MimopayQuietModeCore();
			MimopayStuff.sPaymentMethod = "XL";
			MimopayStuff.sChannel = "xl_airtime";
			mCore.executePayment();
		}
	}

	public void executeXLAirtime(String amount)
	{
		MimopayStuff.sAmount = normalizeAmountValue(amount);
		doDefaultUI("XL", "xl_airtime", "ID");
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	// Indosat

	public void executeIndosatAirtime()
	{
		doDefaultUI("indosat", "indosat_airtime", "ID");
	}

	public void executeIndosatAirtime(String amount)
	{
		MimopayStuff.sAmount = normalizeAmountValue(amount);
		doDefaultUI("indosat", "indosat_airtime", "ID");
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	// MPoint Airtime

	public void executeMPointAirtime()
	{
		doDefaultUI("Airtime", "mpoint", "EN");
	}

	public void executeMPointAirtime(String amount)
	{
		MimopayStuff.sAmount = normalizeAmountValue(amount);
		doDefaultUI("Airtime", "mpoint", "EN");
	}
	
	public void executeMPointAirtime(String amount, String phoneNumber, boolean autosendsms)
	{
		MimopayStuff.mMimopayLang = "EN";
		mAirtimeValue = normalizeAmountValue(amount);
		mPhoneNumber = phoneNumber;
		if(autosendsms) {
			doDefaultUI("Airtime", "mpoint", "EN");
		} else {
			mCore = new MimopayQuietModeCore();
			MimopayStuff.sPaymentMethod = "Airtime";
			MimopayStuff.sChannel = "mpoint";
			fixMimopayUrlPoint(MimopayStuff.mIsStaging);
			mCore.executePayment();
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////
	// DPoint Airtime

	public void executeDPointAirtime()
	{
		doDefaultUI("Airtime", "dpoint", "EN");
	}

	public void executeDPointAirtime(String amount)
	{
		MimopayStuff.sAmount = normalizeAmountValue(amount);
		doDefaultUI("Airtime", "dpoint", "EN");
	}
	
	public boolean isDPointPaymentIncomplete()
	{
		if(MimopayStuff.alsLastDpoint != null) {
			if(MimopayStuff.alsLastDpoint.size() == 7) {
				return true;
			} else { jprintf("1"); }
		} else { jprintf("2"); }
		return false;
	}

	public void completeDPointPayment(String smscode)
	{
		if(!isDPointPaymentIncomplete()) {
			return;
		}
		mCore = new MimopayQuietModeCore();
		MimopayStuff.sPaymentMethod = "Airtime";
		MimopayStuff.sChannel = "dpoint";
		fixMimopayUrlPoint(MimopayStuff.mIsStaging);
		mCore.mAlsActiveIdx = 0;
		mCore.alsUrls = new ArrayList<String>();
		mCore.alsUrls.add(MimopayStuff.alsLastDpoint.get(0));			//postUrl
		//String oldpaytype = mCore.alsBtnAction.get(mAlsActiveIdx);
		mCore.alsBtnAction = new ArrayList<String>();
		mCore.alsBtnAction.add(MimopayStuff.alsLastDpoint.get(1));		//btnAction
		//mCore.alsBtnAction.add(oldpaytype);
		mCore.alsTextEditName = new ArrayList<String>();
		mCore.alsTextEditName.add(MimopayStuff.alsLastDpoint.get(2));	//code
		mCore.alsDenomKey = new ArrayList<String>();
		mCore.alsDenomKey.add(MimopayStuff.alsLastDpoint.get(3));		//mdn
		mCore.alsDenomPost = new ArrayList<String>();
		mCore.alsDenomPost.add(MimopayStuff.alsLastDpoint.get(4));		//denom
		mCore.alsDenomValue = new ArrayList<String>();
		mCore.alsDenomValue.add(MimopayStuff.alsLastDpoint.get(5));		//mdn
		mCore.mHttppostPhoneNumber = MimopayStuff.alsLastDpoint.get(6);
		mCore.mHttppostCode = smscode;
		mCore.executeBtnAction();
		//MimopayStuff.alsLastDpoint = null;
	}

	public void executeDPointAirtime(String amount, String phoneNumber, boolean autosendsms)
	{
		MimopayStuff.mMimopayLang = "EN";
		mAirtimeValue = normalizeAmountValue(amount);
		mPhoneNumber = phoneNumber;
		if(autosendsms) {
			doDefaultUI("Airtime", "dpoint", "EN");
		} else {
			mCore = new MimopayQuietModeCore();
			MimopayStuff.sPaymentMethod = "Airtime";
			MimopayStuff.sChannel = "dpoint";
			fixMimopayUrlPoint(MimopayStuff.mIsStaging);
			mCore.executePayment();
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////
	// Celcom

	public void executeCelcomAirtime()
	{
		doDefaultUI("Airtime", "celcom", "EN");
	}

	public void executeCelcomAirtime(String amount)
	{
		MimopayStuff.sAmount = normalizeAmountValue(amount);
		doDefaultUI("Airtime", "celcom", "EN");
	}
	
	public void executeCelcomAirtime(String amount, String phoneNumber, boolean autosendsms)
	{
		mAirtimeValue = normalizeAmountValue(amount);
		mPhoneNumber = phoneNumber;
		if(autosendsms) {
			doDefaultUI("Airtime", "celcom", "EN");
		} else {
			mCore = new MimopayQuietModeCore();
			MimopayStuff.sPaymentMethod = "Airtime";
			MimopayStuff.sChannel = "celcom";
			fixMimopayUrlPoint(MimopayStuff.mIsStaging);
			mCore.executePayment();
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	// Vietname Telcos

	// VinaPhone => vnp
	// MobiFone => vms
	// Viettel => vte

	public void executeVnTelco()
	{
		doDefaultUI("VnTelco", "VnTelco", "EN");
	}

	public void executeVnTelco(String channel, String serialNumber, String pinCode, String phoneNumber, String emailAdress)
	{
		mCore = new MimopayQuietModeCore();

		MimopayStuff.sPaymentMethod = "VnTelco";
		MimopayStuff.sChannel = channel;

		JSONArray jarr = new JSONArray();
		jarr.put(channel);
		jarr.put(serialNumber);
		jarr.put(pinCode);
		jarr.put(phoneNumber);
		jarr.put(emailAdress);

		sCode = jarr.toString();
		fixMimopayUrlPoint(MimopayStuff.mIsStaging);
		mCore.executePayment();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	// Utils

	private void fixMimopayUrlPoint(boolean bstaging)
	{
		MimopayStuff.mIsStaging = bstaging;
		String sch = MimopayStuff.sChannel;
		String spymt = MimopayStuff.sPaymentMethod;
		if(bstaging) {
			if(sch.equals("mpoint") || sch.equals("dpoint") || sch.equals("celcom") || spymt.indexOf("VnTelco")!=-1) {
				MimopayStuff.mMimopayUrlPoint = "gtower.mimopay.com";
			} else {
				MimopayStuff.mMimopayUrlPoint = "staging.mimopay.com";
			}
		} else {
			if(sch.equals("mpoint") || sch.equals("dpoint") || sch.equals("celcom") || spymt.indexOf("VnTelco")!=-1) {
				MimopayStuff.mMimopayUrlPoint = "my.mimopay.com";
			} else {
				MimopayStuff.mMimopayUrlPoint = "gateway.mimopay.com";
			}
		}
	}

	public void setCurrency(String currency)
	{
		MimopayStuff.sCurrency = currency;
	}

	public void setUiLanguage(String[] slang)
	{
		jprintf(String.format("slang:%d", slang.length));
		MimopayStuff.sCustomLang = slang;
	}

	public String getSdkVersion()
	{
		return mSdkVersion;
	}

	public void enableGateway(boolean enable)
	{
		if(enable) {
			MimopayStuff.mEnableLog = false;
			MimopayStuff.mMimopayUrlPoint = "gateway.mimopay.com";
			fixMimopayUrlPoint(false);
			MimopayStuff.sSecretKey = MimopayStuff.sSecretKeyGateway;
		} else {
			MimopayStuff.mEnableLog = true;
			MimopayStuff.mMimopayUrlPoint = "staging.mimopay.com";
			fixMimopayUrlPoint(true);
			MimopayStuff.sSecretKey = MimopayStuff.sSecretKeyStaging;
		}
		//mCore.enableGateway(enable);
	}

	public void enableLocal(String localip)
	{
		MimopayStuff.mMimopayUrlPoint = localip;
		MimopayStuff.sSecretKey = MimopayStuff.sSecretKeyStaging;
	}

	public void enableLog(boolean enable)
	{
		MimopayStuff.mEnableLog = enable;
	}

	/*public void disableJson(boolean disable)
	{
		MimopayStuff.mEnableJson = !disable;
	}*/

	public String[] getLastResult()
	{
		int ires = 0;
		String[] sarr = null;
		ArrayList<String> als = MimopayStuff.loadLastResult("lastpayresult");
		if(als != null) {
			ires = als.size();
			sarr = new String[ires];
			for(int i=0;i<ires;i++) {
				sarr[i] = als.get(i);
			}
		}
		return sarr;
	}
}

