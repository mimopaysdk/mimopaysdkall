package com.mimopay;

import android.app.Activity;
import android.os.Build;
import android.util.Log;
import android.os.AsyncTask;
import android.os.Environment;
import android.webkit.URLUtil;
import android.telephony.SmsManager;
import android.net.Uri;
import android.database.Cursor;
import android.content.Context;
import android.content.CursorLoader;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;

// JSON
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// java
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CharsetDecoder;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.security.MessageDigest;
import java.math.BigInteger;
import java.io.File;
import java.net.URL;
import java.net.URLConnection;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.SocketTimeoutException;
import java.io.FileNotFoundException;
import java.util.Iterator;

// apache
import org.apache.http.NameValuePair;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.StatusLine;
import org.apache.http.HttpEntity;

public class MimopayCore
{
	private void jprintf(String s)
	{
		if(!MimopayStuff.mEnableLog) return;
		Log.d("JimBas", "MimopayCore: " + s);
	}

	public ArrayList<String> alsLogos = null;
	public ArrayList<String> alsLabels = null;
	public ArrayList<String> alsBtnAction = null;	// smartfren, sevelin, ...
	public ArrayList<String> alsUrls = null;
	public ArrayList<String> alsDenomKey = null;
	public ArrayList<String> alsDenomValue = null;
	public ArrayList<String> alsDenomAmount = null;
	public ArrayList<String> alsTextEditName = null;
	public ArrayList<String> alsDenomPost = null;
	public ArrayList<String> alsChannelStatus = null;
	public ArrayList<String> alsStringTnc = null;

	//private boolean mbChannelStatusOk = true;
	public String mMimopayLogoUrl = "";
	public String mMimopayHelpLogoUrl = "";
	public int mAlsActiveIdx = -1;
	//public String mHttppostDenom = null;
	public String mHttppostCode = null;
	//public String mOldHttppostCode = null;
	public String mHttppostPhoneNumber = null;

	private final int PAYMENTRESULT_OK = 1;
	private final int PAYMENTRESULT_VALIDATEERROR = 2;
	private final int PAYMENTRESULT_INTERNETPROBLEM = 3;
	private final int PAYMENTRESULT_UNDERMAINTENANCE = 4;
	//private final int PAYMENTRESULT_CHANNELISNOTREADY = 5;

	////////////////////////////////////////////////////////////////////////////////////
	// Override methods

    protected void onProgress(boolean start, String cmd) {}
    protected void onError(String serr) {}
	protected void onMerchantPaymentMethodRetrieved() {}
	protected void onResultUI(String channel, ArrayList<String> params) {}
	protected void onSmsCompleted(boolean success, ArrayList<String> alsSms) {}

	////////////////////////////////////////////////////////////////////////////////////
	// Core metods

	public MimopayCore(){}

	public void executePayment()
	{	
		if(MimopayStuff.sTransactionId.equals("")) {
			MimopayStuff.sTransactionId = getUnixTimeStamp();
			jprintf("Using unix timestamp: " + MimopayStuff.sTransactionId);
		}

		jprintf(
			"PaymentMethod: "	+ MimopayStuff.sPaymentMethod + "\n" +
			"Channel: " 		+ MimopayStuff.sChannel + "\n" +
			"EmailOrUserId: "	+ MimopayStuff.sEmailOrUserId + "\n" +
			"MerchantCode: "	+ MimopayStuff.sMerchantCode + "\n" +
			"ProductName: "		+ MimopayStuff.sProductName + "\n" +
			"TransactionId: "	+ MimopayStuff.sTransactionId + "\n" +
			"SecretKeyStaging: "+ MimopayStuff.sSecretKeyStaging + "\n" +
			"SecretKeyGateway: "+ MimopayStuff.sSecretKeyGateway + "\n" +
			"SecretKey: "		+ MimopayStuff.sSecretKey + "\n" +
			"Currency: "		+ MimopayStuff.sCurrency + "\n" +
			"sAmount: "			+ MimopayStuff.sAmount + "\n" +
			"");

		new RetrieveMerchantPaymentMethod().execute();
	}
	
	public void reset()
	{
		alsLogos = null;
		alsLabels = null;
		alsBtnAction = null;	// smartfren, sevelin, ...
		alsUrls = null;
		alsDenomKey = null;
		alsDenomValue = null;
		alsDenomAmount = null;
		alsStringTnc= null;
		alsTextEditName = null;
		alsDenomPost = null;
		mMimopayLogoUrl = "";
		mMimopayHelpLogoUrl = "";
		mAlsActiveIdx = -1;
		//mHttppostDenom = null;
		mHttppostCode = null;
		mHttppostPhoneNumber = null;
	}
	
	public void retrieveMerchantPaymentMethod()
	{
		new RetrieveMerchantPaymentMethod().execute();
	}

	private ByteBuffer str2bb(String msg)
	{
		Charset charset = Charset.forName("UTF-8");
		CharsetEncoder encoder = charset.newEncoder();
		try {
			return encoder.encode(CharBuffer.wrap(msg));
		} catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	private String bb2str(ByteBuffer buffer)
	{
		Charset charset = Charset.forName("UTF-8");
		CharsetDecoder decoder = charset.newDecoder();
		String data = "";
		try {
			int old_position = buffer.position();
			data = decoder.decode(buffer).toString();
			// reset buffer's position to its original so it is not altered:
			buffer.position(old_position);  
		} catch (Exception e){
			e.printStackTrace();
			return "";
		}
		return data;
	}

//D/JimBas  ( 1715): MimopayCore: btnActionPost,sresultc: {"result":{"transactionId":"040018295758","statusInfo":"OK","statusMessage":"Sukses","remarks":false,"smsCode":"456495","shortCode":"98000","companyCode":"UP","totalAmount":"","atmTransactionId":""}}

	private String getUnixTimeStamp()
	{
		int tm = (int) (System.currentTimeMillis() % Integer.MAX_VALUE);
		return Integer.toString(tm);
	}

	private String getAPIKey(String transid, String merchantcode, String secretkey)
	{
		String smd5 = transid + merchantcode + secretkey;
		ByteBuffer bb = str2bb(smd5);

		String sres = "";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(bb);
			byte[] digest = md.digest();
			StringBuffer hexString = new StringBuffer();
			for(int i=0; i<digest.length; i++)
				hexString.append(String.format("%02x", digest[i]));
			sres = hexString.toString();
		} catch(Exception e) {
			jprintf("exception: " + e.toString());
		}
		return sres;
	}

	private String assembleMerchantLoadUrl()
	{
		String surl = null;
		String apikey = getAPIKey(MimopayStuff.sTransactionId,
			MimopayStuff.sMerchantCode, MimopayStuff.sSecretKey);

		String shttp = "http://";
		String surlpoint = MimopayStuff.mMimopayUrlPoint;
		if(surlpoint.indexOf("gateway") != -1 ||
		  MimopayStuff.sChannel.equals("mpoint") ||
		  MimopayStuff.sChannel.equals("dpoint") || 
		  MimopayStuff.sChannel.equals("celcom")) {
			shttp = "https://";
		}

		mMimopayLogoUrl = mMimopayHelpLogoUrl = null;

		if(MimopayStuff.sPaymentMethod.equals("Topup")) {
			if(MimopayStuff.sChannel.equals("upoint_hrn")) {
				surl = shttp + MimopayStuff.mMimopayUrlPoint + "/upoint_hrn_api/load" + "_json/";
			} else {
				surl = shttp + MimopayStuff.mMimopayUrlPoint + "/topup_api/load" + "_json/";
			}
		} else if(MimopayStuff.sPaymentMethod.equals("Airtime")) {
			if(MimopayStuff.sChannel.equals("upoint")) {
				surl = shttp + MimopayStuff.mMimopayUrlPoint + "/upoint_airtime_api/load" + "_json/";
			} else if(MimopayStuff.sChannel.equals("mpoint")) {
				surl = shttp + MimopayStuff.mMimopayUrlPoint + "/mpoint_airtime_api/load" + "_json/";
			} else if(MimopayStuff.sChannel.equals("dpoint")) {
				surl = shttp + MimopayStuff.mMimopayUrlPoint + "/dpoint_airtime_api/load" + "_json/";
			} else if(MimopayStuff.sChannel.equals("celcom")) {
				surl = shttp + MimopayStuff.mMimopayUrlPoint + "/celcom_airtime_api/load" + "_json/";
			} else {
				jprintf("Unsupported PaymentMethod");
				return null;
			}
		} else if(MimopayStuff.sPaymentMethod.equals("XL")) {
			surl = shttp + MimopayStuff.mMimopayUrlPoint + "/xl_api/load" + "_json/";
		} else if(MimopayStuff.sPaymentMethod.equals("indosat")) {
			surl = shttp + MimopayStuff.mMimopayUrlPoint + "/indosat_airtime_api/load" + "_json/";
		} else if(MimopayStuff.sPaymentMethod.equals("ATM")) {
			surl = shttp + MimopayStuff.mMimopayUrlPoint + "/atm_api/load" + "_json/";
		} else if(MimopayStuff.sPaymentMethod.equals("VnTelco")) {
			shttp = "https://";
			surl = shttp + MimopayStuff.mMimopayUrlPoint + "/topup_vn_api/load" + "_json/";
		}

		if(surl != null) {
			surl += (MimopayStuff.sEmailOrUserId + "/");
			surl += (MimopayStuff.sProductName + "/");
			surl += (MimopayStuff.sMerchantCode + "/");
			surl += (MimopayStuff.sTransactionId + "/");
			surl += (MimopayStuff.sCurrency + "/");
			surl += (MimopayStuff.sAmount + "/");
			surl += (apikey);
		}

		if(mMimopayLogoUrl == null) {
			mMimopayLogoUrl = shttp + surlpoint + "/addons/default/themes/bootstrap/img/sdk/logo.png";
		}
		if(mMimopayHelpLogoUrl == null) {
			mMimopayHelpLogoUrl = shttp + surlpoint + "/addons/default/themes/bootstrap/img/sdk/help_mark.png";
		}
		return surl;
	}

	// <domain/ip>/help/<btn_action>

	private String msHelpInput = "";
	public String msHelpOutput = "";
	public void retrievePaymentMethodHelp(String sbtnaction)
	{
		msHelpOutput = "";
		msHelpInput = sbtnaction;
		new RetrievePaymentMethodHelp().execute();
	}
	private class RetrievePaymentMethodHelp extends AsyncTask<Void, Void, Boolean> 
	{
        @Override protected void onPreExecute() {}
        @Override protected void onPostExecute(Boolean b) {}
        @Override protected void onCancelled() {}
        @Override protected Boolean doInBackground(Void... params)
		{
			int statusCode = -1;
			String shttp = "http://";
			String surlpoint = MimopayStuff.mMimopayUrlPoint;
			if(surlpoint.indexOf("gateway") != -1 || MimopayStuff.sChannel.equals("mpoint") || MimopayStuff.sChannel.equals("dpoint") || MimopayStuff.sChannel.equals("celcom")) {
				shttp = "https://";
			}
			String surlhelp = shttp + surlpoint + "/help/" + msHelpInput;
			jprintf("surlhelp " + surlhelp);

			try {
				HttpClient httpclient = new DefaultHttpClient();
				HttpConnectionParams.setConnectionTimeout(httpclient.getParams(), 60000);
				HttpConnectionParams.setSoTimeout(httpclient.getParams(), 60000);
				List<NameValuePair> nameValuePairsc = new ArrayList<NameValuePair>(1);
				nameValuePairsc.add(new BasicNameValuePair("json", "1"));
				HttpPost httppost = new HttpPost(surlhelp);
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairsc));
				HttpResponse response = httpclient.execute(httppost);
				StatusLine statusline = response.getStatusLine();
				statusCode = statusline.getStatusCode();
				if(statusCode != 200) {
					msHelpOutput = "error";
				} else {
					StringBuilder builder = new StringBuilder();
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) { builder.append(line + "\n"); }
					content.close();
					String sresult = builder.toString();
					jprintf("RetrievePaymentMethodHelp'sresult: " + sresult);
					JSONObject jsobj = new JSONObject(sresult);
					String sstep = jsobj.getString("step");
					JSONArray jarr = new JSONArray(sstep);
					int i,j = jarr.length();
					msHelpOutput = "";
					for(i=0;i<j;i++) {
						msHelpOutput += (jarr.getString(i) + "\n");
					}
				}
			} catch(Exception e) {
				jprintf("RetrievePaymentMethodHelp,Exception: " + e.toString());
				msHelpOutput = "error";
			}
			return true;
		}
	}

	public int mnRespondStatusCode = 0;
	public String msExceptionMessage = "";

	private class RetrieveMerchantPaymentMethod extends AsyncTask<Void, Void, Boolean> 
	{
        @Override protected void onPreExecute() {
			onProgress(true, "connecting");
		}
        @Override protected void onPostExecute(Boolean b) { onProgress(false, ""); }
        @Override protected void onCancelled() { onProgress(false, ""); }
        @Override protected Boolean doInBackground(Void... params)
		{
			String serror = "ErrorConnectingToMimopayServer";
			String merchantloadurl = assembleMerchantLoadUrl();
			jprintf("urlTarget: " + merchantloadurl);
			if(merchantloadurl == null) {
				onError("MerchantLoadUrlNull");
				return false;
			}

			int statusCode = -1;
			boolean bres = false;
			HttpClient httpclient = null;
			HttpPost httppost = null;
			HttpResponse response = null;
			StatusLine statusline = null;
			StringBuilder builder = null;
			HttpEntity entity = null;
			InputStream content = null;
			BufferedReader reader = null;
			String line,sresult;

			try {
				mnRespondStatusCode = 0;
				///////////////////////////////////////////////////////////////////////////
				// now get into the payment
				statusCode = -1;
				httpclient = new DefaultHttpClient();
				HttpConnectionParams.setConnectionTimeout(httpclient.getParams(), 60000);
				HttpConnectionParams.setSoTimeout(httpclient.getParams(), 60000);
				////////////////////////////////////////////////
				httppost = new HttpPost(merchantloadurl);
				response = httpclient.execute(httppost);
				statusline = response.getStatusLine();
				mnRespondStatusCode = statusCode = statusline.getStatusCode();
				////////////////////////////////////////////////
				// mnRespondStatusCode = statusCode = 200;
				////////////////////////////////////////////////
				if(statusCode != 200) {
					if(statusCode == 404) {
						serror = "ErrorHTTP404NotFound";
					}
				} else {
					////////////////////////////////////////////////
					builder = new StringBuilder();
					entity = response.getEntity();
					content = entity.getContent();
					reader = new BufferedReader(new InputStreamReader(content));
					while ((line = reader.readLine()) != null) { builder.append(line + "\n"); }
					content.close();
					sresult = builder.toString();
					////////////////////////////////////////////////
					// sresult ="[{\"payment\":{\"channelName\":\"smartfren\",\"channelStatus\":\"1\",\"postUrl\":\"http://staging.mimopay.com/smartfren_api/topup/1385479814/10_Coins/ID-0031/332960944/IDR/0/68f7876a22bac41a58394bb42ca4b903/\",\"logo\":\"http://staging.mimopay.com/addons/default/themes/bootstrap/img/sdk/smartfren.png\",\"btnAction\":\"smartfren\",\"denomPost\":\"\",\"denomKey\":[],\"denomValue\":[],\"code\":\"smartfren_code\"}},{\"payment\":{\"channelName\":\"sevelin\",\"channelStatus\":\"1\",\"postUrl\":\"http://staging.mimopay.com/sevelin_api/topup/1385479814/10_Coins/ID-0031/332960944/IDR/0/68f7876a22bac41a58394bb42ca4b903/\",\"logo\":\"http://staging.mimopay.com/addons/default/themes/bootstrap/img/sdk/sevelin.png\",\"btnAction\":\"sevelin\",\"denomPost\":\"\",\"denomKey\":[],\"denomValue\":[],\"code\":\"sevelin_code\"}}]";
					// sresult = "{\"payment\":{\"channelName\":{\"vnp\":\"VinaPhone\",\"vms\":\"MobiFone\",\"vte\":\"Viettel\"},\"channelStatus\":\"1\",\"postUrl\":\"https://gtower.mimopay.com/topup_vn_api/topup/1385479814/10_Coins/ID-0031/1791177702/IDR/0/72a9cf75713d350ef47a6462a0c60eb3/\",\"btnAction\":\"topup_vn\",\"telco\":\"telco\",\"topup_serial\":\"topup_serial\",\"topup_hrn\":\"topup_hrn\",\"topup_mdn\":\"topup_mdn\",\"topup_email\":\"topup_email\"}}";
					////////////////////////////////////////////////
					int na = sresult.indexOf("[{");
					int nb = sresult.lastIndexOf("}]");
					if(na != -1 && nb != -1) {
						String sc = sresult.substring(na, nb+2);
						sresult = sc;
					}
					jprintf("RetrieveMerchantPaymentMethod'result: " + sresult);
					if(parseMimopayResponse(sresult)) {
						bres = true;
						// if(mActivity !=null) {
						// 	if(!makesureImage(mMimopayLogoUrl)) 
						// 		bres = false;
						// 	if(!makesureImage(mMimopayHelpLogoUrl)) 
						// 		bres = false;
						// 	for(int i=0;i<alsLogos.size();i++) {
						// 		if(!makesureImage(alsLogos.get(i)))
						// 			bres = false;
						// 	}
						// }
						// if(!mbChannelStatusOk) {
						// 	serror = "ErrorChannelIsNotReady";
						// 	bres = false;
						// }
					} else {
						// not Topup, Airtime, or XL
						serror = "UnsupportedPaymentMethod";
					}
				}
			} catch(Exception e) {
				String se = e.toString();
				int a = se.lastIndexOf(": ");
				if(a >= 0) {
					msExceptionMessage = se.substring(a+2);
				} else {
					a = se.lastIndexOf(".");
					if(a >= 0) {
						msExceptionMessage = se.substring(a+1);
					} else {
					}
				}
				jprintf("RetrieveMerchantPaymentMethod,Exception: " + e.toString());
				mnRespondStatusCode = -1;
				// disini harusnya show error tertentu...!!!!
			}

			if(bres) {
				onMerchantPaymentMethodRetrieved();
			} else {
				jprintf(String.format("RetrieveMerchantPaymentMethod: serror = %s, statusCode = %d", serror, statusCode));
				onError(serror);
			}

			return bres;
		}
	}

	private boolean parseMimopayResponse(String sresp)
	{
		boolean bres = false;

		if(alsLogos == null)
			alsLogos = new ArrayList<String>();
		if(alsLabels == null)
			alsLabels = new ArrayList<String>();
		if(alsBtnAction == null)
			alsBtnAction = new ArrayList<String>();
		if(alsUrls == null)
			alsUrls = new ArrayList<String>();
		if(alsDenomKey == null)
			alsDenomKey = new ArrayList<String>();
		if(alsDenomValue == null)
			alsDenomValue = new ArrayList<String>();
		if(alsDenomAmount == null)
			alsDenomAmount = new ArrayList<String>();
		if(alsStringTnc == null)
			alsStringTnc = new ArrayList<String>();
		if(alsTextEditName == null)
			alsTextEditName = new ArrayList<String>();
		if(alsDenomPost == null)
			alsDenomPost = new ArrayList<String>();
		if(alsChannelStatus == null)
			alsChannelStatus = new ArrayList<String>();

		bres = parseMimopayGrabJson(sresp);
		printAls();
		return bres;
	}

	private boolean parseMimopayGrabJson(String sjson)
	{
		int i,j;
		String s,t;

		if(MimopayStuff.sPaymentMethod.equals("VnTelco")) {
			try {
				JSONObject jsobj = new JSONObject(sjson);
				t = jsobj.getString("payment");
				JSONObject jobjsub = new JSONObject(t);
				// if(isKeyAvail(jobjsub, "channelStatus")) {
				// 	s = jobjsub.getString("channelStatus");
				// 	mbChannelStatusOk = s.equals("1");
				// }
				alsChannelStatus.add(jobjsub.getString("channelStatus"));
				JSONObject jobjchname = new JSONObject(jobjsub.getString("channelName"));
				Iterator<?> keys = jobjchname.keys();
				while(keys.hasNext()) {
				    s = (String)keys.next();
				    alsLabels.add(jobjchname.getString(s));
					alsTextEditName.add(s);
					alsBtnAction.add(jobjsub.getString("btnAction"));
					alsUrls.add(jobjsub.getString("postUrl"));
					alsDenomKey.add("");
					alsDenomValue.add("");
					alsDenomAmount.add("");
					alsStringTnc.add("");
					alsDenomPost.add("");
				}
				int a,b=alsLabels.size();
				s = "https://" + MimopayStuff.mMimopayUrlPoint + "/addons/default/themes/bootstrap/img/sdk/[channelName].png";
				for(a=0;a<b;a++) {
					t = alsLabels.get(a).toLowerCase();
					alsLogos.add(s.replace("[channelName]", t));
					jprintf(String.format("alsLabels[%d]=%s",a,t));
				}
				// alsUrls.add(jobjsub.getString("postUrl"));
				//alsBtnAction.add(jobjsub.getString("btnAction"));
				// alsDenomKey.add("[]");
				// alsDenomValue.add("[]");
				// i = MimopayStuff.sChannel.lastIndexOf("_");
				// t = MimopayStuff.sChannel.substring(i+1);
				// // alsLabels.add(t);
				// alsDenomPost.add("");
				// jprintf("parseMimopayGrabJson'alsDenomPost: " + t);
				return true;
			} catch(JSONException e) {
				jprintf("parseMimopayGrabJson'exception: " + e.toString());
			}
			return false;
		}

		char[] ch = sjson.toCharArray();
		if(ch[0] != '[' && ch[ch.length-1] != ']') {
			s = "[" + sjson + "]";
			sjson = s;
			jprintf("make sjson as array");
		}

		// for testing channelStatus only
		// String as = sjson.replace("\"sevelin\",\"channelStatus\":\"1\"", "\"sevelin\",\"channelStatus\":\"0\"");
		// sjson = as;

		try {
			JSONArray jarr = new JSONArray(sjson);
			j = jarr.length();
			for(i=0;i<j;i++) {
				s = jarr.getString(i);
				JSONObject jsobj = new JSONObject(s);
				t = jsobj.getString("payment");
				JSONObject jobjsub = new JSONObject(t);
				// if(isKeyAvail(jobjsub, "channelStatus")) {
				// 	s = jobjsub.getString("channelStatus");
				// 	mbChannelStatusOk = s.equals("1");
				// }
				alsChannelStatus.add(jobjsub.getString("channelStatus"));
				alsLogos.add(jobjsub.getString("logo"));
				alsUrls.add(jobjsub.getString("postUrl"));
				alsBtnAction.add(jobjsub.getString("btnAction"));
				t = jobjsub.getString("code");
				jprintf("parseMimopayGrabJson'alsTextEditName: " + t);
				alsTextEditName.add(t);
				alsDenomKey.add(jobjsub.getString("denomKey"));
				alsDenomValue.add(jobjsub.getString("denomValue"));
				if(isKeyAvail(jobjsub, "denomAmount")) {
					JSONArray jr = new JSONArray();
					JSONArray ja = new JSONArray(jobjsub.getString("denomKey"));
					JSONObject jo = new JSONObject(jobjsub.getString("denomAmount"));
					for(int a=0;a<ja.length();a++) {
						if(isKeyAvail(jo, ja.getString(a))) {
							jr.put(Integer.toString(jo.getInt(ja.getString(a))));
						} else {
							jr.put(ja.getString(a));
						}
					}
					alsDenomAmount.add(jr.toString());
				} else {
					alsDenomAmount.add(jobjsub.getString("denomValue"));
				}
				if(isKeyAvail(jobjsub, "stringTnc")) {
					alsStringTnc.add(jobjsub.getString("stringTnc"));
				} else {
					alsStringTnc.add("");
				}
				alsLabels.add(jobjsub.getString("channelName"));
				t = jobjsub.getString("denomPost");
				alsDenomPost.add(t);
				jprintf("parseMimopayGrabJson'alsDenomPost: " + t);
			}
			return true;
		} catch(JSONException e) {
			jprintf("parseMimopayGrabJson'exception: " + e.toString());
		}
		return false;
	}

	public void printAls()
	{
		int j = alsBtnAction.size(); for(int i=0; i<j; i++) jprintf(String.format("<alsBtnAction>size:%d, idx:%d, als:%s", j, i, alsBtnAction.get(i)));
		j = alsLogos.size(); for(int i=0; i<j; i++) jprintf(String.format("<alsLogos>size:%d, idx:%d, als:%s", j, i, alsLogos.get(i)));
		j = alsUrls.size(); for(int i=0; i<j; i++) jprintf(String.format("<alsUrls>size:%d, idx:%d, als:%s", j, i, alsUrls.get(i)));
		j = alsDenomKey.size(); for(int i=0; i<j; i++) jprintf(String.format("<alsDenomKey>size:%d, idx:%d, als:%s", j, i, alsDenomKey.get(i)));
		j = alsDenomValue.size(); for(int i=0; i<j; i++) jprintf(String.format("<alsDenomValue>size:%d, idx:%d, als:%s", j, i, alsDenomValue.get(i)));
		j = alsDenomAmount.size(); for(int i=0; i<j; i++) jprintf(String.format("<alsDenomAmount>size:%d, idx:%d, als:%s", j, i, alsDenomAmount.get(i)));
		j = alsStringTnc.size(); for(int i=0; i<j; i++) jprintf(String.format("<alsStringTnc>size:%d, idx:%d, als:%s", j, i, alsStringTnc.get(i)));
		j = alsTextEditName.size(); for(int i=0; i<j; i++) jprintf(String.format("<alsTextEditName>size:%d, idx:%d, als:%s", j, i, alsTextEditName.get(i)));
		j = alsDenomPost.size(); for(int i=0; i<j; i++) jprintf(String.format("<alsDenomPost>size:%d, idx:%d, als:%s", j, i, alsDenomPost.get(i)));
	}

	public static boolean isNumeric(String str)
	{
		for(char c : str.toCharArray()) {
			if(!Character.isDigit(c)) return false;
		}
		return true;
	}

	public static String getFnameFromUrl(String url)
	{
		if(url.equals("")) {
			return "";
		}
		int slashIndex = url.lastIndexOf('/');
		if(slashIndex < 0) {
			return null;
		}
		return url.substring(slashIndex+1);
	}

	public static boolean isFileOkay(String filepath)
	{
		File file = new File(filepath);
		if(file.exists()) {
			if(file.length() == 0) {
				file.delete();	// file 0 kb is meaningless!!!
				return false;
			}
			return true;
		}
		return false;
	}
	
	// private boolean makesureImage(String imageurl)
	// {
	// 	String scoba = downloadFromUrl(imageurl);
	// 	if(scoba != null) {
	// 		jprintf(String.format("Failed to download %s. Reason: %s", imageurl, scoba));
	// 	} else {
	// 		jprintf("Success download " + imageurl);
	// 	}

	// 	return true;
	// }
	
	public Bitmap getBitmapInternal(String urlpath)
	{
		Bitmap rbmp = null;
		FileInputStream fis = null;
		String pathname = getFnameFromUrl(urlpath);
		jprintf("getBitmapInternal'pathname: " + pathname);
		try {
			fis = mActivity.openFileInput(pathname);
			rbmp = BitmapFactory.decodeStream(fis);
			fis.close();
		} catch (FileNotFoundException e) {
			jprintf("getBitmapInternal: e=" + e.toString());
		} catch (IOException e) {
			jprintf("getBitmapInternal: e=" + e.toString());
		}  
		return rbmp;
 	}

	// /data/data/com.bayninestudios.androidogremesh/files
	// private String downloadFromUrl(String urlpath)
	// {
	// 	String serror = null;
	// 	String pathname = getFnameFromUrl(urlpath);

	// 	File file = mActivity.getFileStreamPath(pathname);
	// 	if(file.exists()) {
	// 		serror = "FileExistAlready";
	// 		return serror;
	// 	}

	// 	if(!URLUtil.isNetworkUrl(urlpath)) {
	// 		serror = "ErrorNetworkUrlInvalid";
	// 		return serror;
	// 	}

	// 	try {
	// 		URL url = new URL(urlpath);
	// 		URLConnection cn = url.openConnection();
	// 		cn.connect();
	// 		InputStream stream = cn.getInputStream();
	// 		if(stream == null) {
	// 			serror = "ErrorStreamIsNull";
	// 			return serror;
	// 		}
	// 		FileOutputStream out = mActivity.openFileOutput(pathname, mActivity.MODE_PRIVATE);
	// 		byte buf[] = new byte[512];
	// 		do {
	// 			int numread = stream.read(buf);
	// 			if(numread <= 0)
	// 				break;
	// 			out.write(buf, 0, numread);
	// 		} while (true);
	// 		stream.close();
	// 		out.close();
	// 	} catch (IOException ex) {
	// 		serror = "ErrorImageDownloadStreamClose";
	// 	} catch(Exception e) {
	// 		serror = "downloadFromUrl,e: " + e.toString();
	// 	}
	// 	return serror;
	// }

	// v1.0.1
	public boolean setChannelActiveIndex(String channel)
	{
		jprintf("setChannelActiveIndex:channel: " + channel);
		String s;
		int i,j = alsBtnAction.size();
		for(i=0;i<j;i++) {
			s = alsBtnAction.get(i);
			jprintf("setChannelActiveIndex:alsBtnAction[i]: " + s);
			if(s.equals(channel)) {
				mAlsActiveIdx = i;
				return true;
			}
		}
		return false;
	}

	public void executeBtnAction()
	{
		new btnActionPost().execute();
	}
	
	public class btnActionPost extends AsyncTask<Void, Void, Boolean> 
	{
        @Override protected void onPreExecute() {
			onProgress(true, "validating");
		}
        @Override protected void onPostExecute(Boolean b) { onProgress(false, ""); }
        @Override protected void onCancelled() { onProgress(false, ""); }
		@SuppressWarnings("unchecked")
        @Override protected Boolean doInBackground(Void... params)
		{
			mnRespondStatusCode = 0;
			jprintf(" mHttppostCode: " + mHttppostCode + " mAlsActiveIdx: " + Integer.toString(mAlsActiveIdx));
			if(mAlsActiveIdx == -1) {
				jprintf("ArrayList index shouldn't be -1");
				onError("UnspecifiedChannelRequest");		// v1.0.1
				return false;
			} 
			int resstat = PAYMENTRESULT_INTERNETPROBLEM;
			String url = alsUrls.get(mAlsActiveIdx); 
			jprintf("url: " + url);
			String paymentname = alsBtnAction.get(mAlsActiveIdx); 
			jprintf("paymentname: " + paymentname);
			String texteditname = ""; 
			if(alsTextEditName != null && alsTextEditName.size() > mAlsActiveIdx) { 
				texteditname = alsTextEditName.get(mAlsActiveIdx); 
			} 
			jprintf("texteditname: " + texteditname);
			String sdenompost = alsDenomPost.get(mAlsActiveIdx); 
			jprintf("sdenompost: " + sdenompost);

			// check phone number
			if(paymentname.equals("upoint_airtime") || paymentname.equals("xl_airtime")) {
				if(Mimopay.isPhoneNumberInvalid(mHttppostPhoneNumber)) {
					onError("ErrorInvalidPhoneNumber");
					return false;
				}

			}

			String se = null;
			int statusCode = -1;
			int nvalpair;
			try { 
				HttpClient httpclientc = new DefaultHttpClient();
				HttpConnectionParams.setConnectionTimeout(httpclientc.getParams(), 60000);
				HttpConnectionParams.setSoTimeout(httpclientc.getParams(), 60000);
				HttpPost httppostc = null;

				// Add your data
				List<NameValuePair> nameValuePairsc = null;
				if(paymentname.equals("smartfren") || paymentname.equals("sevelin") || paymentname.equals("xl_hrn")  || paymentname.equals("upoint_hrn") || paymentname.equals("dpoint_airtime_charge")) {
					nvalpair = 3;
					if(paymentname.equals("dpoint_airtime_charge")) nvalpair = 5;
					//nvalpair = 2; if(MimopayStuff.mEnableJson) nvalpair++;
					nameValuePairsc = new ArrayList<NameValuePair>(nvalpair);
					nameValuePairsc.add(new BasicNameValuePair("btnAction", paymentname));
					nameValuePairsc.add(new BasicNameValuePair(texteditname, mHttppostCode));
					if(paymentname.equals("dpoint_airtime_charge")) {
						String dpointMdn = alsDenomKey.get(0);
						String denomValue = alsDenomValue.get(0);
						jprintf(String.format("paymentname:%s, mHttppostCode:%s, mHttppostPhoneNumber:%s, denomValue:%s", paymentname, mHttppostCode, mHttppostPhoneNumber, denomValue));
						//jprintf(String.format("paymentname:%s, mHttppostCode:%s, mHttppostPhoneNumber:%s", paymentname, mHttppostCode, mHttppostPhoneNumber));
						nameValuePairsc.add(new BasicNameValuePair(dpointMdn, mHttppostPhoneNumber));	// dpoint_mdn
						nameValuePairsc.add(new BasicNameValuePair(sdenompost, denomValue));	
					}
					//if(MimopayStuff.mEnableJson)
					nameValuePairsc.add(new BasicNameValuePair("json", "1"));
					httppostc = new HttpPost(url);
					httppostc.setEntity(new UrlEncodedFormEntity(nameValuePairsc));
				//} else if(paymentname.equals("atm_bca")) {
				} else if(paymentname.indexOf("atm_") != -1) {
					httppostc = new HttpPost(url);
					nvalpair = 3;
					//nvalpair = 2; if(MimopayStuff.mEnableJson) nvalpair++;
					nameValuePairsc = new ArrayList<NameValuePair>(nvalpair);
					nameValuePairsc.add(new BasicNameValuePair("btnAction", paymentname));
					nameValuePairsc.add(new BasicNameValuePair("denom", mHttppostCode));
					//if(MimopayStuff.mEnableJson)
					nameValuePairsc.add(new BasicNameValuePair("json", "1"));
					httppostc.setEntity(new UrlEncodedFormEntity(nameValuePairsc));
				} else if(paymentname.equals("upoint_airtime") || paymentname.equals("xl_airtime") || paymentname.equals("mpoint_airtime") || paymentname.equals("dpoint_airtime") || paymentname.equals("celcom_airtime") || paymentname.equals("indosat_airtime")) {
					jprintf("mHttppostPhoneNumber: " + mHttppostPhoneNumber);
					httppostc = new HttpPost(url);
					nvalpair = 4;
					// added new param, upoint_item
					if(paymentname.equals("upoint_airtime") && !MimopayStuff.sUpointItem.equals("")) {
						nvalpair = 5;
					}
					//nvalpair = 3; if(MimopayStuff.mEnableJson) nvalpair++;
					nameValuePairsc = new ArrayList<NameValuePair>(nvalpair);
					nameValuePairsc.add(new BasicNameValuePair("btnAction", paymentname));
					nameValuePairsc.add(new BasicNameValuePair(texteditname, mHttppostPhoneNumber));
					//nameValuePairsc.add(new BasicNameValuePair(mHttppostDenom, mHttppostCode));
					nameValuePairsc.add(new BasicNameValuePair(sdenompost, mHttppostCode));
					//if(MimopayStuff.mEnableJson)
					nameValuePairsc.add(new BasicNameValuePair("json", "1"));
					// added new param, upoint_item
					if(paymentname.equals("upoint_airtime") && !MimopayStuff.sUpointItem.equals("")) {
						nameValuePairsc.add(new BasicNameValuePair("upoint_item", MimopayStuff.sUpointItem));
					}
					for(int i=0;i<nameValuePairsc.size();i++) {
						jprintf(String.format("nameValuePairsc[%d]=%s",i,nameValuePairsc.get(i)));
					}
					httppostc.setEntity(new UrlEncodedFormEntity(nameValuePairsc));
				} else if(MimopayStuff.sPaymentMethod.equals("VnTelco")) {
					String[] starr = null;
					try {
						JSONArray jarr = new JSONArray(mHttppostCode);
						int i,j=jarr.length();
						starr = new String[j];
						for(i=0;i<j;i++) starr[i] = jarr.getString(i);
					} catch(JSONException e) {}
					if(starr == null) {
						onError("UnsupportedPaymentMethod");
						return false;
					}
					jprintf("vntelco: " + mHttppostCode);
					jprintf("btnAction: " + paymentname);
					jprintf("postUrl: " + url);
					nameValuePairsc = new ArrayList<NameValuePair>(7);
					nameValuePairsc.add(new BasicNameValuePair("btnAction", paymentname));
					nameValuePairsc.add(new BasicNameValuePair("telco", starr[0]));
					nameValuePairsc.add(new BasicNameValuePair("topup_serial", starr[1]));
					nameValuePairsc.add(new BasicNameValuePair("topup_hrn", starr[2]));
					nameValuePairsc.add(new BasicNameValuePair("topup_mdn", starr[3]));
					nameValuePairsc.add(new BasicNameValuePair("topup_email", starr[4]));
					nameValuePairsc.add(new BasicNameValuePair("json", "1"));
					httppostc = new HttpPost(url);
					httppostc.setEntity(new UrlEncodedFormEntity(nameValuePairsc));
					//onError("UnsupportedPaymentMethod");
				} else {
					jprintf("Unsupported Payment Method: " + paymentname);
					onError("UnsupportedPaymentMethod");
					return false;
				}

				////////////////////////////////////////////////////////////////////////////////
				HttpResponse responsec = httpclientc.execute(httppostc);
				StatusLine statusLinec = responsec.getStatusLine();
				mnRespondStatusCode = statusCode = statusLinec.getStatusCode();
				////////////////////////////////////////////////////////////////////////////////
				// statusCode = 200;
				////////////////////////////////////////////////////////////////////////////////
				jprintf("btnActionPost,statusCode: " + Integer.toString(statusCode));
				if(statusCode == 200) {
					// read return
					////////////////////////////////////////////////////////////////////////////////
					StringBuilder builderc = new StringBuilder();
					HttpEntity entityc = responsec.getEntity();
					InputStream contentc = entityc.getContent();
					BufferedReader readerc = new BufferedReader(new InputStreamReader(contentc));
					String linec;
					while ((linec = readerc.readLine()) != null) {
						builderc.append(linec + "\n");
					}
					contentc.close();
					String sresultc = builderc.toString();
					////////////////////////////////////////////////////////////////////////////////
					// String sresultc = "{\"result\":{\"transactionId\":\"010009736140\",\"statusInfo\":\"OK\",\"statusMessage\":\"Sukses\",\"remarks\":\"Mohon mencatat ID transaksi anda sebagai referensi. Untuk informasi lebih lanjut silahkan hubungi SmartFren. Apabila kamu adalah pengguna Smartfren, hubungi 888. Untuk pengguna lainya silahkan hubungi 08811223344 atau 50100000 atau kunjungi smartfren.com untuk informasi lebih lanjut\",\"smsCode\":\"\",\"shortCode\":\"\",\"companyCode\":\"\",\"totalAmount\":\"\",\"atmTransactionId\":\"\"}}";
					////////////////////////////////////////////////////////////////////////////////
					jprintf("btnActionPost,sresultc: " + sresultc);
					if(sresultc.indexOf("dontpanic.jpg") != -1) {
						resstat = PAYMENTRESULT_UNDERMAINTENANCE;
					} else {
						resstat = decidePaymentMethodProcess(sresultc);
					}
				}
			} catch(SocketTimeoutException e) {
				se = e.toString();
			} catch(Exception e) {
				se = e.toString();
			}

			if(se != null) {
				jprintf("Exception: " + se);
				int a = se.lastIndexOf(": ");
				if(a >= 0) {
					msExceptionMessage = se.substring(a+2);
				} else {
					a = se.lastIndexOf(".");
					if(a >= 0) {
						msExceptionMessage = se.substring(a+1);
					} else {
					}
				}
				mnRespondStatusCode = -1;
			}

			switch(resstat)
			{
			case PAYMENTRESULT_VALIDATEERROR:
				onError("ErrorValidatingVoucherCode");
				return false;
			case PAYMENTRESULT_INTERNETPROBLEM:
				onError("ErrorConnectingToMimopayServer");
				return false;
			case PAYMENTRESULT_UNDERMAINTENANCE:
				onError("ErrorUnderMaintenance");
				return false;
			// case PAYMENTRESULT_CHANNELISNOTREADY:
			// 	onError("ErrorChannelIsNotReady");
			// 	return false;
			case PAYMENTRESULT_OK:
				return true;
			}

			return false;
		}
	}

	private int decidePaymentMethodProcess(String jsonstr)
	{
		String type = alsBtnAction.get(mAlsActiveIdx);
		jprintf("decidePaymentMethodProcess:type: " + type);
		JSONObject json = null;
		ArrayList<String> als = null;
		String sstat = "";

		///////////////////////////////////////////////////////////////////////
		// smartfren, sevelin, ....
		if(type.equals("smartfren") || type.equals("sevelin") || type.equals("xl_hrn") || type.equals("upoint_hrn") || type.equals("topup_vn")) {
			try {
				json = new JSONObject(jsonstr);
				sstat = json.getString("result");
				json = new JSONObject(sstat);
				als = new ArrayList<String>();
				//String s = json.getString("statusMessage");
				als.add(json.getString("statusInfo"));
				//als.add(json.getString("statusMessage"));
				String sstatmsg = json.getString("statusMessage");
				String hstatmsg = Html.fromHtml((String) sstatmsg).toString();
				als.add(hstatmsg);
				//als.add(json.getString("transactionId"));
				MimopayStuff.saveLastResult("lastpayresult", als);
				onResultUI(type, als);
				return PAYMENTRESULT_OK;
			} catch(JSONException e) {
				jprintf("JSONException,alsTextEditName: " + e.toString());
				return PAYMENTRESULT_INTERNETPROBLEM;
			}
		///////////////////////////////////////////////////////////////////////
		// ATMs
		} else if(type.indexOf("atm_") != -1) {
			try {
				json = new JSONObject(jsonstr);
				sstat = json.getString("result");
				json = new JSONObject(sstat);
				als = new ArrayList<String>();
				als.add(json.getString("companyCode"));
				als.add(json.getString("totalAmount"));
				als.add(json.getString("atmTransactionId"));
				int nsize = als.size();
				MimopayStuff.saveLastResult("lastpayresult", als);
				onResultUI(type, als);
				return PAYMENTRESULT_OK;
			} catch(JSONException e) {
				jprintf("JSONException,atm_: " + e.toString());
				return PAYMENTRESULT_INTERNETPROBLEM;
			}
		///////////////////////////////////////////////////////////////////////
		// upoint, ....
		} else if(type.equals("upoint_airtime")) {
			try {
				json = new JSONObject(jsonstr);
				sstat = json.getString("result");
				json = new JSONObject(sstat);
				als = new ArrayList<String>();
				String sstatinfo = json.getString("statusInfo");
				if(sstatinfo.equals("ERROR")) {
					als.add("ERROR");
					als.add(json.getString("statusMessage"));
				} else {
					als.add(json.getString("smsCode"));
					als.add(json.getString("shortCode"));
					als.add(json.getString("companyCode"));
				}
				MimopayStuff.saveLastResult("lastpayresult", als);
				onResultUI(type, als);
				return PAYMENTRESULT_OK;
			} catch(JSONException e) {
				jprintf("JSONException,alsTextEditName: " + e.toString());
				return PAYMENTRESULT_INTERNETPROBLEM;
			}
		///////////////////////////////////////////////////////////////////////
		// xl_airtime
		} else if(type.equals("xl_airtime")) {
			try {
				json = new JSONObject(jsonstr);
				sstat = json.getString("result");
				json = new JSONObject(sstat);
				sstat = json.getString("statusInfo");
				String sstatmsg = json.getString("statusMessage");
				String hstatmsg = Html.fromHtml((String) sstatmsg).toString();
				String tstatmsg = sstat.equals("ERROR") ? hstatmsg : sstatmsg;
				als = new ArrayList<String>();
				als.add(sstat);
				als.add(tstatmsg);
				MimopayStuff.saveLastResult("lastpayresult", als);
				onResultUI(type, als);
				return PAYMENTRESULT_OK;
			} catch(JSONException e) {
				jprintf("JSONException,alsTextEditName: " + e.toString());
				return PAYMENTRESULT_INTERNETPROBLEM;
			}
		///////////////////////////////////////////////////////////////////////
		// indosat_airtime
		} else if(type.equals("indosat_airtime")) {
// D/JimBas  ( 7198): MimopayCore: btnActionPost,sresultc: {"result":{"transactionId":"190018295285","statusInfo":"OK","statusMessage":"Sukses","remarks":"","smsCode":"","shortCode":"99231","keyWord":"BELI","companyCode":"","totalAmount":"","atmTransactionId":""}}
// D/JimBas  ( 7198): MimopayCore: decidePaymentMethodProcess:type: indosat_airtime
			try {
				json = new JSONObject(jsonstr);
				sstat = json.getString("result");
				json = new JSONObject(sstat);
				sstat = json.getString("statusInfo");
				String sstatmsg = json.getString("statusMessage");
				String hstatmsg = Html.fromHtml((String) sstatmsg).toString();
				String tstatmsg = sstat.equals("ERROR") ? hstatmsg : sstatmsg;
				als = new ArrayList<String>();
				als.add(sstat);
				als.add(tstatmsg);
				mHttppostCode = json.getString("keyWord");
				MimopayStuff.saveLastResult("lastpayresult", als);
				onResultUI(type, als);
				return PAYMENTRESULT_OK;
			} catch(JSONException e) {
				jprintf("JSONException,alsTextEditName: " + e.toString());
				return PAYMENTRESULT_INTERNETPROBLEM;
			}
		///////////////////////////////////////////////////////////////////////
		// mpoint_airtime
		} else if(type.equals("mpoint_airtime")) {
			try {
				json = new JSONObject(jsonstr);
				sstat = json.getString("result");
				json = new JSONObject(sstat);
				sstat = json.getString("statusInfo");
				String sstatmsg = json.getString("statusMessage");
				String hstatmsg = Html.fromHtml((String) sstatmsg).toString();
				String tstatmsg = sstat.equals("FAILED") ? hstatmsg : sstatmsg;
				als = new ArrayList<String>();
				als.add(sstat);
				als.add(tstatmsg);
				MimopayStuff.saveLastResult("lastpayresult", als);
				onResultUI(type, als);
				return PAYMENTRESULT_OK;
			} catch(JSONException e) {
				jprintf("JSONException,alsTextEditName: " + e.toString());
				return PAYMENTRESULT_INTERNETPROBLEM;
			}
		///////////////////////////////////////////////////////////////////////
		// dpoint_airtime
		} else if(type.equals("dpoint_airtime")) {
			try {
				json = new JSONObject(jsonstr);
				// please note that this is special for DPoint because the payment
				// flow is different than upoint, xl, or maxis
				if(isKeyAvail(json, "result")) {
					sstat = json.getString("result");
					//json = new JSONObject(jsonstr);
					//sstat = json.getString("result");
					json = new JSONObject(sstat);
					sstat = json.getString("statusInfo");
					String sstatmsg = json.getString("statusMessage");
					String hstatmsg = Html.fromHtml((String) sstatmsg).toString();
					String tstatmsg = sstat.equals("ERROR") ? hstatmsg : sstatmsg;
					//als = new ArrayList<String>();
					//als.add(sstat);
					//als.add(tstatmsg);
					//onResultUI(type, als);
					mnRespondStatusCode = -1;
					msExceptionMessage = tstatmsg;
					return PAYMENTRESULT_INTERNETPROBLEM;
				}
				sstat = json.getString("charge");
				json = new JSONObject(sstat);
				als = new ArrayList<String>();
				als.add(json.getString("postUrl"));
				als.add(json.getString("btnAction"));
				als.add(json.getString("code"));
				als.add(json.getString("mdn"));
				als.add(json.getString("denom"));
				als.add(mHttppostCode);
				als.add(mHttppostPhoneNumber);
				onResultUI(type, als);
				//MimopayStuff.saveLastResult("dpointpay", als);
				return PAYMENTRESULT_OK;
			} catch(JSONException e) {
				jprintf("JSONException,alsTextEditName: " + e.toString());
				return PAYMENTRESULT_INTERNETPROBLEM;
			}
		} else if(type.equals("dpoint_airtime_charge")) {
			try {
				json = new JSONObject(jsonstr);
				sstat = json.getString("result");
				json = new JSONObject(sstat);
				sstat = json.getString("statusInfo");
				//String sstatmsg = json.getString("statusMessage");
				//String hstatmsg = Html.fromHtml((String) sstatmsg).toString();
				String tstatmsg = sstat.equals("ERROR") ? "Invalid SMS Code" : "Top up Success";
				als = new ArrayList<String>();
				als.add(sstat);
				als.add(tstatmsg);
				MimopayStuff.saveLastResult("lastpayresult", als);
				onResultUI(type, als);
				return PAYMENTRESULT_OK;
			} catch(JSONException e) {
				jprintf("JSONException,alsTextEditName: " + e.toString());
				return PAYMENTRESULT_INTERNETPROBLEM;
			}
		///////////////////////////////////////////////////////////////////////
		// celcom_airtime
		} else if(type.equals("celcom_airtime")) {
			try {
				json = new JSONObject(jsonstr);
				sstat = json.getString("result");
				json = new JSONObject(sstat);
				als = new ArrayList<String>();
				String sstatinfo = json.getString("statusInfo");
				if(sstatinfo.equals("ERROR")) {
					als.add("ERROR");
					als.add(json.getString("statusMessage"));
				} else {
					als.add(json.getString("smsCode"));
					als.add(json.getString("shortCode"));
				}
				MimopayStuff.saveLastResult("lastpayresult", als);
				onResultUI(type, als);
				return PAYMENTRESULT_OK;
			} catch(JSONException e) {
				jprintf("JSONException,alsTextEditName: " + e.toString());
				return PAYMENTRESULT_INTERNETPROBLEM;
			}
		///////////////////////////////////////////////////////////////////////
		// else...
		///////////////////////////////////////////////////////////////////////
		} else {
		}

		return PAYMENTRESULT_VALIDATEERROR;
	}

	@SuppressWarnings("unchecked")
	private boolean isKeyAvail(JSONObject jsobj, String reqkey)
	{
		Iterator<String> keys = jsobj.keys();
		while(keys.hasNext()) {
			String skey = keys.next();
			if(skey.equals(reqkey)) return true;
		}
		jprintf("unavailable'key: " + reqkey);
		return false;
	}

	private long mCurMillis = 0;
	private int mTimeout = 0;
	//private String mSMSDialogCaption = "";
	public Activity mActivity = null;
	private boolean mOnWaitSMS = false;
	
	public void sendSMS(String smsMessage, String phoneNumber)
	{
		//mSMSDialogCaption = sdlgcap;
		mnRespondStatusCode = 0;
		String[] sendSMSTaskParams = { smsMessage, phoneNumber };
		new SendSMSTask().execute(sendSMSTaskParams);
	}

	private class SendSMSTask extends AsyncTask<String, Void, Boolean> 
	{
		String smsmessage = null;
		String dstphonenum = null;
        @Override protected void onPreExecute() {
			//onProgress(true, mSMSDialogCaption, "Please wait while sending SMS...");
			onProgress(true, "sendsms");
		}
        @Override protected void onPostExecute(Boolean b) {
			onProgress(false, "");
			ArrayList<String> als = new ArrayList<String>();
			als.add("sent");
			als.add(smsmessage);
			als.add(dstphonenum);
			onSmsCompleted(b, als);
		}
        @Override protected void onCancelled() { onProgress(false, ""); }
        @Override protected Boolean doInBackground(String... params)
		{
			smsmessage = params[0];
			dstphonenum = params[1];
			jprintf("[SendSMSTask] smsmessage: " + smsmessage + " dstphonenum: " + dstphonenum);
			SmsManager smsmgr = SmsManager.getDefault();
			if(smsmgr != null) {
				smsmgr.sendTextMessage(dstphonenum, null, smsmessage, null, null);
				try { Thread.sleep(3000); } catch (InterruptedException e) {}
				return true;
			}
			jprintf("Failed to send SMS");
			return false;
		}
	}

	//public void waitSMS(/*String sdlgcap,*/Activity activity, int ntimeout)
	public void waitSMS(int ntimeout)
	{
		// return sms2 yg hari ini jam yg sama
		//mSMSDialogCaption = sdlgcap;
		//mActivity = activity;		
		mCurMillis = System.currentTimeMillis();
		mTimeout = ntimeout;
		mOnWaitSMS = true;
		new WaitSMSTask().execute();
	}

	public void waitSMS(int ntimeout, boolean btime)
	{
		// return sms2 yg hari ini jam yg sama
		//mSMSDialogCaption = sdlgcap;
		//mActivity = activity;
		if(btime) mCurMillis = System.currentTimeMillis();
		mTimeout = ntimeout;
		mOnWaitSMS = true;
		new WaitSMSTask().execute();
	}

	public void stopWaitSMS()
	{
		mOnWaitSMS = false;
	}

	private class WaitSMSTask extends AsyncTask<String, Void, Boolean> 
	{
		ArrayList<String> als = null;
        @Override protected void onPreExecute() {
			onProgress(true, "waitsms");
		}
        @Override protected void onPostExecute(Boolean b) {
			onProgress(false, "");
			onSmsCompleted(b, als);
		}
        @Override protected void onCancelled() { onProgress(false, ""); }
        @Override protected Boolean doInBackground(String... params)
		{ 
			mCl = null;
			for(int i=0;i<mTimeout;i++) {
				als = null;
				if(querySms()) { 
					onSmsCompleted(true, als);
				} 
				try{Thread.sleep(500);}catch(Exception e){}
				if(!mOnWaitSMS) { 
					als = new ArrayList<String>();
					als.add("read");
					als.add("stopped");
					return false;
				}
			} 
			als = new ArrayList<String>();
			als.add("read");
			als.add("timeout"); 
			return false;
		}

		private CursorLoader mCl = null;

		@SuppressWarnings("deprecation")
        private boolean querySms()
		{ 
			Cursor cursor1 = null;
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				//CursorLoader cl = new CursorLoader(mActivity.getApplicationContext());
				//mCl = null;
				mActivity.runOnUiThread(new Runnable() { public void run() { 
					mCl = new CursorLoader(mActivity.getApplicationContext()); 
				}});
				if(mCl == null) { 
					return false;
				} 
				mCl.setUri(Uri.parse("content://sms/inbox")); 
				cursor1 = mCl.loadInBackground(); 
			} else {
				Uri mSmsinboxQueryUri = Uri.parse("content://sms/inbox"); 
				cursor1 = mActivity.getContentResolver().query(mSmsinboxQueryUri,
					new String[] { "_id", "thread_id", "address", "person", "date", "body", "type" }, null, null, null); 
				mActivity.startManagingCursor(cursor1); 
			} 
			String[] columns = new String[] { "address", "person", "date", "body", "type" };
			if(cursor1.getCount() > 0) { 
				while (cursor1.moveToNext()) {
					String date = cursor1.getString(cursor1.getColumnIndex(columns[2]));
					long ndate = Long.parseLong(date);
					// check messages
					if(ndate >= mCurMillis) {
						if(als == null) {
							als = new ArrayList<String>();
							als.add("read");
						}
						String address = cursor1.getString(cursor1.getColumnIndex(columns[0]));
						String name = cursor1.getString(cursor1.getColumnIndex(columns[1]));
						String msg = cursor1.getString(cursor1.getColumnIndex(columns[3]));
						JSONArray jarr = new JSONArray();
						jarr.put(address); jarr.put(name); jarr.put(msg);
						als.add(jarr.toString());
						//jprintf("## jarr: " + jarr.toString());
						mCurMillis = System.currentTimeMillis();
					}
				}
			} 
			return (als == null ? false : true);
		}
	}
}

