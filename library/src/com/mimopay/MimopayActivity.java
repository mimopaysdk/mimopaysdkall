package com.mimopay;

import android.app.Activity;
import android.os.Bundle;
import android.os.Build;
import android.os.Looper;
import android.os.Handler;
import android.widget.TextView;
import android.util.Log;
import android.util.StateSet;
import android.util.AttributeSet;
import android.content.Context;
import android.content.Intent;
import android.content.ContentResolver;
import android.text.Spanned;
import android.widget.ProgressBar;
import android.widget.Button;
import android.widget.Toast;
import android.view.View.OnClickListener;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.view.View;
import android.os.AsyncTask;
import android.webkit.WebView;
import android.text.TextWatcher;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.InputFilter;
import android.view.inputmethod.InputMethodManager;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.ClipData;
import android.widget.ScrollView;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.LinearLayout;
import android.widget.CompoundButton;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.view.Gravity;
import android.os.Environment;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.ImageButton;
import android.graphics.Matrix;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.Config;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.WindowManager;
import android.view.Display;
import android.content.res.Configuration;
import android.content.pm.ActivityInfo;
import android.telephony.TelephonyManager;
import android.telephony.SmsManager;
import android.webkit.WebViewClient;
import android.graphics.Picture;
import android.net.Uri;
import android.database.Cursor;
import android.app.Dialog;
import android.content.DialogInterface.OnCancelListener;
import android.view.Window;
import android.graphics.ColorFilter;
import android.util.TypedValue;
import android.util.DisplayMetrics;

// JSON
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// java
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CharsetDecoder;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.security.MessageDigest;
import java.math.BigInteger;
import java.io.File;
import java.net.URL;
import java.net.URLConnection;
import java.io.FileOutputStream;
import java.net.SocketTimeoutException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.concurrent.atomic.AtomicBoolean;

public class MimopayActivity extends Activity
{
	private void jprintf(String s)
	{
		if(!MimopayStuff.mEnableLog) return;
		Log.d("JimBas", "MimopayActivity: " + s);
	}

    private EditText edtUserToken = null;
	private int measuredWidth = 0;
	private int measuredHeight = 0;
	private boolean	bMaxTopUpNumbers = false;
	private final int TEXT_COLOR = 0xFF0D47A1;
	private final int BKGND_COLOR = Color.TRANSPARENT;//0xF3DFDFDF;
	private final int BTNCOLOR = 0xFF0D47A1;
	private final int BTNCOLOR2 = 0xFF1976D2;
	private final int BTNCOLORSTROKE = 0xFF1565C0;
	private final int WNDSTKCOLOR = Color.DKGRAY;

	private MimopayActivityCore mCore = null;

	private String mInfoResult = "ERROR";
	private ArrayList<String> mAlsResult = new ArrayList<String>();

	private String mPhoneNumber = null;
	private String mAirtimeValue = null;
	private boolean mbCannotSMS = false;
	private String msSmsContent = "";
	private String msSmsNumber = "";
	private int mnTextAppearSmaller = 0;
	private float mScale = 0f;
	private int nBtnActionCnt = 0;

	public class CTextView extends TextView
	{
	    public CTextView(Context context) { super(context); }
	    @Override protected void onDraw(Canvas canvas) {
	    	super.onDraw(canvas);
	    	if(mnTextAppearSmaller > 0) return;
    		float fth = getTextSize() * 0.85f;
    		mnTextAppearSmaller = (int) fth;
	        //jprintf("CTextView.onDraw: " + Float.toString(fth));
	    }
	}

	private void _setTheme()
	{
		int nstyle = android.R.style.Theme_Dialog;
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB && Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			jprintf("_setTheme=HOLO");
			nstyle = android.R.style.Theme_Holo_Light_Dialog_MinWidth;
		} else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
			jprintf("_setTheme=DEVICEDEFAULT");
			//nstyle = android.R.style.Theme_DeviceDefault_Light_Dialog_MinWidth;
			nstyle = android.R.style.Theme_Holo_Light_Dialog_MinWidth;
		} else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			jprintf("_setTheme=MATERIAL");
			nstyle = android.R.style.Theme_Material_Light_Dialog_MinWidth;
		}
		setTheme(nstyle);
	}

	private int getTextAppearance()
	{
		int nstyle = android.R.style.TextAppearance_Small;
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB && Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			nstyle = android.R.style.TextAppearance_Holo_Small;
		} else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
			//nstyle = android.R.style.TextAppearance_DeviceDefault_Small;
			nstyle = android.R.style.TextAppearance_Holo_Small;
		} else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			nstyle = android.R.style.TextAppearance_Material_Small;
		}
		return nstyle;
	} 

	private int getTextAppearanceBig()
	{
		int nstyle = android.R.style.TextAppearance_Medium;
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB && Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			nstyle = android.R.style.TextAppearance_Holo_Medium;
		} else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
			//nstyle = android.R.style.TextAppearance_DeviceDefault_Medium;
			nstyle = android.R.style.TextAppearance_Holo_Medium;
		} else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			nstyle = android.R.style.TextAppearance_Material_Medium;
		}
		return nstyle;
	} 

	@Override public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler());

		getWindow().setBackgroundDrawable(new ColorDrawable(0x8f000000));

		// fix orientation on start
		int reqorient = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ?
				ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
		setRequestedOrientation(reqorient);

		_setTheme();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setFinishOnTouchOutside(false);

		mCore = new MimopayActivityCore();

		mPhoneNumber = getIntent().getStringExtra("PhoneNumber");
		mAirtimeValue = getIntent().getStringExtra("AirtimeValue");

		mCore.mActivity = MimopayActivity.this;
		mAlsResult.clear();
		mAlsResult.add("UserCanceled");

		getScreenDimension();
		loadLastUsedPhoneNumber();

		mScale = MimopayActivity.this.getResources().getDisplayMetrics().density;
		mCore.executePayment();
	}

	private class TopExceptionHandler implements Thread.UncaughtExceptionHandler
	{
		private Thread.UncaughtExceptionHandler defaultUEH;
		TopExceptionHandler() {
			this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
		}
		public void uncaughtException(Thread t, Throwable e) {
			// dp("forceCloseReason: " + e.toString());
            final StackTraceElement[] stackTrace = e.getStackTrace();
            for(StackTraceElement element : stackTrace) {
                String em = "Exception thrown from " + element.getMethodName()
                    + " in class " + element.getClassName() + " [on line number "
                    + element.getLineNumber() + " of file " + element.getFileName() + "]";
                jprintf(em);
            }
			mInfoResult = "FATALERROR";
			mAlsResult.clear();
			mAlsResult.add(e.toString());
			if(Mimopay.mMi != null) {
				Mimopay.mMi.onReturn(mInfoResult, mAlsResult);
			}
			new Thread() { @Override public void run() {
				Looper.prepare();  
				if(!isActivityFinishing()) {
					Toast.makeText(getApplicationContext(), "Mimopay has Crashed. Recovering now...", Toast.LENGTH_LONG).show();
				}
				//System.exit(1);
				Looper.loop();
			}}.start();
			try { Thread.sleep(3000); } catch(InterruptedException ei) {}
			System.exit(666);
		}
	}

	@SuppressWarnings("deprecation")
	private void getScreenDimension()
	{
		Point size = new Point();
		WindowManager w = getWindowManager();

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			w.getDefaultDisplay().getSize(size);
			measuredWidth = size.x;
			measuredHeight = size.y;
		} else {
			Display d = w.getDefaultDisplay(); 
			measuredWidth = d.getWidth(); 
			measuredHeight = d.getHeight(); 
		}
	}

	@SuppressWarnings("deprecation")
	private void setBkgndCompat(View v, Drawable background)
	{
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			v.setBackground(background);
		} else {
			v.setBackgroundDrawable(background);
		}
	}

	String msLastUsedPhoneNumber = null;

	@SuppressWarnings("unchecked")
	private boolean isJsonKeyAvail(JSONObject jsobj, String reqkey)
	{
		Iterator<String> keys = jsobj.keys();
		while(keys.hasNext()) {
			String skey = keys.next();
			if(skey.equals(reqkey)) return true;
		}
		return false;
	}

	private final int STLN_BTNDONE = 0;
	private final int STLN_BTNNEXT = 1;
	private final int STLN_VOUCHERCODE = 2;
	private final int STLN_SHOWHELP = 3;
	private final int STLN_UNABLEHELP = 4;
	private final int STLN_TEXTPHNUM = 5;
	private final int STLN_PHONENUM = 6;
	private final int STLN_NOSUPPGSM = 7;
	private final int STLN_CHECKBOXUPOINT = 8;
	private final int STLN_CHECKBOXXL = 9;
	private final int STLN_CHECKBOXMPOINT = 10;
	private final int STLN_DUALSIM = 11;
	private final int STLN_AUTOSEND = 12;
	private final int STLN_BTNSENDSMS = 13;
	private final int STLN_BTNCANCEL = 14;
	private final int STLN_TRANSACTION = 15;
	private final int STLN_PURCHASEBY = 16;
	private final int STLN_SMSSTOCKAPP = 17;
	private final int STLN_BTNNEXTORDONE = 18;
	private final int STLN_SMSRECVCONF = 19;
	private final int STLN_MERCURLNULL = 20;
	private final int STLN_PYMETNOTSUPP = 21;
	private final int STLN_UNAVAILPYCH = 22;
	private final int STLN_ERROR404 = 23;
	private final int STLN_UNDMAINTNC = 24;
	private final int STLN_CHNOTREADY = 25;
	private final int STLN_INVALPHNUM = 26;
	private final int STLN_ERRORCONN = 27;
	private final int STLN_ERRORVALID = 28;
	private final int STLN_16DGTCOPIED = 29;
	private final int STLN_VALIDATING = 30;
	private final int STLN_CONNECTING = 31;
	private final int STLN_SENDINGSMS = 32;
	private final int STLN_WIILRECVSMS = 33;
	private final int STLN_SMSSENT = 34;
	private final int STLN_FAILSMS = 35;
	private final int STLN_SMSMAYFAIL = 36;
	private final int STLN_WAITREPSMS = 37;
	private final int STLN_PLEASEWAIT = 38;
	private final int STLN_INORDERSMS = 39;
	private final int STLN_TEXTUP = 40;
	private final int STLN_SENDTO = 41;
	private final int STLN_SHORTCODE = 42;
	private final int STLN_CNFRMSCODE = 43;
	private final int STLN_CHECKBOXDPOINT = 44;
	private final int STLN_BTNNEXTORDONEDPOINT = 45;
	private final int STLN_BTNGOTOPUP = 46;
	private final int STLN_AMOUNTVALUE = 47;
	private final int STLN_PRODUCTNAME = 48;
	private final int STLN_CHECKBOXCELCOM = 49;
	private final int STLN_CHECKBOXINDOSAT = 50;

	private final String[] mStringLangEn = {
		"DONE", "NEXT", "Voucher code", "Press '?' to show help", "Unable to show help at the moment",
		"We have read an invalid phone number of your device. You must texting it manually.", "Phone Number:",
		"We suspect your device is a WiFi-only device. You have to SMS the UPoint received code manually using other device.",
		"By checking it, an SMS will automatically send to UPoint at the end of this transaction. " +
			"Please make sure that the number is an active number of this device and also it is Telkomsel/Flexi number.",
		"By checking it, incomming SMS from XL will automatically checked and continue with the code verification. " +
			"Please make sure that the number is an active number of this device and also it is XL number.",
		"By checking it, incomming SMS from Maxis will automatically checked and continue with the code verification. " +
			"Please make sure that the number is an active number of this device and also it is Maxis number.",
		"Dual SIM Card", "Press 'Send SMS' for automatic sending SMS. Your phone credits will be deducted.",
		"Send SMS", "CANCEL", "Transaction", "Purchase by using phone number:",
		"Press 'NEXT' to sending SMS by using your device's SMS app. Your phone credits will be deducted.",
		"Sending confirmation code SMS. Press NEXT for next step or DONE if you want to confirm manually",
		"You will receive confirmation SMS", "Merchant URL should not be null",
		"Payment method not supported", "Unavailable payment channel or not supported",
		"Error 404 occurs. Please try again", "Sorry, server is under maintenance at the moment", "Sorry, channel is not ready yet",
		"Invalid phone number", "Error connecting to server or busy", "Error validating voucher code",
		"Internet connection problem. Your 16-digit number has been copied into clipboard. You may try again, long press and paste it",
		"Please wait while validating code to server...", "Please wait while connecting to server...",
		"Please wait while sending SMS...", "SMS has been sent. You will receive a replied SMS soon",
		"SMS has been sent. You may have received a replied SMS already. Please wait if you haven't received it yet",
		"Failed to send SMS", "SMS sent may failed, please try again",
		"Please wait while checking/wait for replied SMS. Press 'back' to stop.",
		"Please wait a moment...", "In order to automatically send SMS, you have to text that 'up' code below:",
		"Text UP(space)", "and send to", "Short code:", "In order to confirm the payment, you must type that short code.",
		 "By checking it, incomming SMS from Digi will automatically checked and continue with the code verification. " +
			"Please make sure that the number is an active number of this device and also it is Digi number.",
		"SMS CODE confirmation. Press NEXT for next step or DONE if you want to confirm manually", "Go Topup",
		"Amount Value: ", "Product Name: ",
		 "By checking it, incomming SMS from Celcom will automatically checked and continue with the code verification. " +
			"Please make sure that the number is an active number of this device and also it is Celcom number.",
	};

	private final String[] mStringLangId = {
		"SELESAI", "LANJUT", "Kode voucher", "Tekan '?' untuk menampilkan bantuan", "Maaf, saat ini sedang tidak bisa menampilkan bantuan",
		"Nomor telpon anda tidak berhasil dibaca. Anda harus mengetikkan secara manual.", "Nomor Telpon:",
		"Perangkat anda hanya mendukung Wifi-only. Anda bisa mengirimkan SMS dengan menggunakan perangkat lain",
		"Centang ini jika anda ingin transaksi ini lanjut secara otomatis mengirimkan SMS. " + 
			"Pastikan nomor yg anda ketik adalah nomor yg aktif diperangkat ini dan nomor tersebut adalah nomor Telkomsel/Flexi.",
		"Centang ini jika anda ingin SMS yg akan diterima dari XL akan secara otomatis dibaca dan lanjut untuk verifikasi kode. " + 
			"Pastikan nomor yg anda ketik adalah nomor yg aktif diperangkat ini dan nomor tersebut adalah nomor XL.",
		"",	// khusus maxis
		"Dual Kartu SIM", "Tekan 'Kirim SMS' utk mengirim SMS secara otomatis. Pulsa anda otomatis akan berkurang.",
		"Kirim SMS", "BATAL", "Transaksi", "Pembelian menggunakan nomor telpon:",
		"Tekan 'LANJUT' mengirim SMS dengan menggunakan aplikasi SMS yg telah ada di handset anda. Pulsa anda otomatis akan berkurang.",
		"Kode konfirmasi SMS sedang dikirim. Tekan LANJUT untuk langkah berikutnya atau SELESAI jika ingin konfimasi sendiri",
		"Anda akan segera menerima konfirmasi SMS", "URL merchant Tidak Boleh Sama Dengan Nol",
		"Cara pembayaran tidak didukung", "Kanal pembayaran tidak didukung atau tersedia",
		"Terjadi error 404. Mohon ulangi kembali", "Maaf, server sedang dalam perbaikan saat ini", "Maaf, channel tidak siap saat ini",
		"Nomor telepon tidak valid", "Error koneksi ke server atau server sibuk", "Error validasi kode voucher",
		"Koneksi internet bermasalah. Kode 16-digit anda sudah di-copy, anda bisa mengulangi kembali dan paste kode tersebut.",
		"Mohon menunggu, sedang memvalidasi kode dgn server...", "Mohon menunggu, melakukan koneksi ke server...",
		"Mohon menunggu, mengirimkan SMS...", "SMS telah terkirim. Anda segera akan mendapatkan SMS balasan",
		"SMS telah terkirim. Anda mungkin sudah menerima SMS balasan. Jika belum mohon tunggu, anda akan segera menerimanya",
		"Gagal mengirim SMS", "SMS mungkin gagal terkirim mohon coba lagi",
		"Mohon menunggu, sedang memeriksa/menunggu SMS balasan. Tekan 'back' jika ingin berhenti",
		"Mohon tunggu sebentar...", "Agar supaya SMS terkirim secara otomatis, anda harus mengetikkan kode 'up' tersebut diatas.",
		"Kirim SMS berisi UP(spasi)", "kirim ke", "Kode balasan:", "Untuk konfirmasi transaksi, anda harus mengetikkan kode balasan tersebut.",
		"", "", "", // khusus dpoint
		"Nilai Pecahan: ", "Nama Produk: ",
		"", // khusus celcom
		"Centang ini jika anda ingin SMS yg akan diterima dari Indosat akan secara otomatis dibaca dan lanjut untuk verifikasi kode. " + 
			"Pastikan nomor yg anda ketik adalah nomor yg aktif diperangkat ini dan nomor tersebut adalah nomor Indosat.",
	};

	private String getStringLang(int nid)
	{
		if(MimopayStuff.mMimopayLang.equals("EN")) {
			if(MimopayStuff.sCustomLang != null) {
				int len = MimopayStuff.sCustomLang.length;
				if(nid < len)
					return MimopayStuff.sCustomLang[nid];
				return "Unavailable string lang index " + Integer.toString(nid);
			}
			return mStringLangEn[nid];
		}
		// ID
		return mStringLangId[nid];
	}

	private void loadLastUsedPhoneNumber()
	{
		try {
			FileInputStream fis = getApplicationContext().openFileInput("LastUsedPhoneNumber");
			ObjectInputStream ois = new ObjectInputStream(fis);
			msLastUsedPhoneNumber = (String) ois.readObject();
			fis.close();
			jprintf("msLastUsedPhoneNumber: " + msLastUsedPhoneNumber);
		} catch(Exception e) {
			jprintf("loadLastUsedPhoneNumber'e: " + e.toString());
            JSONObject jsonobj = new JSONObject();
			msLastUsedPhoneNumber = jsonobj.toString();
		}
	}

	private String getLastUsedPhoneNumber(String spaytype)
	{
		String srval = null;
        try {
            JSONObject jsonobj = new JSONObject(msLastUsedPhoneNumber);
			if(isJsonKeyAvail(jsonobj, spaytype)) {
				srval = jsonobj.getString(spaytype);
			}
        } catch (JSONException e) {
			jprintf("getLastUsedPhoneNumber'e: " + e.toString());
		}
		return srval;
	}

	private void saveLastUsedPhoneNumber(String spaytype, String sphonenum)
	{
        try {
            JSONObject jsonobj = new JSONObject(msLastUsedPhoneNumber);
            jsonobj.put(spaytype, sphonenum);
			msLastUsedPhoneNumber = jsonobj.toString();
			jprintf("saveLastUsedPhoneNumber'msLastUsedPhoneNumber: " + msLastUsedPhoneNumber);
			FileOutputStream out = getApplicationContext().openFileOutput("LastUsedPhoneNumber", Context.MODE_PRIVATE);
			ObjectOutputStream oos = new ObjectOutputStream(out);
			oos.writeObject(msLastUsedPhoneNumber);
			out.close();
        } catch (JSONException e) {
			jprintf("saveLastUsedPhoneNumber'e: " + e.toString());
		} catch(Exception e) {
			jprintf("saveLastUsedPhoneNumber'e: " + e.toString());
		}
	}

	private void setupVnTelcoUI()
	{
        RelativeLayout mainlayout = new RelativeLayout(this);
		//mainlayout.setPadding(25,20,25,20);
		GradientDrawable rlshape =  new GradientDrawable();
		rlshape.setCornerRadius(4);
		rlshape.setColor(Color.WHITE);
		rlshape.setStroke(2, WNDSTKCOLOR);
		setBkgndCompat(mainlayout, rlshape);

			// form at center
		    RelativeLayout rl = new RelativeLayout(this);
			RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,  LayoutParams.WRAP_CONTENT);
			rllp.addRule(RelativeLayout.CENTER_IN_PARENT);
			rl.setLayoutParams(rllp);

			int id = 0;

            String[] slabels = {"Serial Number", "Pin Code", "Phone Number", "Email Address"};
            final ArrayList<Boolean> bedtx = new ArrayList<Boolean>();
            final ArrayList<EditText> edtx = new ArrayList<EditText>();
            final Button btn = new Button(this);

		    RadioGroup rg = null;
			RadioButton[] rb = null;

			// payment method
			if(mCore.alsTextEditName != null) {
				int i,j,k;
				j = mCore.alsTextEditName.size();
			    //add radio buttons
			    rg = new RadioGroup(this);
				rg.setId(++id);
				rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(RadioGroup rg, int cid) {
						int i,j = bedtx.size();
						for(i=0;i<j;i++) {
							if(bedtx.get(i) == false) {
								btn.setEnabled(false);
								return;
							}
						}
						btn.setEnabled(true);
					}
				});
				RelativeLayout.LayoutParams rglp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				rglp.addRule(RelativeLayout.BELOW, id-1);
			    rg.setOrientation(RadioGroup.VERTICAL);
				_setMargins(rglp,20,15,20,10);
				rg.setLayoutParams(rglp);
				rb = new RadioButton[j];
				for(i=0,k=0; i<j; i++,k++) {
					rb[k] = new RadioButton(this);
					rb[k].setText(getPaymentMethodTitle(mCore.alsTextEditName.get(i)));
					rb[k].setId(100+i);
					RadioGroup.LayoutParams rblp = new RadioGroup.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					rb[k].setLayoutParams(rblp);
					rb[k].setTextAppearance(MimopayActivity.this, getTextAppearance());
					rb[k].setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
				    rb[k].setTextColor(TEXT_COLOR);
					rg.addView(rb[k]);
				}
				rl.addView(rg);
			}

			final RadioButton[] frb = rb;

		    for(int i=0;i<4;i++) {
				// add boolean
				final AtomicBoolean bet = new AtomicBoolean(false);
				// add edit text
				final EditText et = new EditText(this);
				et.setId(++id);
				RelativeLayout.LayoutParams etlp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				etlp.addRule(RelativeLayout.BELOW, id-1);
				//etlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
				_setMargins(etlp,20,5,20,5);
				et.setLayoutParams(etlp);
				if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
					GradientDrawable etshape =  new GradientDrawable();
					etshape.setCornerRadius(9);
					etshape.setColor(Color.TRANSPARENT);
					etshape.setStroke(2, BTNCOLOR);
					setBkgndCompat(et, etshape);
				}
				et.setHint(slabels[i]);
				et.setMinLines(1);
				et.setMaxLines(3);
				et.getBackground().setColorFilter(BTNCOLOR, PorterDuff.Mode.MULTIPLY);  
				if(i<3) {
					et.setFilters(new InputFilter[] { voucherfilter });
					et.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_PHONE);
				} else {
					et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS );
				}
				et.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				et.setTextAppearance(MimopayActivity.this, getTextAppearance());
				et.setTextColor(BTNCOLOR);
		        et.addTextChangedListener(new EditTextWatcher(et,i) { @Override public void afterTextChanged(Editable s) {
					bedtx.set(mnInt, s.length() <= 0 ? false : true);
					boolean btelco = false;
					int i,j=mCore.alsTextEditName.size();
					for(i=0;i<j;i++) {
						if(frb[i].isChecked()) {
							btelco = true;
							break;
						}
					}
					j = bedtx.size();
					for(i=0;i<j;i++) {
						if(bedtx.get(i) == false) {
							btn.setEnabled(false);
							return;
						}
					}
					if(btelco) {
						btn.setEnabled(true);
					} else {
						btn.setEnabled(false);
					}
				}});
		        edtx.add(et);
				bedtx.add(false);
				rl.addView(et);
				//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));
			}
	 
			LinearLayout ell = new LinearLayout(this);
			ell.setId(++id);
			RelativeLayout.LayoutParams elllp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			elllp.addRule(RelativeLayout.BELOW, id-1);
			elllp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			_setMargins(elllp,0,25,20,10);
			ell.setLayoutParams(elllp);

				// dummy space
				// RelativeLayout rldummy = new RelativeLayout(this);
				// LinearLayout.LayoutParams rldummylp = new LinearLayout.LayoutParams(0,  LayoutParams.WRAP_CONTENT);
				// rldummylp.weight = 1.0f;
				// rldummy.setLayoutParams(rldummylp);
				// ell.addView(rldummy);

				// add top up button
				setButtonShape(btn, 0, BTNCOLOR);
				_setPadding(btn,30, mnButtonOffset, 30, 0);
				LinearLayout.LayoutParams blp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				// blp.weight = 1.0f;
				// blp.gravity = Gravity.CENTER;
				btn.setLayoutParams(blp);
				btn.setText(getStringLang(STLN_BTNNEXT));
				btn.setGravity(Gravity.CENTER);
				btn.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				btn.setTextAppearance(MimopayActivity.this, getTextAppearance());
				btn.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
				btn.setEnabled(false);
				////////////////////////////////////////////////////////
				// btn.setEnabled(true);
				////////////////////////////////////////////////////////
				bMaxTopUpNumbers = false;
			    btn.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					// hide keyboard
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
					String s;
					JSONArray jarr = new JSONArray();
					int j=mCore.alsTextEditName.size();
					for(int i=0;i<j;i++) {
						if(frb[i].isChecked()) {
							s = mCore.alsTextEditName.get(i);
							jprintf("telco: " + s);
							jarr.put(s);
							mCore.mAlsActiveIdx = i;
							break;
						}
					}
					for(int i=0;i<edtx.size();i++) {
						s = edtx.get(i).getText().toString();
						jarr.put(s);
						jprintf("edittextvn: " + s);
					}
					// jarr.put("vte");
					// jarr.put("92314601982");	// serial
					// jarr.put("1924002840726");	// hrn
					// jarr.put("0945190990");	// mdn
					// jarr.put("jim@bas.com");
					mCore.mHttppostCode = jarr.toString();
					mCore.executeBtnAction();
				}});
			    ell.addView(btn);

		    rl.addView(ell);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));

		ScrollView sv = new ScrollView(this);
		RelativeLayout.LayoutParams svlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  LayoutParams.WRAP_CONTENT);
		_setMargins(svlp,5,5,5,5);
		svlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		svlp.addRule(RelativeLayout.CENTER_VERTICAL);
		sv.setLayoutParams(svlp);
		sv.setVerticalScrollBarEnabled(false);
		sv.setHorizontalScrollBarEnabled(false);
		sv.addView(rl);
		mainlayout.addView(sv);
        setContentView(mainlayout, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}

	private void setupDenomUI()
	{
        RelativeLayout mainlayout = new RelativeLayout(this);
		//mainlayout.setPadding(25,20,25,20);
		GradientDrawable rlshape =  new GradientDrawable();
		rlshape.setCornerRadius(4);
		rlshape.setColor(Color.WHITE);
		rlshape.setStroke(2, WNDSTKCOLOR);
		setBkgndCompat(mainlayout, rlshape);

			// form at center
		    RelativeLayout rl = new RelativeLayout(this);
			RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,  LayoutParams.WRAP_CONTENT);
			rllp.addRule(RelativeLayout.CENTER_IN_PARENT);
			rl.setLayoutParams(rllp);

			int id = 0;

		    // title
		    TextView tvtitle = new TextView(this);
			tvtitle.setId(++id);
			RelativeLayout.LayoutParams tvtitlelp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			_setMargins(tvtitlelp,20,10,0,10);
			tvtitle.setLayoutParams(tvtitlelp);
			tvtitle.setText(getPaymentMethodTitle(MimopayStuff.sChannel));
			tvtitle.setTextAppearance(MimopayActivity.this, getTextAppearanceBig());
			tvtitle.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
		    tvtitle.setTextColor(TEXT_COLOR);
		    rl.addView(tvtitle);

			// // the logo
			// ImageView ivlogo = new ImageView(this);
			// ivlogo.setId(++id);
			// RelativeLayout.LayoutParams ivlogolp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			// // ivlogolp.addRule(RelativeLayout.ALIGN_TOP);
			// // ivlogolp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			// ivlogolp.setMargins(15,15,0,15);
			// ivlogo.setLayoutParams(ivlogolp);
			// Bitmap mlbmp = mCore.getBitmapInternal(mCore.mMimopayLogoUrl);
			// if(mlbmp != null) {
			// 	ivlogo.setImageBitmap(mlbmp);
			// }
			// rl.addView(ivlogo);
			// //rl.addView(inBetweenHorizontalSpace(++spaceid, 15, id));

		    // pre-init button
		    final Button btn = new Button(this);
			mCore.mHttppostCode = null;

		    //add radio buttons
		    RadioGroup rg = new RadioGroup(this);
			rg.setId(++id);
			rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				public void onCheckedChanged(RadioGroup rg, int cid) {
					String s = mCore.alsDenomKey.get(mCore.mAlsActiveIdx);
					int did = cid - 100;
					try {
						JSONArray jarr = new JSONArray(s);
						mCore.mHttppostCode = jarr.getString(did);
					} catch(JSONException e) {}
					jprintf(
						"cid: " + Integer.toString(cid) +
						" mCore.mHttppostCode: " + mCore.mHttppostCode
					);
					btn.setEnabled(true);
					// setButtonEnabled(btn, true);
					// if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					// 	btn.setAlpha(1.0f);
					// }
				}
			});
			RelativeLayout.LayoutParams rglp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			rglp.addRule(RelativeLayout.BELOW, id-1);
		    rg.setOrientation(RadioGroup.VERTICAL);
			_setMargins(rglp,15,0,15,0);
			rg.setLayoutParams(rglp);
			RadioButton[] rb = null;

			try {
				String s = mCore.alsDenomAmount.get(mCore.mAlsActiveIdx);
				if(s.isEmpty()) s = mCore.alsDenomValue.get(mCore.mAlsActiveIdx);
				//String s = mCore.alsDenomValue.get(mCore.mAlsActiveIdx);
				JSONArray jarr = new JSONArray(s);
				int i,k,j=jarr.length();
				rb = new RadioButton[j];
				for(i=0,k=0; i<j; i++) {
					rb[k] = new RadioButton(this);
					String ss = jarr.getString(i);
					jprintf("jarr: " + ss + " i: " + Integer.toString(i));
					rb[k].setText(ss);
					rb[k].setId(100+i);
					RadioGroup.LayoutParams rblp = new RadioGroup.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					rb[k].setLayoutParams(rblp);
					rb[k].setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
					rb[k].setTextAppearance(MimopayActivity.this, getTextAppearance());
				    rb[k].setTextColor(TEXT_COLOR);
					rg.addView(rb[k]);
					k++;
				}
			} catch(JSONException e) {
				jprintf("setupDenomUI,JSONException: " + e.toString());
				return;
			}
			rl.addView(rg);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));

			LinearLayout ell = new LinearLayout(this);
			ell.setId(++id);
			RelativeLayout.LayoutParams elllp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			elllp.addRule(RelativeLayout.BELOW, id-1);
			elllp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			_setMargins(elllp,0,30,20,10);
			ell.setLayoutParams(elllp);

				// dummy space
				// RelativeLayout rldummy = new RelativeLayout(this);
				// LinearLayout.LayoutParams rldummylp = new LinearLayout.LayoutParams(0,  LayoutParams.WRAP_CONTENT);
				// rldummylp.weight = 1.0f;
				// rldummy.setLayoutParams(rldummylp);
				// ell.addView(rldummy);

				// add top up button
				setButtonShape(btn, 0, BTNCOLOR);
				_setPadding(btn,30, mnButtonOffset, 30, 0);
				LinearLayout.LayoutParams blp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				// blp.weight = 1.0f;
				// blp.gravity = Gravity.CENTER;
				btn.setLayoutParams(blp);
				btn.setText(getStringLang(STLN_BTNNEXT));
				btn.setGravity(Gravity.CENTER);
				btn.setEnabled(false);
				//setButtonEnabled(btn, false);
				btn.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				btn.setTextAppearance(MimopayActivity.this, getTextAppearance());
				btn.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
			    btn.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					if(mCore.mHttppostCode != null && !mCore.mHttppostCode.equals("")) {
						if(MimopayStuff.sPaymentMethod.equals("ATM")) {
							MimopayStuff.sAmount = mCore.mHttppostCode;
							mCore.reset();
							mCore.retrieveMerchantPaymentMethod();
						} else {
							mCore.executeBtnAction();
						}
					}
				}});
			    ell.addView(btn);

		    rl.addView(ell);

		ScrollView sv = new ScrollView(this);
		RelativeLayout.LayoutParams svlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  LayoutParams.WRAP_CONTENT);
		svlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		svlp.addRule(RelativeLayout.CENTER_VERTICAL);
		sv.setLayoutParams(svlp);
		sv.setVerticalScrollBarEnabled(false);
		sv.setHorizontalScrollBarEnabled(false);
        sv.addView(rl);
        mainlayout.addView(sv);
        setContentView(mainlayout, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}

	InputFilter voucherfilter = new InputFilter()
	{
		@Override public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend)
		{
			if(bMaxTopUpNumbers)
				return "";
			for(int i = start; i < end; i++) {
				if(!Character.isDigit(source.charAt(i))) {
					return "";
				}
			}
			return null;
		}
	};

	private Handler mHandlerHelp = null;

	private String getPaymentMethodTitle(String sref)
	{
		if(sref.equals("smartfren")) { return "Smartfren"; }
		if(sref.equals("sevelin")) { return "Sevelin"; }
		if(sref.equals("upoint")) { return "Telkomsel Pulsa - Upoint"; }
		if(sref.equals("upoint_hrn")) { return "Telkomsel Voucher - Upoint"; }
		if(sref.equals("indosat_airtime")) { return "Indosat Pulsa"; }
		if(sref.equals("atm_bca")) { return "ATM BCA"; }
		if(sref.equals("atm_bersama")) { return "ATM Bersama"; }
		if(sref.equals("xl_airtime")) { return "XL Pulsa"; }
		if(sref.equals("xl_hrn")) { return "XL Voucher"; }
		if(sref.equals("mpoint")) { return "Maxis MPoint"; }
		if(sref.equals("dpoint")) { return "Digi DPoint"; }
		if(sref.equals("celcom")) { return "Celcom Airtime"; }
		if(sref.equals("vnp")) { return "Vina Phone"; }
		if(sref.equals("vms")) { return "Mobi Fone"; }
		if(sref.equals("vte")) { return "Viettel"; }
		return "";
	}

	// private int pxdp(int px)
	// {
	// 	//return (int)(dpx*mScale+0.5f);
	// 	//return (int)(dpx*mScale);
	// 	//Resources r = mContext.getResources();
	// 	int npx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, getResources().getDisplayMetrics());
	// 	return npx;
	// } 

	@SuppressWarnings("deprecation")
	public int dpToPx(int dp)
	{
		DisplayMetrics displayMetrics = MimopayActivity.this.getResources().getDisplayMetrics();
		int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));       
		return px;
	}

	@SuppressWarnings("deprecation")
	public int pxToDp(int px)
	{
		DisplayMetrics displayMetrics = MimopayActivity.this.getResources().getDisplayMetrics();
		int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
		return dp;
	}

	private void _setMargins(MarginLayoutParams lp, int l, int t, int r, int b)
	{
		lp.setMargins(dpToPx(l),dpToPx(t),dpToPx(r),dpToPx(b));
	} 

	private void _setPadding(View v, int l, int t, int r, int b)
	{
		v.setPadding(dpToPx(l),dpToPx(t),dpToPx(r),dpToPx(b));
	} 

	private void setupTopUpVoucherUI()
	{
        RelativeLayout mainlayout = new RelativeLayout(this);
		GradientDrawable rlshape =  new GradientDrawable();
		rlshape.setCornerRadius(4);
		rlshape.setColor(Color.WHITE);
		rlshape.setStroke(2, WNDSTKCOLOR);
		setBkgndCompat(mainlayout, rlshape);

			// form at center
		    RelativeLayout rl = new RelativeLayout(this);
			RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,  LayoutParams.WRAP_CONTENT);
			rllp.addRule(RelativeLayout.CENTER_IN_PARENT);
			rl.setLayoutParams(rllp);

			int id = 0;
			//int spaceid = 1000;
			Bitmap mlbmp = null;
		    final Button btnnext = new Button(this);

		    // title
		    TextView tvtitle = new TextView(this);
			tvtitle.setId(++id);
			RelativeLayout.LayoutParams tvtitlelp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			_setMargins(tvtitlelp, 20,10,0,10);
			tvtitle.setLayoutParams(tvtitlelp);
			tvtitle.setText(getPaymentMethodTitle(MimopayStuff.sChannel));
			tvtitle.setTextAppearance(MimopayActivity.this, getTextAppearanceBig());
			tvtitle.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
		    tvtitle.setTextColor(TEXT_COLOR);
		    rl.addView(tvtitle);
	 
			// ImageView ivlogo = new ImageView(this);
			// ivlogo.setId(++id);
			// RelativeLayout.LayoutParams ivlogolp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			// ivlogolp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			// ivlogolp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			// ivlogolp.setMargins(10,10,0,0);
			// ivlogo.setLayoutParams(ivlogolp);
			// ivlogo.setScaleType(ImageView.ScaleType.CENTER);
			// // mlbmp = getRoundedCornerBitmap(mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx)), Color.WHITE);
			// mlbmp = mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx));
			// if(mlbmp != null) {
			// 	ivlogo.setImageBitmap(mlbmp);
			// }
			// rl.addView(ivlogo);

		    /////////////////////
			// the logo
			// ImageView ivlogo = new ImageView(this);
			// ivlogo.setId(++id);
			// RelativeLayout.LayoutParams ivlogolp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			// // ivlogolp.addRule(RelativeLayout.ALIGN_TOP);
			// // ivlogolp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			// int left=0,top=0;
			// String sch = MimopayStuff.sChannel;
			// if(sch.equals("sevelin")) {
			// 	left = 10; top = 10;
			// } else if(sch.equals("upoint_hrn") || sch.equals("xl_hrn")) {
			// 	top = 10;
			// }
			// ivlogolp.setMargins(left,top,0,20);
			// ivlogo.setLayoutParams(ivlogolp);
			// ivlogo.setScaleType(ImageView.ScaleType.CENTER);
			// // mlbmp = getRoundedCornerBitmap(mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx)), Color.WHITE);
			// mlbmp = mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx));
			// if(mlbmp != null) {
			// 	ivlogo.setImageBitmap(mlbmp);
			// }
			// rl.addView(ivlogo);

			// add edit text
			final EditText et = new EditText(this);
			et.setId(++id);
			final AtomicBoolean bet = new AtomicBoolean(false);
			RelativeLayout.LayoutParams etlp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			etlp.addRule(RelativeLayout.BELOW, id-1);
			etlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			_setMargins(etlp,20,0,20,0);
			et.setLayoutParams(etlp);
			if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
				GradientDrawable etshape =  new GradientDrawable();
				etshape.setCornerRadius(9);
				etshape.setColor(Color.TRANSPARENT);
				etshape.setStroke(2, BTNCOLOR);
				setBkgndCompat(et, etshape);
			}
			et.setHint(getStringLang(STLN_VOUCHERCODE));
			et.setMinLines(1);
			et.setMaxLines(3);
			et.setFilters(new InputFilter[] { voucherfilter });
			// ini akan jadi masalah di theme deviceDefault
			et.getBackground().setColorFilter(BTNCOLOR, PorterDuff.Mode.MULTIPLY);  
			et.setFilters(new InputFilter[] { voucherfilter });
			et.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_PHONE);
			et.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
			et.setTextAppearance(MimopayActivity.this, getTextAppearance());
	        et.addTextChangedListener(new EditTextWatcher(et) { @Override public void afterTextChanged(Editable s) {
				bet.set(s.length() < 16 ? false : true);
				if(bet.get()) {
					bMaxTopUpNumbers = true;
					btnnext.setEnabled(true);
				} else {
					bMaxTopUpNumbers = false;
					btnnext.setEnabled(false);
				}
			}});
			bMaxTopUpNumbers = false;
			rl.addView(et);

		    RelativeLayout rlinstr = new RelativeLayout(this);
			rlinstr.setId(++id);
			RelativeLayout.LayoutParams rlinstrlp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			rlinstrlp.addRule(RelativeLayout.BELOW, id-1);
			rlinstrlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			_setMargins(rlinstrlp,20,10,20,0);
			rlinstr.setLayoutParams(rlinstrlp);
				final TextView tvints = new TextView(this);
				RelativeLayout.LayoutParams tvintslp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				tvints.setLayoutParams(tvintslp);
				tvints.setText("");
				tvints.setTypeface(Typeface.create("sans-serif", Typeface.ITALIC));
				if(mnTextAppearSmaller > 0) {
					tvints.setTextSize(TypedValue.COMPLEX_UNIT_PX, mnTextAppearSmaller);
				} else {
					tvints.setTextAppearance(MimopayActivity.this, getTextAppearance());
				}
			    tvints.setTextColor(BTNCOLOR2);
				rlinstr.addView(tvints);
				final ProgressBar pbints = new ProgressBar(this, null, android.R.attr.progressBarStyle);
				RelativeLayout.LayoutParams pbintslp = new RelativeLayout.LayoutParams(50, 50);
				pbintslp.addRule(RelativeLayout.CENTER_HORIZONTAL);
				pbints.setLayoutParams(pbintslp);
				pbints.setIndeterminate(true);
				pbints.getIndeterminateDrawable().setColorFilter(BTNCOLOR, Mode.MULTIPLY);
				pbints.setVisibility(View.GONE);
				rlinstr.addView(pbints);
			rl.addView(rlinstr);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 20, id));
	 	 
			LinearLayout ell = new LinearLayout(this);
			ell.setId(++id);
			RelativeLayout.LayoutParams elllp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			elllp.addRule(RelativeLayout.BELOW, id-1);
			elllp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			_setMargins(elllp,0,20,20,10);
			ell.setLayoutParams(elllp);

				// // dummy space
				// RelativeLayout rldummy = new RelativeLayout(this);
				// LinearLayout.LayoutParams rldummylp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				// //rldummylp.weight = 0.75f;
				// rldummy.setLayoutParams(rldummylp);
				// ell.addView(rldummy);

				// add top up button
			    final Button btnhlp = new Button(this);
				setButtonShape(btnhlp, 1, BTNCOLOR2);
				LinearLayout.LayoutParams bhlp = new LinearLayout.LayoutParams((int)(40*mScale+0.5f), LayoutParams.WRAP_CONTENT);
				//bhlp.weight = 0.35f;
				//bhlp.gravity = Gravity.CENTER;
				btnhlp.setLayoutParams(bhlp);
				btnhlp.setText("?");
				btnhlp.setGravity(Gravity.CENTER);
				btnhlp.setTextAppearance(MimopayActivity.this, getTextAppearance());
				btnhlp.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				btnhlp.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
			    btnhlp.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					if(mHandlerHelp != null) return;
					mHandlerHelp = new Handler();
					mCore.retrievePaymentMethodHelp(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
					tvints.setVisibility(View.GONE);
					pbints.setVisibility(View.VISIBLE);
					mHandlerHelp.postDelayed(new Runnable(){public void run(){
						jprintf("msHelpOutput: " + mCore.msHelpOutput);
						if(mCore.msHelpOutput.equals("")) {
							mHandlerHelp.postDelayed(this, 2000);
						} else {
							pbints.setVisibility(View.GONE);
							if(!mCore.msHelpOutput.equals("error")) {
								tvints.setText(mCore.msHelpOutput);
							} else {
								tvints.setText(getStringLang(STLN_UNABLEHELP));
								btnhlp.setEnabled(true);
							}
							tvints.setVisibility(View.VISIBLE);
							mHandlerHelp = null;
						}
					}}, 2000);
					btnhlp.setEnabled(false);
				}});
			    ell.addView(btnhlp);

				// add next button
				setButtonShape(btnnext, 2, BTNCOLOR);
				_setPadding(btnnext,30, mnButtonOffset, 30, 0);
				LinearLayout.LayoutParams blp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				//blp.weight = 1.0f;
				// blp.gravity = Gravity.CENTER;
				btnnext.setLayoutParams(blp);
				btnnext.setText(getStringLang(STLN_BTNNEXT));
				btnnext.setGravity(Gravity.CENTER);
				btnnext.setEnabled(false);
				btnnext.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				btnnext.setTextAppearance(MimopayActivity.this, getTextAppearance());
				btnnext.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
			    btnnext.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					// hide keyboard
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
					mCore.mHttppostCode = et.getText().toString();
					mCore.executeBtnAction();
				}});
			    ell.addView(btnnext);

		    rl.addView(ell);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));
	 
		ScrollView sv = new ScrollView(this);
		RelativeLayout.LayoutParams svlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  LayoutParams.WRAP_CONTENT);
		svlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		svlp.addRule(RelativeLayout.CENTER_VERTICAL);
		sv.setLayoutParams(svlp);
		sv.setVerticalScrollBarEnabled(false);
		sv.setHorizontalScrollBarEnabled(false);
        sv.addView(rl);
        mainlayout.addView(sv);
        setContentView(mainlayout, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}

	private void setupAirtimeDenomUI()
	{
        RelativeLayout mainlayout = new RelativeLayout(this);
		//mainlayout.setPadding(25,20,25,20);
		GradientDrawable rlshape =  new GradientDrawable();
		rlshape.setCornerRadius(4);
		rlshape.setColor(Color.WHITE);
		rlshape.setStroke(2, WNDSTKCOLOR);
		setBkgndCompat(mainlayout, rlshape);

			// form at center
		    RelativeLayout rl = new RelativeLayout(this);
			RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,  LayoutParams.WRAP_CONTENT);
			rllp.addRule(RelativeLayout.CENTER_IN_PARENT);
			rl.setLayoutParams(rllp);

			int id = 0;
			//int spaceid = 1000;

			// check storage
			Bitmap mlbmp = null;
		    final Button btnnext = new Button(this);

		    // title
		    TextView tvtitle = new TextView(this);
			tvtitle.setId(++id);
			RelativeLayout.LayoutParams tvtitlelp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			_setMargins(tvtitlelp,20,10,0,10);
			tvtitle.setLayoutParams(tvtitlelp);
			tvtitle.setText(getPaymentMethodTitle(MimopayStuff.sChannel));
			tvtitle.setTextAppearance(MimopayActivity.this, getTextAppearanceBig());
			tvtitle.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
		    tvtitle.setTextColor(TEXT_COLOR);
		    rl.addView(tvtitle);

			// // the logo
			// // if(storageokay) {
			// ImageView ivlogo = new ImageView(this);
			// ivlogo.setId(++id);
			// RelativeLayout.LayoutParams ivlogolp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			// // ivlogolp.addRule(RelativeLayout.ALIGN_TOP);
			// // ivlogolp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			// int left=0,top=0;
			// String sch = MimopayStuff.sChannel;
			// if(sch.equals("upoint") || sch.equals("xl_airtime")) {
			// 	top = 10;
			// } else if(sch.equals("mpoint") || sch.equals("dpoint") || sch.equals("celcom")) {
			// 	left = 15; top = 15;
			// }
			// ivlogolp.setMargins(left,top,0,20);
			// ivlogo.setLayoutParams(ivlogolp);
			// ivlogo.setScaleType(ImageView.ScaleType.CENTER);
			// // mlbmp = getRoundedCornerBitmap(mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx)), Color.WHITE);
			// mlbmp = mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx));
			// if(mlbmp != null) {
			// 	ivlogo.setImageBitmap(mlbmp);
			// }
			// rl.addView(ivlogo);
			// //rl.addView(inBetweenHorizontalSpace(++spaceid, 5, id));

			mCore.mHttppostCode = null;

		    //add radio buttons
		    RadioGroup rg = new RadioGroup(this);
			rg.setId(++id);
			rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				public void onCheckedChanged(RadioGroup rg, int cid) {
					//mCore.mAlsActiveIdx = idx;
					String s = mCore.alsDenomKey.get(mCore.mAlsActiveIdx);
					int did = cid - 100;
					try {
						JSONArray jarr = new JSONArray(s);
						mCore.mHttppostCode = jarr.getString(did);
					} catch(JSONException e) {}
					jprintf(
						"cid: " + Integer.toString(cid) +
						" mCore.mHttppostCode: " + mCore.mHttppostCode +
						" mCore.mAlsActiveIdx: " + Integer.toString(mCore.mAlsActiveIdx)
					);
					btnnext.setEnabled(true);
				}
			});
			RelativeLayout.LayoutParams rglp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			rglp.addRule(RelativeLayout.BELOW, id-1);
		    rg.setOrientation(RadioGroup.VERTICAL);
			_setMargins(rglp,20,0,0,0);
			rg.setLayoutParams(rglp);
			RadioButton[] rb = null;

			try {
				// String s = mCore.alsDenomAmount.get(mCore.mAlsActiveIdx);
				// if(s.isEmpty()) s = mCore.alsDenomValue.get(mCore.mAlsActiveIdx);
				String s = mCore.alsDenomValue.get(mCore.mAlsActiveIdx);
				JSONArray jarr = new JSONArray(s);
				int i,k,j=jarr.length();
				rb = new RadioButton[j];
				for(i=0,k=0; i<j; i++) {
					rb[k] = new RadioButton(this);
					String ss = jarr.getString(i);
					jprintf("jarr: " + ss + " i: " + Integer.toString(i));
					rb[k].setText(ss);
					rb[k].setId(100+i);
					RadioGroup.LayoutParams rblp = new RadioGroup.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					rb[k].setLayoutParams(rblp);
					rb[k].setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
					rb[k].setTextAppearance(MimopayActivity.this, getTextAppearance());
				    rb[k].setTextColor(TEXT_COLOR);
					rg.addView(rb[k]);
					k++;
				}
			} catch(JSONException e) {
				jprintf("setupDenomUI,JSONException: " + e.toString());
				return;
			}
			rl.addView(rg);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));

			// disclaimer
			//String sz = mCore.alsDenomAmount.get(mCore.mAlsActiveIdx);
			String stnc = mCore.alsStringTnc.get(mCore.mAlsActiveIdx);
			//jprintf(String.format("sz=%s stnc=%s", sz, stnc));
			RelativeLayout erl = new RelativeLayout(this);
			erl.setId(++id);
			RelativeLayout.LayoutParams erllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			erllp.addRule(RelativeLayout.BELOW, id-1);
			_setMargins(erllp,20,10,20,0);
			erl.setLayoutParams(erllp);
			    TextView tvtnc = new TextView(this);
				RelativeLayout.LayoutParams tvtnclp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				//_setMargins(tvtnclp,10,10,0,10);
				tvtnclp.addRule(RelativeLayout.CENTER_IN_PARENT);
				tvtnc.setLayoutParams(tvtnclp);
				tvtnc.setText(stnc);
				tvtnc.setTextAppearance(MimopayActivity.this, getTextAppearance());
				//tvtnc.setTypeface(Typeface.create("sans-serif", Typeface.ITALIC));
			    tvtnc.setTextColor(TEXT_COLOR);
			    erl.addView(tvtnc);
		    rl.addView(erl);

		    RelativeLayout rlinstr = new RelativeLayout(this);
			rlinstr.setId(++id);
			RelativeLayout.LayoutParams rlinstrlp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			rlinstrlp.addRule(RelativeLayout.BELOW, id-1);
			rlinstrlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			_setMargins(rlinstrlp,20,10,20,0);
			rlinstr.setLayoutParams(rlinstrlp);
				final TextView tvints = new TextView(this);
				RelativeLayout.LayoutParams tvintslp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				tvints.setLayoutParams(tvintslp);
				tvints.setText("");
				tvints.setTypeface(Typeface.create("sans-serif", Typeface.ITALIC));
				if(mnTextAppearSmaller > 0) {
					tvints.setTextSize(TypedValue.COMPLEX_UNIT_PX, mnTextAppearSmaller);
				} else {
					tvints.setTextAppearance(MimopayActivity.this, getTextAppearance());
				}
			    tvints.setTextColor(BTNCOLOR2);
				rlinstr.addView(tvints);
				final ProgressBar pbints = new ProgressBar(this, null, android.R.attr.progressBarStyle);
				RelativeLayout.LayoutParams pbintslp = new RelativeLayout.LayoutParams(50, 50);
				pbintslp.addRule(RelativeLayout.CENTER_HORIZONTAL);
				pbints.setLayoutParams(pbintslp);
				pbints.setIndeterminate(true);
				pbints.getIndeterminateDrawable().setColorFilter(BTNCOLOR, Mode.MULTIPLY);
				pbints.setVisibility(View.GONE);
				rlinstr.addView(pbints);
			rl.addView(rlinstr);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 20, id));

			LinearLayout ell = new LinearLayout(this);
			ell.setId(++id);
			RelativeLayout.LayoutParams elllp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			elllp.addRule(RelativeLayout.BELOW, id-1);
			elllp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			_setMargins(elllp,0,0,20,10);
			ell.setLayoutParams(elllp);

				// dummy space
				// RelativeLayout rldummy = new RelativeLayout(this);
				// LinearLayout.LayoutParams rldummylp = new LinearLayout.LayoutParams(0,  LayoutParams.WRAP_CONTENT);
				// rldummylp.weight = 0.75f;
				// rldummy.setLayoutParams(rldummylp);
				// ell.addView(rldummy);

				// add top up button
			    final Button btnhlp = new Button(this);
				setButtonShape(btnhlp, 1, BTNCOLOR2);
				LinearLayout.LayoutParams bhlp = new LinearLayout.LayoutParams((int)(40*mScale+0.5f), LayoutParams.WRAP_CONTENT);
				// bhlp.weight = 0.35f;
				// bhlp.gravity = Gravity.CENTER;
				btnhlp.setLayoutParams(bhlp);
				btnhlp.setText("?");
				btnhlp.setGravity(Gravity.CENTER);
				btnhlp.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
				btnhlp.setTextAppearance(MimopayActivity.this, getTextAppearance());
				btnhlp.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
			    btnhlp.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					if(mHandlerHelp != null) return;
					mHandlerHelp = new Handler();
					mCore.retrievePaymentMethodHelp(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
					tvints.setVisibility(View.GONE);
					pbints.setVisibility(View.VISIBLE);
					mHandlerHelp.postDelayed(new Runnable(){public void run(){
						jprintf("msHelpOutput: " + mCore.msHelpOutput);
						if(mCore.msHelpOutput.equals("")) {
							mHandlerHelp.postDelayed(this, 2000);
						} else {
							pbints.setVisibility(View.GONE);
							if(!mCore.msHelpOutput.equals("error")) {
								tvints.setText(mCore.msHelpOutput);
							} else {
								tvints.setText(getStringLang(STLN_UNABLEHELP));
								btnhlp.setEnabled(true);
							}
							tvints.setVisibility(View.VISIBLE);
							mHandlerHelp = null;
						}
					}}, 2000);
					btnhlp.setEnabled(false);
				}});
			    ell.addView(btnhlp);

				// add next button
				setButtonShape(btnnext, 2, BTNCOLOR);
				_setPadding(btnnext, 30, mnButtonOffset, 30, 0);
				LinearLayout.LayoutParams blp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				//blp.weight = 1.0f;
				//blp.gravity = Gravity.CENTER;
				btnnext.setLayoutParams(blp);
				btnnext.setText(getStringLang(STLN_BTNNEXT));
				btnnext.setGravity(Gravity.CENTER);
				btnnext.setEnabled(false);
				btnnext.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				btnnext.setTextAppearance(MimopayActivity.this, getTextAppearance());
				btnnext.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
			    btnnext.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					if(mCore.mHttppostCode != null && !mCore.mHttppostCode.equals("")) {
						String s = getMyPhoneNumber();
						if(s == null) {
							mbCannotSMS = true;
						}
						setupUserPhoneNumberUI();
					}
				}});
			    ell.addView(btnnext);

		    rl.addView(ell);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));

		ScrollView sv = new ScrollView(this);
		RelativeLayout.LayoutParams svlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  LayoutParams.WRAP_CONTENT);
		svlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		svlp.addRule(RelativeLayout.CENTER_VERTICAL);
		_setMargins(svlp,0,0,0,6);
		sv.setLayoutParams(svlp);
		sv.setVerticalScrollBarEnabled(false);
		sv.setHorizontalScrollBarEnabled(false);
        sv.addView(rl);
        mainlayout.addView(sv);
        setContentView(mainlayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}

	private TextView lineSeparator(int id, int below)
	{
	    TextView lineseptv = new TextView(this, null, android.R.attr.listSeparatorTextViewStyle);
		lineseptv.setId(id);
		RelativeLayout.LayoutParams lineseptvlp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, 2);
		lineseptvlp.addRule(RelativeLayout.BELOW, below);
		lineseptv.setLayoutParams(lineseptvlp);
	    //lineseptv.setTextColor(TEXT_COLOR);
	    lineseptv.setBackgroundColor(BTNCOLOR);
		return lineseptv;
	}

	private RelativeLayout inBetweenHorizontalSpace(int id, int height, int below)
	{
		RelativeLayout sep = new RelativeLayout(this);
		sep.setId(id);
		RelativeLayout.LayoutParams seplp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, height);
		seplp.addRule(RelativeLayout.BELOW, below);
		sep.setLayoutParams(seplp);
		return sep;
	}

	private void setupUserPhoneNumberUI()
	{
		//jprintf("setupUserPhoneNumberUI:mCore.mHttppostCode="+mCore.mHttppostCode);

        RelativeLayout mainlayout = new RelativeLayout(this);
		//mainlayout.setPadding(25,20,25,20);
		GradientDrawable rlshape =  new GradientDrawable();
		rlshape.setCornerRadius(4);
		rlshape.setColor(Color.WHITE);
		rlshape.setStroke(2, WNDSTKCOLOR);
		setBkgndCompat(mainlayout, rlshape);

			// form at center
		    RelativeLayout rl = new RelativeLayout(this);
			RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,  LayoutParams.WRAP_CONTENT);
			rllp.addRule(RelativeLayout.CENTER_IN_PARENT);
			rl.setLayoutParams(rllp);

			int id = 0;
			//int spaceid = 1000;

		    // title
		    TextView tvtitle = new TextView(this);
			tvtitle.setId(++id);
			RelativeLayout.LayoutParams tvtitlelp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			_setMargins(tvtitlelp,20,10,0,10);
			tvtitle.setLayoutParams(tvtitlelp);
			tvtitle.setText(getPaymentMethodTitle(MimopayStuff.sChannel));
			tvtitle.setTextAppearance(MimopayActivity.this, getTextAppearanceBig());
			tvtitle.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
		    tvtitle.setTextColor(TEXT_COLOR);
		    rl.addView(tvtitle);

			// ImageView ivlogo = new ImageView(this);
			// ivlogo.setId(++id);
			// RelativeLayout.LayoutParams ivlogolp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			// // ivlogolp.addRule(RelativeLayout.ALIGN_TOP);
			// // ivlogolp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			// int left=0,top=0;
			// String sch = MimopayStuff.sChannel;
			// if(sch.equals("upoint") || sch.equals("xl_airtime")) {
			// 	top = 10;
			// } else if(sch.equals("mpoint") || sch.equals("dpoint") || sch.equals("celcom")) {
			// 	left = 15; top = 15;
			// }
			// ivlogolp.setMargins(left,top,0,20);
			// //ivlogolp.setMargins(0,10,0,20);
			// ivlogo.setLayoutParams(ivlogolp);
			// ivlogo.setScaleType(ImageView.ScaleType.CENTER);
			// if(mCore.mAlsActiveIdx >= 0) {
			// 	//Bitmap mlbmp = getRoundedCornerBitmap(mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx)), Color.WHITE);
			// 	Bitmap mlbmp = mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx));
			// 	if(mlbmp != null) {
			// 		ivlogo.setImageBitmap(mlbmp);
			// 	}
			// }
			// rl.addView(ivlogo);
			// //rl.addView(inBetweenHorizontalSpace(++spaceid, 20, id));

			// add textview
		    TextView tv = new TextView(this);
			tv.setId(++id);
			RelativeLayout.LayoutParams tvlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			tvlp.addRule(RelativeLayout.BELOW, id-1);
			_setMargins(tvlp,20,0,20,10);
			tv.setLayoutParams(tvlp);
			String ss = getStringLang(STLN_TEXTPHNUM);
			//if(!MimopayStuff.sAmount.equals("0")) {
			if(!mCore.mHttppostCode.equals("0")) {
				ss = getStringLang(STLN_PRODUCTNAME);
				try {
					JSONArray jarr = new JSONArray(mCore.alsDenomKey.get(0));
					//String sb = mCore.alsDenomValue.get(0);
					String sda = mCore.alsDenomAmount.get(0);
					// String sb = mCore.alsDenomAmount.get(0);
					// if(sb.isEmpty()) sb = mCore.alsDenomValue.get(0);
					//JSONArray jarrVal = new JSONArray(sb);
					JSONArray jarrAmount = new JSONArray(sda);
					for(int b=0;b<jarr.length();b++) {
						String sa = jarr.getString(b);
						// jprintf(String.format("amount[%d]=%s", b, sa));
						// jprintf(String.format("amount[%d]=%s", b, jarrVal.getString(b)));
						//if(sa.equals(MimopayStuff.sAmount)) {
						if(sa.equals(mCore.mHttppostCode)) {
							String ssda = jarrAmount.getString(b);
							if(MimopayStuff.sChannel.equals("upoint")) {
								float f = Float.parseFloat(sa) * 1.1f;
								ss += ("\n" + MimopayStuff.sProductName + " (" + MimopayStuff.sCurrency + " " + Integer.toString((int)f) + ")");
							} else {
								ss += ("\n" + MimopayStuff.sProductName + " (" + MimopayStuff.sCurrency + " " + ssda + ")");
							}
							break;
						}
					}
				} catch(JSONException e) {}
			}
			tv.setText(ss);
			tv.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
			tv.setTextAppearance(MimopayActivity.this, getTextAppearance());
		    tv.setTextColor(TEXT_COLOR);
		    rl.addView(tv);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));
	 
		    final Button btn = new Button(this);
			final EditText et = new EditText(this);
			et.setId(++id);
			RelativeLayout.LayoutParams etlp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			etlp.addRule(RelativeLayout.BELOW, id-1);
			etlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			_setMargins(etlp,20,0,20,10);
			et.setLayoutParams(etlp);
			if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
				GradientDrawable etshape =  new GradientDrawable();
				etshape.setCornerRadius(9);
				etshape.setColor(Color.TRANSPARENT);
				etshape.setStroke(2, BTNCOLOR);
				setBkgndCompat(et, etshape);
			}
			et.setMinLines(1);
			et.setMaxLines(3);
			et.setHint(getStringLang(STLN_PHONENUM));
			et.getBackground().setColorFilter(BTNCOLOR, PorterDuff.Mode.MULTIPLY);  
			et.setFilters(new InputFilter[] { voucherfilter });
			et.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_PHONE);
			et.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
			et.setTextAppearance(MimopayActivity.this, getTextAppearance());
	        et.addTextChangedListener(new EditTextWatcher(et) { @Override public void afterTextChanged(Editable s) {
				if(s.length() >= 10) {
					btn.setEnabled(true);
				} else {
					btn.setEnabled(false);
				}
			}});
			et.setLongClickable(true);
			String slastphonenum = getLastUsedPhoneNumber(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
			if(slastphonenum != null) { 
				et.setText(slastphonenum);
			}

			rl.addView(et);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 15, id));
	 
			String sdisclaim = "";
			///////////////////////////////////////////////////////
			// mbCannotSMS = false;
			///////////////////////////////////////////////////////
			if(mbCannotSMS) {
				sdisclaim = getStringLang(STLN_NOSUPPGSM);
			} else {
				if(MimopayStuff.sChannel.equals("upoint")) {
					sdisclaim = getStringLang(STLN_CHECKBOXUPOINT);
				} else if(MimopayStuff.sChannel.equals("xl_airtime")) {
					sdisclaim = getStringLang(STLN_CHECKBOXXL);
				} else if(MimopayStuff.sChannel.equals("indosat_airtime")) {
					sdisclaim = getStringLang(STLN_CHECKBOXINDOSAT);
				} else if(MimopayStuff.sChannel.equals("mpoint")) {
					sdisclaim = getStringLang(STLN_CHECKBOXMPOINT);
				} else if(MimopayStuff.sChannel.equals("dpoint")) {
					sdisclaim = getStringLang(STLN_CHECKBOXDPOINT);
				} else if(MimopayStuff.sChannel.equals("celcom")) {
					sdisclaim = getStringLang(STLN_CHECKBOXCELCOM);
				}
			}
			// disclaimer SMS send
			final CheckBox cbdisclaim = new CheckBox(this);
			cbdisclaim.setId(++id);
			RelativeLayout.LayoutParams cbdisclaimlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			cbdisclaimlp.addRule(RelativeLayout.BELOW, id-1);
			_setMargins(cbdisclaimlp,20,10,20,10);
			cbdisclaim.setLayoutParams(cbdisclaimlp);
			cbdisclaim.setText(sdisclaim);
			cbdisclaim.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
			cbdisclaim.setTextAppearance(MimopayActivity.this, getTextAppearance());
		    cbdisclaim.setTextColor(TEXT_COLOR);
			cbdisclaim.setGravity(Gravity.TOP);
			// float ft = cbdisclaim.getTextSize();
			// cbdisclaim.setTextSize(ft * 0.6f);
			if(mbCannotSMS) {
				cbdisclaim.setVisibility(View.GONE);
			} else {
				cbdisclaim.setChecked(true);
			}
			rl.addView(cbdisclaim);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 20, id));

			// disclaimer
			//String sz = mCore.alsDenomAmount.get(mCore.mAlsActiveIdx);
			String stnc = mCore.alsStringTnc.get(mCore.mAlsActiveIdx);
			//jprintf(String.format("sz=%s stnc=%s", sz, stnc));
			RelativeLayout erl = new RelativeLayout(this);
			erl.setId(++id);
			RelativeLayout.LayoutParams erllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			erllp.addRule(RelativeLayout.BELOW, id-1);
			_setMargins(erllp,20,10,20,0);
			erl.setLayoutParams(erllp);
			    TextView tvtnc = new TextView(this);
				RelativeLayout.LayoutParams tvtnclp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				//_setMargins(tvtnclp,10,10,0,10);
				tvtnclp.addRule(RelativeLayout.CENTER_IN_PARENT);
				tvtnc.setLayoutParams(tvtnclp);
				tvtnc.setText(stnc);
				tvtnc.setTextAppearance(MimopayActivity.this, getTextAppearance());
				//tvtnc.setTypeface(Typeface.create("sans-serif", Typeface.ITALIC));
			    tvtnc.setTextColor(TEXT_COLOR);
			    erl.addView(tvtnc);
		    rl.addView(erl);

			LinearLayout ell = new LinearLayout(this);
			ell.setId(++id);
			RelativeLayout.LayoutParams elllp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			elllp.addRule(RelativeLayout.BELOW, id-1);
			elllp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			_setMargins(elllp,0,30,20,10);
			ell.setLayoutParams(elllp);

				// dummy space
				// RelativeLayout rldummy = new RelativeLayout(this);
				// LinearLayout.LayoutParams rldummylp = new LinearLayout.LayoutParams(0,  LayoutParams.WRAP_CONTENT);
				// rldummylp.weight = 1.0f;
				// rldummy.setLayoutParams(rldummylp);
				// ell.addView(rldummy);

				// add next button
				setButtonShape(btn, 0, BTNCOLOR);
				_setPadding(btn, 30, mnButtonOffset, 30, 0);
				LinearLayout.LayoutParams blp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				// blp.weight = 1.0f;
				// blp.gravity = Gravity.CENTER;
				btn.setLayoutParams(blp);
				btn.setText(getStringLang(STLN_BTNNEXT));
			    btn.setGravity(Gravity.CENTER);
				if(slastphonenum == null) {
					btn.setEnabled(false);
				}
				btn.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				btn.setTextAppearance(MimopayActivity.this, getTextAppearance());
				btn.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
			    btn.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					// hide keyboard
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
					mbCannotSMS = !cbdisclaim.isChecked();
					mCore.mHttppostPhoneNumber =  et.getText().toString();
					jprintf("mHttppostPhoneNumber: " + mCore.mHttppostPhoneNumber);
					mCore.executeBtnAction();
					if(!mbCannotSMS && (MimopayStuff.sChannel.equals("xl_airtime") || MimopayStuff.sChannel.equals("indosat_airtime") || MimopayStuff.sChannel.equals("mpoint") || MimopayStuff.sChannel.equals("dpoint") || MimopayStuff.sChannel.equals("celcom"))) {
						mCore.waitSMS(60);	// 30 seconds
					}
				}});
			    ell.addView(btn);

		    rl.addView(ell);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));

		ScrollView sv = new ScrollView(this);
		RelativeLayout.LayoutParams svlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  LayoutParams.WRAP_CONTENT);
		svlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		svlp.addRule(RelativeLayout.CENTER_VERTICAL);
		sv.setLayoutParams(svlp);
		sv.setVerticalScrollBarEnabled(false);
		sv.setHorizontalScrollBarEnabled(false);
        sv.addView(rl);
        mainlayout.addView(sv);
        setContentView(mainlayout, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}

	private void setupAtmChoosenUI()
	{
        RelativeLayout mainlayout = new RelativeLayout(this);
		//mainlayout.setPadding(25,20,25,20);
		GradientDrawable rlshape =  new GradientDrawable();
		rlshape.setCornerRadius(4);
		rlshape.setColor(Color.WHITE);
		rlshape.setStroke(2, WNDSTKCOLOR);
		setBkgndCompat(mainlayout, rlshape);

			// form at center
		    RelativeLayout rl = new RelativeLayout(this);
			RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,  LayoutParams.WRAP_CONTENT);
			rllp.addRule(RelativeLayout.CENTER_IN_PARENT);
			rl.setLayoutParams(rllp);

			int id = 0;

		    // title
		    TextView tvtitle = new TextView(this);
			tvtitle.setId(++id);
			RelativeLayout.LayoutParams tvtitlelp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			_setMargins(tvtitlelp,20,10,0,10);
			tvtitle.setLayoutParams(tvtitlelp);
			tvtitle.setText(getPaymentMethodTitle(MimopayStuff.sChannel));
			tvtitle.setTextAppearance(MimopayActivity.this, getTextAppearanceBig());
			tvtitle.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
		    tvtitle.setTextColor(TEXT_COLOR);
		    rl.addView(tvtitle);

			// // the logo
			// ImageView ivlogo = new ImageView(this);
			// ivlogo.setId(++id);
			// RelativeLayout.LayoutParams ivlogolp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			// // ivlogolp.addRule(RelativeLayout.ALIGN_TOP);
			// // ivlogolp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			// ivlogolp.setMargins(15,15,0,15);
			// ivlogo.setLayoutParams(ivlogolp);
			// //Bitmap mlbmp = getRoundedCornerBitmap(mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx)), Color.WHITE);
			// Bitmap mlbmp = mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx));
			// if(mlbmp != null) {
			// 	ivlogo.setImageBitmap(mlbmp);
			// }
			// rl.addView(ivlogo);
			// //rl.addView(inBetweenHorizontalSpace(++spaceid, 20, id));

		    TextView tv = new TextView(this);
			tv.setId(++id);
			RelativeLayout.LayoutParams tvlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			tvlp.addRule(RelativeLayout.BELOW, id-1);
			_setMargins(tvlp,20,0,20,0);
			tv.setLayoutParams(tvlp);
			String z = "";
			int hrgbrg = Integer.parseInt(mCore.alsDenomKey.get(mCore.mAlsActiveIdx));
			int tottag = Integer.parseInt(mCore.alsTextEditName.get(mCore.mAlsActiveIdx));
			z =  ("Nama Produk: " + MimopayStuff.sProductName + "\n");
			z += ("Harga : Rp. " + Integer.toString(hrgbrg) + "\n");
			z += ("Biaya Transaksi : Rp. " + Integer.toString(tottag-hrgbrg) + "\n\n");
			z += ("Jumlah Tagihan : Rp. " + Integer.toString(tottag) + "\n");
		    tv.setText(z);
			String lines[] = z.split("\\r?\\n");
			tv.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
			tv.setTextAppearance(MimopayActivity.this, getTextAppearance());
		    tv.setTextColor(TEXT_COLOR);
		    rl.addView(tv);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 20, id));

			LinearLayout ell = new LinearLayout(this);
			ell.setId(++id);
			RelativeLayout.LayoutParams elllp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			elllp.addRule(RelativeLayout.BELOW, id-1);
			elllp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			_setMargins(elllp,20,30,20,10);
			ell.setLayoutParams(elllp);

				// dummy space
				// RelativeLayout rldummy = new RelativeLayout(this);
				// LinearLayout.LayoutParams rldummylp = new LinearLayout.LayoutParams(0,  LayoutParams.WRAP_CONTENT);
				// rldummylp.weight = 1.0f;
				// rldummy.setLayoutParams(rldummylp);
				// ell.addView(rldummy);

				// add next button
			    Button btn = new Button(this);
				setButtonShape(btn, 0, BTNCOLOR);
				_setPadding(btn, 30, mnButtonOffset, 30, 0);
				LinearLayout.LayoutParams blp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				// blp.weight = 1.0f;
				// blp.gravity = Gravity.CENTER;
				btn.setLayoutParams(blp);
				// setButtonShape(btn, 0, BTNCOLOR);
				// btn.setPadding(30,10,30,10);
				// RelativeLayout.LayoutParams btnlp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				// btnlp.addRule(RelativeLayout.BELOW, spaceid);
				// btnlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
				// btn.setLayoutParams(btnlp);
				btn.setText(getStringLang(STLN_BTNNEXT));
				btn.setGravity(Gravity.CENTER);
				btn.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				btn.setTextAppearance(MimopayActivity.this, getTextAppearance());
				btn.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
			    btn.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					mCore.mHttppostCode = mCore.alsDenomKey.get(mCore.mAlsActiveIdx);
					mCore.executeBtnAction();
				}});
			    ell.addView(btn);

		    rl.addView(ell);

		ScrollView sv = new ScrollView(this);
		RelativeLayout.LayoutParams svlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  LayoutParams.WRAP_CONTENT);
		svlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		svlp.addRule(RelativeLayout.CENTER_VERTICAL);
		sv.setLayoutParams(svlp);
		sv.setVerticalScrollBarEnabled(false);
		sv.setHorizontalScrollBarEnabled(false);
        sv.addView(rl);
        mainlayout.addView(sv);
        setContentView(mainlayout, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}

	private void setupDenomAtmResultUI(String companyCode, String totalBill, String transId)
	{
        RelativeLayout mainlayout = new RelativeLayout(this);
		//mainlayout.setPadding(25,20,25,20);
		GradientDrawable rlshape =  new GradientDrawable();
		rlshape.setCornerRadius(4);
		rlshape.setColor(Color.WHITE);
		rlshape.setStroke(2, WNDSTKCOLOR);
		setBkgndCompat(mainlayout, rlshape);

			// form at center
		    RelativeLayout rl = new RelativeLayout(this);
			RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,  LayoutParams.WRAP_CONTENT);
			rllp.addRule(RelativeLayout.CENTER_IN_PARENT);
			rl.setLayoutParams(rllp);

			int id = 0;
			//int spaceid = 1000;
			Bitmap mlbmp = null;

		    // title
		    TextView tvtitle = new TextView(this);
			tvtitle.setId(++id);
			RelativeLayout.LayoutParams tvtitlelp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			_setMargins(tvtitlelp,20,10,0,10);
			tvtitle.setLayoutParams(tvtitlelp);
			tvtitle.setText(getPaymentMethodTitle(MimopayStuff.sChannel));
			tvtitle.setTextAppearance(MimopayActivity.this, getTextAppearanceBig());
			tvtitle.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
		    tvtitle.setTextColor(TEXT_COLOR);
		    rl.addView(tvtitle);

			// // the logo
			// ImageView ivlogo = new ImageView(this);
			// ivlogo.setId(++id);
			// RelativeLayout.LayoutParams ivlogolp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			// // ivlogolp.addRule(RelativeLayout.ALIGN_TOP);
			// // ivlogolp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			// ivlogolp.setMargins(15,15,0,15);
			// ivlogo.setLayoutParams(ivlogolp);
			// //mlbmp = getRoundedCornerBitmap(mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx)), Color.WHITE);
			// mlbmp = mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx));
			// if(mlbmp != null) {
			// 	ivlogo.setImageBitmap(mlbmp);
			// }
			// rl.addView(ivlogo);
			// //rl.addView(inBetweenHorizontalSpace(++spaceid, 20, id));

		    TextView tv = new TextView(this);
			tv.setId(++id);
			RelativeLayout.LayoutParams tvlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			tvlp.addRule(RelativeLayout.BELOW, id-1);
			_setMargins(tvlp,15,0,15,15);
			tv.setLayoutParams(tvlp);
			String z = "";
			String type = mCore.alsBtnAction.get(mCore.mAlsActiveIdx);
			if(type.indexOf("bca") != -1) {
				z  = ("Kode Perusahaan: " + companyCode + "\n");
				z += ("Jumlah Tagihan : Rp. " + totalBill + "\n");
				z += ("ID Transaksi : " + transId);
			} else if(type.indexOf("bersama") != -1) {
				z  = ("Jumlah Tagihan : Rp. " + totalBill + "\n");
				z += ("Kode Bank: " + companyCode + "\n");
				z += ("Nomor Rekening Tujuan : " + transId);
			}
		    tv.setText(z);
			tv.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
			tv.setTextAppearance(MimopayActivity.this, getTextAppearance());
		    tv.setTextColor(TEXT_COLOR);
		    rl.addView(tv);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));		

		    RelativeLayout rlinstr = new RelativeLayout(this);
			rlinstr.setId(++id);
			RelativeLayout.LayoutParams rlinstrlp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			rlinstrlp.addRule(RelativeLayout.BELOW, id-1);
			//rlinstrlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			_setMargins(rlinstrlp,15,0,15,0);
			rlinstr.setLayoutParams(rlinstrlp);
				final TextView tvints = new TextView(this);
				RelativeLayout.LayoutParams tvintslp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				tvints.setLayoutParams(tvintslp);
				tvints.setText("");
				tvints.setTypeface(Typeface.create("sans-serif", Typeface.ITALIC));
				if(mnTextAppearSmaller > 0) {
					tvints.setTextSize(TypedValue.COMPLEX_UNIT_PX, mnTextAppearSmaller);
				} else {
					tvints.setTextAppearance(MimopayActivity.this, getTextAppearance());
				}
			    tvints.setTextColor(BTNCOLOR2);
				rlinstr.addView(tvints);
				final ProgressBar pbints = new ProgressBar(this, null, android.R.attr.progressBarStyle);
				RelativeLayout.LayoutParams pbintslp = new RelativeLayout.LayoutParams(50, 50);
				pbintslp.addRule(RelativeLayout.CENTER_HORIZONTAL);
				pbints.setLayoutParams(pbintslp);
				pbints.setIndeterminate(true);
				pbints.getIndeterminateDrawable().setColorFilter(BTNCOLOR, Mode.MULTIPLY);
				pbints.setVisibility(View.GONE);
				rlinstr.addView(pbints);
			rl.addView(rlinstr);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 20, id));

			LinearLayout ell = new LinearLayout(this);
			ell.setId(++id);
			RelativeLayout.LayoutParams elllp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			elllp.addRule(RelativeLayout.BELOW, id-1);
			elllp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			_setMargins(elllp,0,20,20,10);
			ell.setLayoutParams(elllp);

				// dummy space
				// RelativeLayout rldummy = new RelativeLayout(this);
				// LinearLayout.LayoutParams rldummylp = new LinearLayout.LayoutParams(0,  LayoutParams.WRAP_CONTENT);
				// rldummylp.weight = 0.75f;
				// rldummy.setLayoutParams(rldummylp);
				// ell.addView(rldummy);

				// add top up button
			    final Button btnhlp = new Button(this);
				setButtonShape(btnhlp, 1, BTNCOLOR2);
				LinearLayout.LayoutParams bhlp = new LinearLayout.LayoutParams((int)(40*mScale+0.5f), LayoutParams.WRAP_CONTENT);
				// bhlp.weight = 0.35f;
				// bhlp.gravity = Gravity.CENTER;
				btnhlp.setLayoutParams(bhlp);
				btnhlp.setText("?");
				btnhlp.setGravity(Gravity.CENTER);
				btnhlp.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
				btnhlp.setTextAppearance(MimopayActivity.this, getTextAppearance());
				btnhlp.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
			    btnhlp.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					if(mHandlerHelp != null) return;
					mHandlerHelp = new Handler();
					mCore.retrievePaymentMethodHelp(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
					tvints.setVisibility(View.GONE);
					pbints.setVisibility(View.VISIBLE);
					mHandlerHelp.postDelayed(new Runnable(){public void run(){
						jprintf("msHelpOutput: " + mCore.msHelpOutput);
						if(mCore.msHelpOutput.equals("")) {
							mHandlerHelp.postDelayed(this, 2000);
						} else {
							pbints.setVisibility(View.GONE);
							if(!mCore.msHelpOutput.equals("error")) {
								tvints.setText(mCore.msHelpOutput);
							} else {
								tvints.setText(getStringLang(STLN_UNABLEHELP));
								btnhlp.setEnabled(true);
							}
							tvints.setVisibility(View.VISIBLE);
							mHandlerHelp = null;
						}
					}}, 2000);
					btnhlp.setEnabled(false);
				}});
			    ell.addView(btnhlp);

				// add next button
				final Button btndone = new Button(this);
				setButtonShape(btndone, 2, BTNCOLOR);
				_setPadding(btndone, 30, mnButtonOffset, 30, 0);
				LinearLayout.LayoutParams blp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				// blp.weight = 1.0f;
				// blp.gravity = Gravity.CENTER;
				btndone.setLayoutParams(blp);
				btndone.setText(getStringLang(STLN_BTNDONE));
				btndone.setGravity(Gravity.CENTER);
				btndone.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				btndone.setTextAppearance(MimopayActivity.this, getTextAppearance());
				btndone.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
				final String fcompanyCode = companyCode;
				final String ftotalBill = totalBill;
				final String ftransId = transId;
			    btndone.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					mInfoResult = "SUCCESS";
					mAlsResult.clear();
					mAlsResult.add(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
					mAlsResult.add(fcompanyCode);
					mAlsResult.add(ftotalBill);
					mAlsResult.add(ftransId);
					finish();
				}});
			    ell.addView(btndone);

		    rl.addView(ell);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));

		ScrollView sv = new ScrollView(this);
		RelativeLayout.LayoutParams svlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  LayoutParams.WRAP_CONTENT);
		svlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		svlp.addRule(RelativeLayout.CENTER_VERTICAL);
		sv.setLayoutParams(svlp);
		sv.setVerticalScrollBarEnabled(false);
		sv.setHorizontalScrollBarEnabled(false);
        sv.addView(rl);
        mainlayout.addView(sv);
        setContentView(mainlayout, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}

	private void setButtonShape(Object obj, int ntype, int ncolor)
	{
        StateListDrawable btnsld = new StateListDrawable();
		if(obj instanceof RadioButton) {
			btnsld.addState(new int[] {android.R.attr.state_checked}, getResources().getDrawable(android.R.drawable.presence_online));
			btnsld.addState(new int[] {-android.R.attr.state_checked}, getResources().getDrawable(android.R.drawable.presence_invisible));
			RadioButton rb = (RadioButton) obj;
			rb.setButtonDrawable(btnsld);
			_setPadding(rb,30,-5,0,0);
			//setBkgndCompat(rb, btnsld);
			return;
		}
		if(obj instanceof CheckBox) {
			btnsld.addState(new int[] {android.R.attr.state_checked}, getResources().getDrawable(android.R.drawable.checkbox_on_background));
			btnsld.addState(new int[] {-android.R.attr.state_checked}, getResources().getDrawable(android.R.drawable.checkbox_off_background));
			CheckBox cb = (CheckBox) obj;
			cb.setButtonDrawable(btnsld);
			_setPadding(cb,30,-5,0,0);
			//setBkgndCompat(rb, btnsld);
			return;
		}
		int icolor = Color.rgb(255-Color.red(ncolor), 255-Color.green(ncolor), 255-Color.blue(ncolor));
		RDrawable btnupshape =  new RDrawable(ncolor, ntype, 8);//GradientDrawable.Orientation.BOTTOM_TOP, btncolors);
		RDrawable btndownshape = new RDrawable(icolor, ntype, 8);
		RDrawable btnoffshape = new RDrawable(Color.LTGRAY, ntype, 8);
		if(obj instanceof Button || obj instanceof ImageButton) {
			btnsld.addState(new int[] {android.R.attr.stateNotNeeded}, btndownshape); 
			btnsld.addState(new int[] {android.R.attr.state_pressed, android.R.attr.state_enabled}, btndownshape);
			btnsld.addState(new int[] {android.R.attr.state_focused, android.R.attr.state_enabled}, btnupshape);
			btnsld.addState(new int[] {android.R.attr.state_enabled}, btnupshape);
			btnsld.addState(new int[] {-android.R.attr.state_enabled}, btnoffshape);
			if(obj instanceof Button) {
				Button b = (Button) obj;
				_setPadding(b, 0, mnButtonOffset, 0, 0);
				b.setTextColor(0xffffffff);//TEXT_COLOR);
				setBkgndCompat(b, btnsld); //btnupshape);
			} else if(obj instanceof ImageButton) {
				ImageButton ib = (ImageButton) obj;
				setBkgndCompat(ib, btnsld); //btnupshape);
			}
		}
	}

	private int getIntRounding(float finp)
	{
		int ninp = (int)finp;
		float fa = finp - ((float)ninp);
		if(fa <= 0.5f) {
			return ninp;
		}
		return ninp+1;
	}

	private int mnButtonOffset = 0;

	public class RDrawable extends Drawable
	{
		private int nType = 0;
		private int nRadius = 0;
		private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		private Paint strokepaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		// 0 : semua sudut
		// 1 : sudut2 di sisi kiri
		// 2 : sudut2 di sisi kanan
		public RDrawable(int color, int type, int radius)
		{
			nType = type;
			nRadius = radius;
			paint.setColor(color);
			paint.setStyle(Paint.Style.FILL);
			//int icolor = Color.rgb(128-Color.red(color), 128-Color.green(color), 128-Color.blue(color));
			strokepaint.setStyle(Paint.Style.STROKE);
			strokepaint.setStrokeWidth(2);
			strokepaint.setColor(Color.LTGRAY);//icolor);
		}
		@Override public void draw(Canvas canvas)
		{
			int radius = 10; // note this is actual pixels
			int btnwidth = getBounds().width();
			int btnheight = getBounds().height();
			//btnwidth = (int)( ((float)btnwidth) * 0.65f );
			//int dh = 0(int) ((float)btnheight * 0.20f );
			//int htop = dh / 2;
			// if(mnButtonOffset == 0) {
			// 	mnButtonOffset = htop * -1;
			// }
			float frealheight = (float )btnheight;
			float fbtnheight = frealheight * 0.75f;
			float fselisih = frealheight - fbtnheight;
			int htop = getIntRounding(fselisih/2.0f);

			//btnheight = getIntRounding(fbtnheight);
			//int selisih = realheight - btnheight;
			// htop = selisih / 2;
			// htop += 15;

			jprintf(String.format("RDrawable:\nfrealheight:%.2f\nfbtnheight:%.2f\nhtop:%d\n", frealheight, fbtnheight, htop));
			// Paint apaint = new Paint(Paint.ANTI_ALIAS_FLAG);
			// apaint.setColor(Color.GREEN);
			// apaint.setStyle(Paint.Style.FILL);
			// Paint bpaint = new Paint(Paint.ANTI_ALIAS_FLAG);
			// bpaint.setColor(Color.MAGENTA);
			// bpaint.setStyle(Paint.Style.FILL);

			switch(nType) {
			case 1:
				canvas.drawRoundRect(new RectF(0, htop, btnwidth+nRadius, btnheight-htop), nRadius, nRadius, paint);
				//canvas.drawRect(new RectF(btnwidth, htop, btnwidth-nRadius, btnheight-htop), paint);
				break;
			case 2:
				canvas.drawRoundRect(new RectF(-1*nRadius, htop, btnwidth, btnheight-htop), nRadius, nRadius, paint);
				//canvas.drawRect(new RectF(0, htop, nRadius, btnheight-htop), paint);
				break;
			default:
				canvas.drawRoundRect(new RectF(0, htop, btnwidth, btnheight-htop), nRadius, nRadius, paint);
				canvas.drawRoundRect(new RectF(0, htop, btnwidth, btnheight-htop), nRadius, nRadius, strokepaint);
			}
		}
		@Override public void setAlpha(int i)
		{
			//.. not supported
		}
		@Override public void setColorFilter(ColorFilter colorFilter)
		{
			//.. not supported
		}
		@Override public int getOpacity()
		{
			return 1;
		}
	}

	private void setupTopUpResultUI(String stopupres, String stransid)	//, String message)
	{
        RelativeLayout mainlayout = new RelativeLayout(this);
		//mainlayout.setPadding(3,3,3,3);
		GradientDrawable rlshape =  new GradientDrawable();
		rlshape.setCornerRadius(4);
		rlshape.setColor(Color.WHITE);
		rlshape.setStroke(2, WNDSTKCOLOR);
		setBkgndCompat(mainlayout, rlshape);

			// form at center
		    RelativeLayout rl = new RelativeLayout(this);
			RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,  LayoutParams.WRAP_CONTENT);
			rllp.addRule(RelativeLayout.CENTER_IN_PARENT);
			rl.setLayoutParams(rllp);

			int id = 0;
			//int spaceid = 1000;

		    // title
		    TextView tvtitle = new TextView(this);
			tvtitle.setId(++id);
			RelativeLayout.LayoutParams tvtitlelp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			_setMargins(tvtitlelp,20,10,0,10);
			tvtitle.setLayoutParams(tvtitlelp);
			if(MimopayStuff.sPaymentMethod.equals("VnTelco")) {
				tvtitle.setText(getPaymentMethodTitle(mCore.alsTextEditName.get(mCore.mAlsActiveIdx)));
			} else {
				tvtitle.setText(getPaymentMethodTitle(MimopayStuff.sChannel));
			}
			tvtitle.setTextAppearance(MimopayActivity.this, getTextAppearanceBig());
			tvtitle.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
		    tvtitle.setTextColor(TEXT_COLOR);
		    rl.addView(tvtitle);

			// // the logo
			// ImageView ivlogo = new ImageView(this);
			// ivlogo.setId(++id);
			// RelativeLayout.LayoutParams ivlogolp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			// // ivlogolp.addRule(RelativeLayout.ALIGN_TOP);
			// // ivlogolp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			// int left=0,top=0;
			// String sch = MimopayStuff.sChannel;
			// if(sch.equals("sevelin")) {
			// 	left = 10; top = 10;
			// } else if(sch.equals("upoint_hrn") || sch.equals("xl_hrn")) {
			// 	top = 10;
			// } else if(MimopayStuff.sPaymentMethod.equals("VnTelco")) {
			// 	left = 15; top = 15;
			// }
			// ivlogolp.setMargins(left,top,0,20);
			// // ivlogolp.setMargins(0,0,0,20);
			// ivlogo.setLayoutParams(ivlogolp);
			// ivlogo.setScaleType(ImageView.ScaleType.CENTER);
			// // Bitmap mlbmp = getRoundedCornerBitmap(mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx)), Color.WHITE);
			// Bitmap mlbmp = mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx));
			// if(mlbmp != null) {
			// 	ivlogo.setImageBitmap(mlbmp);
			// }
			// rl.addView(ivlogo);
			// //rl.addView(inBetweenHorizontalSpace(++spaceid, 5, id));

		    TextView tv = new TextView(this);
			tv.setId(++id);
			RelativeLayout.LayoutParams tvlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			tvlp.addRule(RelativeLayout.BELOW, id-1);
			tvlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			_setMargins(tvlp,20,0,20,0);
			tv.setLayoutParams(tvlp);
		    tv.setText(stopupres);
			tv.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
			tv.setTextAppearance(MimopayActivity.this, getTextAppearance());
		    tv.setTextColor(TEXT_COLOR);
		    rl.addView(tv);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 5, id));

		    TextView tvb = new TextView(this);
			tvb.setId(++id);
			RelativeLayout.LayoutParams tvblp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			tvblp.addRule(RelativeLayout.BELOW, id-1);
			tvblp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			_setMargins(tvblp,20,0,20,0);
			tvb.setLayoutParams(tvblp);
			String strans = stransid;
		    tvb.setText(strans);
			tvb.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
			tvb.setTextAppearance(MimopayActivity.this, getTextAppearance());
		    tvb.setTextColor(TEXT_COLOR);
		    rl.addView(tvb);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 20, id));

			LinearLayout ell = new LinearLayout(this);
			ell.setId(++id);
			RelativeLayout.LayoutParams elllp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			elllp.addRule(RelativeLayout.BELOW, id-1);
			elllp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			_setMargins(elllp,0,30,20,10);
			ell.setLayoutParams(elllp);

				// dummy space
				// RelativeLayout rldummy = new RelativeLayout(this);
				// LinearLayout.LayoutParams rldummylp = new LinearLayout.LayoutParams(0,  LayoutParams.WRAP_CONTENT);
				// rldummylp.weight = 1.0f;
				// rldummy.setLayoutParams(rldummylp);
				// ell.addView(rldummy);

				// add next button
			    Button btn = new Button(this);
				setButtonShape(btn, 0, BTNCOLOR);
				_setPadding(btn, 30, mnButtonOffset, 30, 0);
				LinearLayout.LayoutParams blp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				// blp.weight = 1.0f;
				//blp.gravity = Gravity.CENTER;
				btn.setLayoutParams(blp);
				// btn.setId(++id);
				// setButtonShape(btn, 0, BTNCOLOR);
				// btn.setPadding(30,10,30,10);
				// RelativeLayout.LayoutParams btnlp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				// btnlp.addRule(RelativeLayout.BELOW, spaceid);
				// btnlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
				// btn.setLayoutParams(btnlp);
				btn.setText(getStringLang(STLN_BTNDONE));
				btn.setGravity(Gravity.CENTER);
				final String fstopupres = stopupres;
				final String fstransid = stransid;
				btn.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				btn.setTextAppearance(MimopayActivity.this, getTextAppearance());
				btn.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
			    btn.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					mInfoResult = "SUCCESS";
					mAlsResult.clear();
					mAlsResult.add(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
					mAlsResult.add(fstopupres);
					mAlsResult.add(fstransid);
					if(MimopayStuff.sChannel.equals("dpoint")) {
						String spaytype = mCore.alsBtnAction.get(1);
						String sphonenum = mCore.mHttppostPhoneNumber;
						mAlsResult.add(sphonenum);
						//jprintf(String.format("spaytype=%s, sphonenum=%s", spaytype, sphonenum));
						saveLastUsedPhoneNumber(spaytype, sphonenum);
					}
					finish();
				}});

			ell.addView(btn);

		rl.addView(ell);
		// rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));

		ScrollView sv = new ScrollView(this);
		RelativeLayout.LayoutParams svlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  LayoutParams.WRAP_CONTENT);
		svlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		svlp.addRule(RelativeLayout.CENTER_VERTICAL);
		sv.setLayoutParams(svlp);
		sv.setVerticalScrollBarEnabled(false);
		sv.setHorizontalScrollBarEnabled(false);
        sv.addView(rl);
        mainlayout.addView(sv);
        setContentView(mainlayout, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}

	private void setupAirtimeUPointResultUI(String smscontent, String smsnumber, String companyCode)
	{
        RelativeLayout mainlayout = new RelativeLayout(this);
		//mainlayout.setPadding(25,20,25,20);
		GradientDrawable rlshape =  new GradientDrawable();
		rlshape.setCornerRadius(4);
		rlshape.setColor(Color.WHITE);
		rlshape.setStroke(2, WNDSTKCOLOR);
		setBkgndCompat(mainlayout, rlshape);

			// form at center
		    RelativeLayout rl = new RelativeLayout(this);
			RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,  LayoutParams.WRAP_CONTENT);
			rllp.addRule(RelativeLayout.CENTER_IN_PARENT);
			rl.setLayoutParams(rllp);

			// jprintf("setupAirtimeUPointResultUI:smscontent: " + smscontent);
			// jprintf("setupAirtimeUPointResultUI:smsnumber: " + smsnumber);

			int id = 0;

		    // title
		    TextView tvtitle = new TextView(this);
			tvtitle.setId(++id);
			RelativeLayout.LayoutParams tvtitlelp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			_setMargins(tvtitlelp,20,10,0,10);
			tvtitle.setLayoutParams(tvtitlelp);
			tvtitle.setText(getPaymentMethodTitle(MimopayStuff.sChannel));
			tvtitle.setTextAppearance(MimopayActivity.this, getTextAppearanceBig());
			tvtitle.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
		    tvtitle.setTextColor(TEXT_COLOR);
		    rl.addView(tvtitle);

			// ImageView ivlogo = new ImageView(this);
			// ivlogo.setId(++id);
			// RelativeLayout.LayoutParams ivlogolp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			// // ivlogolp.addRule(RelativeLayout.ALIGN_TOP);
			// // ivlogolp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			// int left=0,top=10;
			// String sch = MimopayStuff.sChannel;
			// if(sch.equals("celcom")) {
			// 	left = 15; top = 15;
			// }
			// ivlogolp.setMargins(left,top,0,20);
			// // ivlogolp.setMargins(0,10,0,20);
			// ivlogo.setLayoutParams(ivlogolp);
			// ivlogo.setScaleType(ImageView.ScaleType.CENTER);
			// // Bitmap mlbmp = getRoundedCornerBitmap(mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx)), Color.WHITE);
			// Bitmap mlbmp = mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx));
			// if(mlbmp != null) {
			// 	ivlogo.setImageBitmap(mlbmp);
			// }
			// rl.addView(ivlogo);
			// //rl.addView(inBetweenHorizontalSpace(++spaceid, 5, id));

		    TextView tv = new TextView(this);
			tv.setId(++id);
			RelativeLayout.LayoutParams tvlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			tvlp.addRule(RelativeLayout.BELOW, id-1);
			_setMargins(tvlp,20,0,20,10);
			tv.setLayoutParams(tvlp);

			String st = "";
			String skode = "999999";

			if(smscontent.equals("ERROR")) {
				st = smsnumber;
				mbCannotSMS = true;
			} else {
				int a = smscontent.indexOf(" ", 0);
				skode = smscontent.substring(a+1);
				if(MimopayStuff.sChannel.equals("celcom")) {
					skode = smscontent.replaceAll(" ", "(space)");
					st = "Text " + skode + " " + getStringLang(STLN_SENDTO) + " " + smsnumber + ". " +
						getStringLang(STLN_PURCHASEBY) + " " + mCore.mHttppostPhoneNumber + ".";
				} else {
					// st = getStringLang(STLN_TEXTUP) + skode + " " + getStringLang(STLN_SENDTO) + " " + smsnumber + ". " +
					// 	getStringLang(STLN_PURCHASEBY) + " " + mCore.mHttppostPhoneNumber + ".";
					String as = getStringLang(STLN_TEXTUP);
					st = as.replaceAll("UP", companyCode) + skode + " " + getStringLang(STLN_SENDTO) + " " + smsnumber + ". " +
						getStringLang(STLN_PURCHASEBY) + " " + mCore.mHttppostPhoneNumber + ".";
				}
			}

		    tv.setText(st);
			tv.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
			tv.setTextAppearance(MimopayActivity.this, getTextAppearance());
		    tv.setTextColor(TEXT_COLOR);
		    rl.addView(tv);
			
			EditText et = null;
			RelativeLayout rldummy = null;
			final Button btnok = new Button(this);
			final String fsmscontent = smscontent;
			final String fsmsnumber = smsnumber;
			final String fcompanyCode = companyCode;

			if(!mbCannotSMS) {
				//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));
				st += ("\n\n" + getStringLang(STLN_INORDERSMS));
			    tv.setText(st);
				TextView tvup = new TextView(this);
				tvup.setId(++id);
				RelativeLayout.LayoutParams tvuplp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				tvuplp.addRule(RelativeLayout.BELOW, id-1);
				_setMargins(tvuplp,20,2,0,0);
				tvup.setLayoutParams(tvuplp);
				tvup.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				tvup.setTextAppearance(MimopayActivity.this, getTextAppearance());
			    tvup.setTextColor(TEXT_COLOR);
				String a = "";
				if(MimopayStuff.sChannel.equals("celcom")) {
					int c = smscontent.indexOf(" ", 0);
					skode = smscontent.substring(0,c);
					tvup.setText(skode);
					a = st.replace("\'up\'", skode);
					skode = smscontent.substring(c+1);
					// jprintf(String.format("skode = %s", skode));
				} else {
					tvup.setText(companyCode);
					a = st.replace("\'up\'", companyCode);
					// tvup.setText("UP");
					// a = st.replace("\'up\'", "UP");
				}
			    tv.setText(a);
				rl.addView(tvup);

				et = new EditText(this);
				int ntvupid = id;
				et.setId(++id);
				RelativeLayout.LayoutParams etlp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				etlp.addRule(RelativeLayout.BELOW, id-1);
				etlp.addRule(RelativeLayout.RIGHT_OF, ntvupid);
				etlp.addRule(RelativeLayout.ALIGN_BASELINE, ntvupid);
				_setMargins(etlp,0,0,20,0);
				et.setLayoutParams(etlp);
				if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
					GradientDrawable etshape =  new GradientDrawable();
					etshape.setCornerRadius(9);
					etshape.setColor(Color.TRANSPARENT);
					etshape.setStroke(2, BTNCOLOR);
					setBkgndCompat(et, etshape);
				}
				et.setMinLines(1);
				et.setMaxLines(3);
				et.getBackground().setColorFilter(BTNCOLOR, PorterDuff.Mode.MULTIPLY);  
				et.setFilters(new InputFilter[] { new InputFilter.LengthFilter(skode.length()) });
				et.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_PHONE);
				et.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				et.setTextAppearance(MimopayActivity.this, getTextAppearance());
				final String fskode = skode;
		        et.addTextChangedListener(new EditTextWatcher(et) { @Override public void afterTextChanged(Editable s) {
					int len = fskode.length();
					if(s.length() == len) {
						String t = mEditText.getText().toString();
						if(t.equals(fskode)) {
							btnok.setEnabled(true);
						}
					} else {
						btnok.setEnabled(false);
					}
		        }});
				rl.addView(et);
				//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));
	
				final CheckBox cbduallsim = new CheckBox(this);
				cbduallsim.setId(++id);
				RelativeLayout.LayoutParams cbduallsimlp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				cbduallsimlp.addRule(RelativeLayout.BELOW, id-2);
				//_setMargins(cbduallsimlp,10,20,10,0);
				_setMargins(cbduallsimlp,20,10,20,10);
				cbduallsim.setLayoutParams(cbduallsimlp);
				cbduallsim.setText(getStringLang(STLN_DUALSIM));
				cbduallsim.setGravity(Gravity.LEFT | Gravity.CENTER);
				cbduallsim.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				cbduallsim.setTextAppearance(MimopayActivity.this, getTextAppearance());
			    cbduallsim.setTextColor(TEXT_COLOR);
				/*if(mbCannotSMS) {
					cbduallsim.setVisibility(View.GONE);
				} else {
					cbduallsim.setChecked(true);
				}*/
				rl.addView(cbduallsim);
				//rl.addView(inBetweenHorizontalSpace(++spaceid, 5, id));

				final TextView tv2 = new TextView(this);
				tv2.setId(++id);
				RelativeLayout.LayoutParams tv2lp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				tv2lp.addRule(RelativeLayout.BELOW, id-1);
				tv2lp.addRule(RelativeLayout.ALIGN_LEFT, cbduallsim.getId());
				_setMargins(tv2lp,10,10,10,0);
				tv2.setLayoutParams(tv2lp);
				tv2.setText(getStringLang(STLN_AUTOSEND));
				tv2.setTextColor(TEXT_COLOR);
				rl.addView(tv2);
				//rl.addView(inBetweenHorizontalSpace(++spaceid, 30, id));

				LinearLayout ll = new LinearLayout(this);
				ll.setId(++id);
				RelativeLayout.LayoutParams lllp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				lllp.addRule(RelativeLayout.BELOW, id-1);
				lllp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				_setMargins(lllp,20,30,20,10);
				ll.setLayoutParams(lllp);
					// dummy space
					// rldummy = new RelativeLayout(this);
					// LinearLayout.LayoutParams rldummylp = new LinearLayout.LayoutParams(0,  LayoutParams.WRAP_CONTENT);
					// rldummylp.weight = 1.0f;
					// rldummy.setLayoutParams(rldummylp);
					// ll.addView(rldummy);
				    // button ok
					setButtonShape(btnok, 1, BTNCOLOR);
					_setPadding(btnok, 30, mnButtonOffset, 30, 0);
					LinearLayout.LayoutParams boklp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					// boklp.weight = 0.9f;
					btnok.setLayoutParams(boklp);
					btnok.setText(getStringLang(STLN_BTNSENDSMS));
					btnok.setGravity(Gravity.CENTER);
					btnok.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
					btnok.setTextAppearance(MimopayActivity.this, getTextAppearance());
					btnok.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
					btnok.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
						msSmsContent = fsmscontent;
						msSmsNumber = fsmsnumber;
						if(getUserSmsStatus() == 2) {
							smsHasBeenSent();
							mSendSMSStockAppHandler = null;
							dismissInboxCheck();
						} else {
							if(cbduallsim.isChecked()) {
								if(mSendSMSStockAppHandler == null) {
									Intent sendIntent = new Intent(Intent.ACTION_VIEW);
									if(MimopayStuff.sChannel.equals("upoint")) {
										//sendIntent.putExtra("sms_body", "up " + fsmscontent);
										sendIntent.putExtra("sms_body", fcompanyCode + " " + fsmscontent);
									} else {
										sendIntent.putExtra("sms_body", fsmscontent);
									}
									sendIntent.putExtra("address", fsmsnumber);
									sendIntent.setType("vnd.android-dir/mms-sms");
									startActivityForResult(sendIntent, 0);
									//startActivity(sendIntent);
								} else {
									Toast.makeText(getApplicationContext(), getStringLang(STLN_PLEASEWAIT), Toast.LENGTH_SHORT).show();
								}
							} else {
								if(MimopayStuff.sChannel.equals("upoint")) {
									//mCore.sendSMS("up " + fsmscontent, fsmsnumber);
									mCore.sendSMS(fcompanyCode + " " + fsmscontent, fsmsnumber);
								} else {
									mCore.sendSMS(fsmscontent, fsmsnumber);
								}
							}
						}
					}});
					btnok.setEnabled(false);
					ll.addView(btnok);

					// button cancel
					Button btncancel = new Button(this);
					setButtonShape(btncancel, 2, BTNCOLOR2);
					_setPadding(btncancel, 30, mnButtonOffset, 30, 0);
					LinearLayout.LayoutParams bcanlp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					// bcanlp.weight = 0.9f;
					btncancel.setLayoutParams(bcanlp);
					btncancel.setText(getStringLang(STLN_BTNCANCEL));
					btncancel.setGravity(Gravity.CENTER);
					btncancel.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
					btncancel.setTextAppearance(MimopayActivity.this, getTextAppearance());
					btncancel.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
					btncancel.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
						mInfoResult = "SUCCESS";
						//mAlsResult = new ArrayList<String>();
						mAlsResult.clear();
						String spaytype = mCore.alsBtnAction.get(mCore.mAlsActiveIdx);
						mAlsResult.add(spaytype);
						//mAlsResult.add(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
						mAlsResult.add(fsmscontent);
						mAlsResult.add(fsmsnumber);
						String sphonenum = mCore.mHttppostPhoneNumber;
						mAlsResult.add(sphonenum);
						//mAlsResult.add(mCore.mHttppostPhoneNumber);
						saveLastUsedPhoneNumber(spaytype, sphonenum);
						finish();
					}});
					ll.addView(btncancel);
				rl.addView(ll);
				//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));
				cbduallsim.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					if(cbduallsim.isChecked()) {
						tv2.setText(getStringLang(STLN_SMSSTOCKAPP));
						btnok.setText(getStringLang(STLN_BTNNEXT));
					} else {
						tv2.setText(getStringLang(STLN_AUTOSEND));
						btnok.setText(getStringLang(STLN_BTNSENDSMS));
					}
				}});
			} else {
				//rl.addView(inBetweenHorizontalSpace(++spaceid, 30, id));
				// btnok.setId(++id);
				// setButtonShape(btnok, 0, BTNCOLOR);
				// RelativeLayout.LayoutParams btnoklp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				// btnoklp.addRule(RelativeLayout.BELOW, id-1);
				// btnok.setLayoutParams(btnoklp);
				// btnok.setText(getStringLang(STLN_BTNDONE));
				// btnok.setGravity(Gravity.CENTER);
				// btnok.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				// btnok.setTextAppearance(MimopayActivity.this, getTextAppearance());
				// btnok.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
				// btnok.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
				// 	mInfoResult = "SUCCESS";
				// 	mAlsResult.clear();
				// 	String spaytype = mCore.alsBtnAction.get(mCore.mAlsActiveIdx);
				// 	mAlsResult.add(spaytype);
				// 	mAlsResult.add(fsmscontent);
				// 	mAlsResult.add(fsmsnumber);
				// 	String sphonenum = mCore.mHttppostPhoneNumber;
				// 	mAlsResult.add(sphonenum);
				// 	saveLastUsedPhoneNumber(spaytype, sphonenum);
				// 	finish();
				// }});
				// rl.addView(btnok);
				LinearLayout ell = new LinearLayout(this);
				ell.setId(++id);
				RelativeLayout.LayoutParams elllp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				elllp.addRule(RelativeLayout.BELOW, id-1);
				elllp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				_setMargins(elllp,0,30,20,10);
				ell.setLayoutParams(elllp);
					// dummy space
					// rldummy = new RelativeLayout(this);
					// LinearLayout.LayoutParams rldummylp = new LinearLayout.LayoutParams(0,  LayoutParams.WRAP_CONTENT);
					// rldummylp.weight = 1.0f;
					// rldummy.setLayoutParams(rldummylp);
					// ell.addView(rldummy);
					// add next button
					setButtonShape(btnok, 0, BTNCOLOR);
					_setPadding(btnok, 30, mnButtonOffset, 30, 0);
					LinearLayout.LayoutParams blp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					// blp.weight = 1.0f;
					// blp.gravity = Gravity.CENTER;
					btnok.setLayoutParams(blp);
					btnok.setText(getStringLang(STLN_BTNDONE));
				    btnok.setGravity(Gravity.CENTER);
					btnok.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
					btnok.setTextAppearance(MimopayActivity.this, getTextAppearance());
					btnok.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
				    btnok.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
						mInfoResult = "SUCCESS";
						mAlsResult.clear();
						String spaytype = mCore.alsBtnAction.get(mCore.mAlsActiveIdx);
						mAlsResult.add(spaytype);
						mAlsResult.add(fsmscontent);
						mAlsResult.add(fsmsnumber);
						String sphonenum = mCore.mHttppostPhoneNumber;
						mAlsResult.add(sphonenum);
						saveLastUsedPhoneNumber(spaytype, sphonenum);
						finish();
					}});
				    ell.addView(btnok);
			    rl.addView(ell);
			}

		ScrollView sv = new ScrollView(this);
		RelativeLayout.LayoutParams svlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  LayoutParams.WRAP_CONTENT);
		svlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		svlp.addRule(RelativeLayout.CENTER_VERTICAL);
		sv.setLayoutParams(svlp);
		sv.setVerticalScrollBarEnabled(false);
		sv.setHorizontalScrollBarEnabled(false);
        sv.addView(rl);
        mainlayout.addView(sv);
        setContentView(mainlayout, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}

	//String mXLReferenceId = "";
	String mXLStatusInfo = "";
	String mXLStatusMessage = "";
	
	private void setupAirtimeXLResult1UI(String statinfo, String statmsg)//String srefid)
	{
        RelativeLayout mainlayout = new RelativeLayout(this);
		//mainlayout.setPadding(25,20,25,20);
		GradientDrawable rlshape =  new GradientDrawable();
		rlshape.setCornerRadius(4);
		rlshape.setColor(Color.WHITE);
		rlshape.setStroke(2, WNDSTKCOLOR);
		setBkgndCompat(mainlayout, rlshape);

			// form at center
		    RelativeLayout rl = new RelativeLayout(this);
			RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,  LayoutParams.WRAP_CONTENT);
			rllp.addRule(RelativeLayout.CENTER_IN_PARENT);
			rl.setLayoutParams(rllp);

			int id = 0;
			//int spaceid = 1000;

		    // title
		    TextView tvtitle = new TextView(this);
			tvtitle.setId(++id);
			RelativeLayout.LayoutParams tvtitlelp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			_setMargins(tvtitlelp,20,10,0,10);
			tvtitle.setLayoutParams(tvtitlelp);
			tvtitle.setText(getPaymentMethodTitle(MimopayStuff.sChannel));
			tvtitle.setTextAppearance(MimopayActivity.this, getTextAppearanceBig());
			tvtitle.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
		    tvtitle.setTextColor(TEXT_COLOR);
		    rl.addView(tvtitle);

			// ImageView ivlogo = new ImageView(this);
			// ivlogo.setId(++id);
			// RelativeLayout.LayoutParams ivlogolp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			// // ivlogolp.addRule(RelativeLayout.ALIGN_TOP);
			// // ivlogolp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			// int left=0,top=0;
			// String sch = MimopayStuff.sChannel;
			// if(sch.equals("xl_airtime")) {
			// 	top = 10;
			// } else if(sch.equals("mpoint") || sch.equals("dpoint") || sch.equals("celcom")) {
			// 	left = 15; top = 15;
			// }
			// ivlogolp.setMargins(left,top,0,20);
			// // ivlogolp.setMargins(0,10,0,20);
			// ivlogo.setLayoutParams(ivlogolp);
			// // Bitmap mlbmp = getRoundedCornerBitmap(mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx)), Color.WHITE);
			// Bitmap mlbmp = mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx));
			// if(mlbmp != null) {
			// 	ivlogo.setImageBitmap(mlbmp);
			// }
			// rl.addView(ivlogo);
			// //rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));

		    TextView tv = new TextView(this);
			tv.setId(++id);
			RelativeLayout.LayoutParams tvlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			tvlp.addRule(RelativeLayout.BELOW, id-1);
			_setMargins(tvlp,20,0,20,10);
			tv.setLayoutParams(tvlp);

			if(MimopayStuff.sChannel.equals("mpoint")) {
				statmsg = "Success";
			}

			String st = getStringLang(STLN_TRANSACTION) + " " + statmsg;
			st += ("\n" + getStringLang(STLN_PURCHASEBY) + " " + mCore.mHttppostPhoneNumber);

		    tv.setText(st);
			tv.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
			tv.setTextAppearance(MimopayActivity.this, getTextAppearance());
		    tv.setTextColor(TEXT_COLOR);
		    rl.addView(tv);
			
			if(!mbCannotSMS && statinfo.equals("OK")) {
				//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));
				mXLStatusInfo = statinfo;
				mXLStatusMessage = statmsg;
				
				TextView tvwait = new TextView(this);
				tvwait.setId(++id);
				RelativeLayout.LayoutParams tvwaitlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				tvwaitlp.addRule(RelativeLayout.BELOW, id-1);
				//tvwaitlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
				_setMargins(tvwaitlp,20,0,20,0);
				tvwait.setLayoutParams(tvwaitlp);
				String srsms = "";
				if(MimopayStuff.sChannel.equals("dpoint")) {
					srsms = getStringLang(STLN_BTNNEXTORDONEDPOINT);
				} else {
					srsms = getStringLang(STLN_BTNNEXTORDONE);
				}
				tvwait.setText(srsms);
				tvwait.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				tvwait.setTextAppearance(MimopayActivity.this, getTextAppearance());
			    tvwait.setTextColor(TEXT_COLOR);
				rl.addView(tvwait);
				//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));
				initStockSMSProgDlg(new OnCancelListener() { @Override public void onCancel(DialogInterface dialog) {
					jprintf("checking sms inbox stopped");
					mCore.stopWaitSMS();
				}});

			} else {
				String sx = "";
				if(statinfo.equals("OK")) {
					sx = "\n" + getStringLang(STLN_SMSRECVCONF);
					st += ("\n" + sx);
				    tv.setText(st);
				}
			}

			//rl.addView(inBetweenHorizontalSpace(++spaceid, 20, id));

			LinearLayout ll = new LinearLayout(this);
			ll.setId(++id);
			RelativeLayout.LayoutParams lllp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			lllp.addRule(RelativeLayout.BELOW, id-1);
			lllp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			_setMargins(lllp,0,30,20,10);
			ll.setLayoutParams(lllp);

				// dummy space
				// RelativeLayout rldummy = new RelativeLayout(this);
				// LinearLayout.LayoutParams rldummylp = new LinearLayout.LayoutParams(0,  LayoutParams.WRAP_CONTENT);
				// rldummylp.weight = 1.0f;
				// rldummy.setLayoutParams(rldummylp);
				// ll.addView(rldummy);

				Button btnok = new Button(this);
				_setPadding(btnok, 30, mnButtonOffset, 30, 0);
				LinearLayout.LayoutParams boklp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				// boklp.weight = 1.0f;

				if(!mbCannotSMS && statinfo.equals("OK")) {
					Button btncancel = new Button(this);
					setButtonShape(btncancel, 1, BTNCOLOR2);
					_setPadding(btncancel,30, mnButtonOffset, 30, 0);
					LinearLayout.LayoutParams bcanlp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					// bcanlp.weight = 0.9f;
					btncancel.setLayoutParams(bcanlp);
					btncancel.setText(getStringLang(STLN_BTNNEXT));
					btncancel.setGravity(Gravity.CENTER);
					btncancel.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
					btncancel.setTextAppearance(MimopayActivity.this, getTextAppearance());
					btncancel.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
					btncancel.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
						mCore.waitSMS(60, false);
						initStockSMSProgDlg(new OnCancelListener() { @Override public void onCancel(DialogInterface dialog) {
							jprintf("checking sms inbox stopped");
							mCore.stopWaitSMS();
						}});
					}});
					ll.addView(btncancel);
					setButtonShape(btnok, 2, BTNCOLOR);
					// boklp.weight = 0.9f;
				} else {
					setButtonShape(btnok, 0, BTNCOLOR);
				}

				btnok.setLayoutParams(boklp);
				btnok.setText(getStringLang(STLN_BTNDONE));
				btnok.setGravity(Gravity.CENTER);
				final String fsrefnum = statinfo; //srefnum;
				btnok.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					mInfoResult = "SUCCESS";
					mAlsResult.clear();
					String spaytype = mCore.alsBtnAction.get(mCore.mAlsActiveIdx);
					mAlsResult.add(spaytype);
					mAlsResult.add(fsrefnum);
					String sphonenum = mCore.mHttppostPhoneNumber;
					mAlsResult.add(sphonenum);
					if(MimopayStuff.sChannel.equals("dpoint")) {
						spaytype = mCore.alsBtnAction.get(1);
					}
					saveLastUsedPhoneNumber(spaytype, sphonenum);
					// jprintf(String.format("spaytype=%s, sphonenum=%s", spaytype, sphonenum));
					finish();
				}});
				btnok.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				btnok.setTextAppearance(MimopayActivity.this, getTextAppearance());
				btnok.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
				ll.addView(btnok);

			rl.addView(ll);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));

		ScrollView sv = new ScrollView(this);
		RelativeLayout.LayoutParams svlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  LayoutParams.WRAP_CONTENT);
		svlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		svlp.addRule(RelativeLayout.CENTER_VERTICAL);
		sv.setLayoutParams(svlp);
		sv.setVerticalScrollBarEnabled(false);
		sv.setHorizontalScrollBarEnabled(false);
        sv.addView(rl);
        mainlayout.addView(sv);
        setContentView(mainlayout, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}
	
	private void processXLRepliedSMS(boolean bsuccess, String jarrsms)
	{
		jprintf(String.format("jarrsms=%s, pilihan=%s", jarrsms, mCore.mHttppostCode));
		if(bsuccess) {
			// make sure this is XL replied SMS
			JSONArray jarr = null;
			String senderphno = null;
			String smsmsg = null;
			try {
				jarr = new JSONArray(jarrsms);
				senderphno = new String(jarr.getString(0));
				smsmsg = new String(jarr.getString(2));
			} catch(JSONException e) {}
			if(senderphno == null || smsmsg == null) {
				return;
			}
			// Kamu akan dikenakan tarif Rp10000(sdh PPN). Utk menyelesaikan transaksi, balas sms ini dgn kode ini 4326. CS:0215764122
			//jprintf("processXLRepliedSMS'mHttppostCode: " + mCore.mHttppostCode);
			//D/JimBas  ( 2398): MimopayActivity: processXLRepliedSMS'mHttppostCode: 10000
			if(smsmsg.indexOf(mCore.mHttppostCode) != -1) {
				// parse it to get the 4 digit number
				String ssmsmsg = smsmsg.replaceAll("\\s", "#");
				jprintf("senderphno: " + senderphno + " smsmsg: " + smsmsg + " ssmsmsg: " + ssmsmsg);
				String thecode = "----";
				int a = ssmsmsg.indexOf("kode", 0);
				if(a != -1) {
					int b = ssmsmsg.indexOf(".", a);
					if(b != -1) {
						String sssmsmsg = ssmsmsg.substring(a,b);
						// kode#ini#4326
						jprintf("parsedkode: " + sssmsmsg);
						int c = sssmsmsg.lastIndexOf("#");
						if(c != -1) {
							thecode = sssmsmsg.substring(c+1);
							if(thecode.length() == 4) {
								mCore.stopWaitSMS();
								jprintf("thecode: " + thecode);
								final String fthecode = thecode;
								final String fsenderphno = senderphno;
								runOnUiThread(new Runnable() { public void run() {
									dismissInboxCheck();
									setupAirtimeXLResult2UI(fthecode, fsenderphno);
								}});
								return;
							} else { jprintf("processXLRepliedSMS'error5"); }
						} else { jprintf("processXLRepliedSMS'error4"); }
					} else { jprintf("processXLRepliedSMS'error3"); }
				} else { jprintf("processXLRepliedSMS'error2"); }
			} else { jprintf("processXLRepliedSMS'error1"); }
		} else {
			jprintf("read_status: " + jarrsms);
			if(jarrsms.equals("timeout")) {
				//mbCannotSMS = true;
				//runOnUiThread(new Runnable() { public void run() {
				//	setupAirtimeXLResult1UI(mXLStatusInfo, mXLStatusMessage);
				//}});
				dismissInboxCheck();
			}
		}
	}
	
	private void processIndosatRepliedSMS(boolean bsuccess, String jarrsms)
	{
		jprintf(String.format("processIndosatRepliedSMS:jarrsms=%s, pilihan=%s", jarrsms, mCore.mHttppostCode));
		if(bsuccess) {
			// make sure this is XL replied SMS
			JSONArray jarr = null;
			String senderphno = null;
			String smsmsg = null;
			try {
				jarr = new JSONArray(jarrsms);
				senderphno = new String(jarr.getString(0));
				smsmsg = new String(jarr.getString(2));
			} catch(JSONException e) {}
			if(senderphno == null || smsmsg == null) {
				return;
			}
			//Ketik BELI 201 sebagai balasan pesan ini untuk menyelesaikan pembelian. Rp11.000 akan dikurangi dari pulsa anda. CS:085779160178
			//jprintf("processIndosatRepliedSMS'mHttppostCode: " + mCore.mHttppostCode);
			//D/JimBas  ( 2398): MimopayActivity: processIndosatRepliedSMS'mHttppostCode: 10000
			if(smsmsg.indexOf(mCore.mHttppostCode) != -1) {
				// parse it to get the 4 digit number
				String ssmsmsg = smsmsg.replaceAll("\\s", "#");
				jprintf("senderphno: " + senderphno + " smsmsg: " + smsmsg + " ssmsmsg: " + ssmsmsg);
				String thecode = "----";
				int a = ssmsmsg.indexOf(mCore.mHttppostCode, 0);
				if(a != -1) {
					int b=0,y=a;
					for(int x=0;x<2;x++) {
						b = ssmsmsg.indexOf("#", y);
						y = b+1;
					}
					if(b != -1) {
						String sssmsmsg = ssmsmsg.substring(a,b);
						// Ketik#BELI#201#sebagai
						jprintf("parsedkode: " + sssmsmsg);
						// int c = sssmsmsg.lastIndexOf("#");
						// if(c != -1) {
							// thecode = sssmsmsg.substring(c+1);
							thecode = sssmsmsg.replaceAll("#", " ");
							// if(thecode.length() == 4) {
								mCore.stopWaitSMS();
								jprintf("thecode=>" + thecode + "<=");
								final String fthecode = thecode;
								final String fsenderphno = senderphno;
								runOnUiThread(new Runnable() { public void run() {
									dismissInboxCheck();
									setupAirtimeXLResult2UI(fthecode, fsenderphno);
								}});
								return;
							// } else { jprintf("processIndosatRepliedSMS'error5"); }
						// } else { jprintf("processIndosatRepliedSMS'error4"); }
					} else { jprintf("processIndosatRepliedSMS'error3"); }
				} else { jprintf("processIndosatRepliedSMS'error2"); }
			} else { jprintf("processIndosatRepliedSMS'error1"); }
		} else {
			jprintf("read_status: " + jarrsms);
			if(jarrsms.equals("timeout")) {
				//mbCannotSMS = true;
				//runOnUiThread(new Runnable() { public void run() {
				//	setupAirtimeXLResult1UI(mXLStatusInfo, mXLStatusMessage);
				//}});
				dismissInboxCheck();
			}
		}
	}

	private void processMPointRepliedSMS(boolean bsuccess, String jarrsms)
	{
		if(bsuccess) {
			// make sure this is MPoint replied SMS
			JSONArray jarr = null;
			String senderphno = null;
			String smsmsg = null;
			try {
				jarr = new JSONArray(jarrsms);
				senderphno = new String(jarr.getString(0));
				smsmsg = new String(jarr.getString(2));
			} catch(JSONException e) {}
			if(senderphno == null || smsmsg == null) {
				return;
			}
			//RM0. You have requested to purchase RM2 credits. Please reply
			//274971
			//to 23110.
			//Expiry 3/9/2014 1:58 PM
			// jprintf("processMPointRepliedSMS'mHttppostCode: " + mCore.mHttppostCode);
			//if(smsmsg.indexOf(mCore.mHttppostCode) != -1) {
				// parse it to get the 6 digit number
				String ssmsmsg = smsmsg.replaceAll("\\s", "#");
				jprintf("senderphno: " + senderphno + " smsmsg: " + smsmsg + " ssmsmsg: " + ssmsmsg);
				String thecode = "------";
				int a = ssmsmsg.indexOf("reply", 0);
				if(a != -1) {
					int b = ssmsmsg.indexOf("to", a);
					if(b != -1) {
						// since 'reply#532618#' need to tweak around
						if(b > 0) b--; // skip '#'
						String sssmsmsg = ssmsmsg.substring(a,b);
						jprintf("parsedkode: " + sssmsmsg);
						int c = sssmsmsg.lastIndexOf("#");
						if(c != -1) {
							thecode = sssmsmsg.substring(c+1);
							if(thecode.length() == 6) {
								mCore.stopWaitSMS();
								jprintf("thecode: " + thecode);
								final String fthecode = thecode;
								final String fsenderphno = senderphno;
								runOnUiThread(new Runnable() { public void run() {
									dismissInboxCheck();
									setupAirtimeXLResult2UI(fthecode, fsenderphno);
									//jprintf("should call setupAirtimeMPointResult2UI");
								}});
								return;
							} else { jprintf("processMPointRepliedSMS'error5"); }
						} else { jprintf("processMPointRepliedSMS'error4"); }
					} else { jprintf("processMPointRepliedSMS'error3"); }
				} else { jprintf("processMPointRepliedSMS'error2"); }
			//} else { jprintf("processMPointRepliedSMS'error1"); }
		} else {
			jprintf("read_status: " + jarrsms);
			if(jarrsms.equals("timeout")) {
				dismissInboxCheck();
			}
		}
	}

	//private EditText mDpointSmsCodeEditText = null;
	private void processDPointRepliedSMS(boolean bsuccess, String jarrsms)
	{
		//jprintf(String.format("jarrsms=%s, pilihan=%s", jarrsms, mCore.mHttppostCode));
		if(bsuccess) {
			// make sure this is XL replied SMS
			JSONArray jarr = null;
			String senderphno = null;
			String smsmsg = null;
			try {
				jarr = new JSONArray(jarrsms);
				senderphno = new String(jarr.getString(0));
				smsmsg = new String(jarr.getString(2));
			} catch(JSONException e) {}
			if(senderphno == null || smsmsg == null) {
				return;
			}
			// Your SMS CODE for Mimopay is 123456, you need this to complete they payment
			//jprintf("processDPointRepliedSMS'mHttppostCode: " + mCore.mHttppostCode);
			//if(smsmsg.indexOf(mCore.mHttppostCode) != -1) {
				// parse it to get the  digit number
				String ssmsmsg = smsmsg.replaceAll("\\s", "#");
				jprintf("senderphno: " + senderphno + " smsmsg: " + smsmsg + " ssmsmsg: " + ssmsmsg);
				String thecode = "------";
				int a = ssmsmsg.indexOf("Mimopay", 0);
				if(a != -1) {
					int b = ssmsmsg.indexOf(",", a);
					if(b != -1) {
						String sssmsmsg = ssmsmsg.substring(a,b);
						// Mimopay#is#123456
						jprintf("parsedkode: " + sssmsmsg);
						int c = sssmsmsg.lastIndexOf("#");
						if(c != -1) {
							thecode = sssmsmsg.substring(c+1);
							if(thecode.length() == 6) {
								mCore.stopWaitSMS();
								jprintf("thecode: " + thecode);
								final String fthecode = thecode;
								final String fsenderphno = senderphno;
								runOnUiThread(new Runnable() { public void run() {
									dismissInboxCheck();
									setupAirtimeXLResult2UI(fthecode, fsenderphno);
									//if(mDpointSmsCodeEditText != null) {
									//	mDpointSmsCodeEditText.setText(fthecode);
									//}
								}});
								return;
							} else { jprintf("processDPointRepliedSMS'error5"); }
						} else { jprintf("processDPointRepliedSMS'error4"); }
					} else { jprintf("processDPointRepliedSMS'error3"); }
				} else { jprintf("processDPointRepliedSMS'error2"); }
			//} else { jprintf("processDPointRepliedSMS'error1"); }
		} else {
			jprintf("read_status: " + jarrsms);
			if(jarrsms.equals("timeout")) {
				//mbCannotSMS = true;
				//runOnUiThread(new Runnable() { public void run() {
				//	setupAirtimeXLResult1UI(mXLStatusInfo, mXLStatusMessage);
				//}});
				dismissInboxCheck();
			}
		}
	}

	private void setupAirtimeXLResult2UI(String skode, String senderphno)
	{
        RelativeLayout mainlayout = new RelativeLayout(this);
		//mainlayout.setPadding(25,20,25,20);
		GradientDrawable rlshape =  new GradientDrawable();
		rlshape.setCornerRadius(4);
		rlshape.setColor(Color.WHITE);
		rlshape.setStroke(2, WNDSTKCOLOR);
		setBkgndCompat(mainlayout, rlshape);

			// form at center
		    RelativeLayout rl = new RelativeLayout(this);
			RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,  LayoutParams.WRAP_CONTENT);
			rllp.addRule(RelativeLayout.CENTER_IN_PARENT);
			rl.setLayoutParams(rllp);

			int id = 0;
			//int spaceid = 1000;

		    // title
		    TextView tvtitle = new TextView(this);
			tvtitle.setId(++id);
			RelativeLayout.LayoutParams tvtitlelp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			_setMargins(tvtitlelp,20,10,0,10);
			tvtitle.setLayoutParams(tvtitlelp);
			tvtitle.setText(getPaymentMethodTitle(MimopayStuff.sChannel));
			tvtitle.setTextAppearance(MimopayActivity.this, getTextAppearanceBig());
			tvtitle.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
		    tvtitle.setTextColor(TEXT_COLOR);
		    rl.addView(tvtitle);

			// ImageView ivlogo = new ImageView(this);
			// ivlogo.setId(++id);
			// RelativeLayout.LayoutParams ivlogolp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			// // ivlogolp.addRule(RelativeLayout.ALIGN_TOP);
			// // ivlogolp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			// int left=0,top=0;
			// String sch = MimopayStuff.sChannel;
			// if(sch.equals("xl_airtime")) {
			// 	top = 10;
			// } else if(sch.equals("mpoint") || sch.equals("dpoint") || sch.equals("celcom")) {
			// 	left = 15; top = 15;
			// }
			// ivlogolp.setMargins(left,top,0,20);
			// ivlogolp.setMargins(0,10,0,20);
			// ivlogo.setLayoutParams(ivlogolp);
			// // Bitmap mlbmp = getRoundedCornerBitmap(mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx)), Color.WHITE);
			// Bitmap mlbmp = mCore.getBitmapInternal(mCore.alsLogos.get(mCore.mAlsActiveIdx));
			// if(mlbmp != null) {
			// 	ivlogo.setImageBitmap(mlbmp);
			// }
			// rl.addView(ivlogo);
			// //rl.addView(inBetweenHorizontalSpace(++spaceid, 20, id));

		    TextView tv = new TextView(this);
			tv.setId(++id);
			RelativeLayout.LayoutParams tvlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			tvlp.addRule(RelativeLayout.BELOW, id-1);
			_setMargins(tvlp,20,0,20,0);
			tv.setLayoutParams(tvlp);
			String st =
				getStringLang(STLN_PHONENUM) + " " + mCore.mHttppostPhoneNumber + ".\n" +
				getStringLang(STLN_SHORTCODE) + " " + skode + ".\n\n" + getStringLang(STLN_CNFRMSCODE);

		    tv.setText(st);
			tv.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
			tv.setTextAppearance(MimopayActivity.this, getTextAppearance());
		    tv.setTextColor(TEXT_COLOR);
		    rl.addView(tv);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));
			
			final Button btnok = new Button(this);
			final String fskode = skode;
			final String fsenderphno = senderphno;
							
				final EditText et = new EditText(this);
				et.setId(++id);
				RelativeLayout.LayoutParams etlp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				etlp.addRule(RelativeLayout.BELOW, id-1);
				_setMargins(etlp,20,0,20,0);
				et.setLayoutParams(etlp);
				if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
					GradientDrawable etshape =  new GradientDrawable();
					etshape.setCornerRadius(9);
					etshape.setColor(Color.TRANSPARENT);
					etshape.setStroke(2, BTNCOLOR);
					setBkgndCompat(et, etshape);
				}
				et.setMinLines(1);
				et.setMaxLines(3);
				et.getBackground().setColorFilter(BTNCOLOR, PorterDuff.Mode.MULTIPLY);  
				et.setFilters(new InputFilter[] { new InputFilter.LengthFilter(skode.length()) });
				et.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_PHONE);
				et.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				et.setTextAppearance(MimopayActivity.this, getTextAppearance());
		        et.addTextChangedListener(new EditTextWatcher(et) { @Override public void afterTextChanged(Editable s) {
		        	String stmp = fskode;
					int len = fskode.length();
					if(MimopayStuff.sChannel.equals("indosat_airtime")) {
						int c = fskode.lastIndexOf(" ");
						if(c != -1) {
							stmp = fskode.substring(c+1);
							len = stmp.length();
						}
					}
					if(s.length() == len) {
						String t = mEditText.getText().toString();
						// jprintf(String.format("%s %s",t,fskode));
						if(t.equals(stmp)) {
							btnok.setEnabled(true);
						}
					} else {
						btnok.setEnabled(false);
					}
		        }});

			rl.addView(et);
			int nbhs = MimopayStuff.sChannel.equals("dpoint") ? 30 : 10;
			//rl.addView(inBetweenHorizontalSpace(++spaceid, nbhs, id));

			CheckBox lcbduallsim = null;
			TextView ltv2 = null;
			if(!MimopayStuff.sChannel.equals("dpoint")) {
				lcbduallsim = new CheckBox(this);
				lcbduallsim.setId(++id);
				RelativeLayout.LayoutParams cbduallsimlp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				cbduallsimlp.addRule(RelativeLayout.BELOW, id-1);
				//_setMargins(cbduallsimlp,10,20,10,0);
				_setMargins(cbduallsimlp,20,10,20,10);
				lcbduallsim.setLayoutParams(cbduallsimlp);
				lcbduallsim.setText(getStringLang(STLN_DUALSIM));
				lcbduallsim.setTextAppearance(this, getTextAppearance());
				lcbduallsim.setGravity(Gravity.LEFT | Gravity.CENTER);
				lcbduallsim.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				lcbduallsim.setTextAppearance(MimopayActivity.this, getTextAppearance());
				lcbduallsim.setTextColor(TEXT_COLOR);
				rl.addView(lcbduallsim);
				//rl.addView(inBetweenHorizontalSpace(++spaceid, 5, id));

				ltv2 = new TextView(this);
				ltv2.setId(++id);
				RelativeLayout.LayoutParams tv2lp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				tv2lp.addRule(RelativeLayout.BELOW, id-1);
				tv2lp.addRule(RelativeLayout.ALIGN_LEFT, lcbduallsim.getId());
				_setMargins(tv2lp,10,10,10,0);
				ltv2.setLayoutParams(tv2lp);
				ltv2.setText(getStringLang(STLN_AUTOSEND));
				ltv2.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				ltv2.setTextAppearance(MimopayActivity.this, getTextAppearance());
			    ltv2.setTextColor(TEXT_COLOR);
				rl.addView(ltv2);
				//rl.addView(inBetweenHorizontalSpace(++spaceid, 20, id));
			}
			final CheckBox cbduallsim = lcbduallsim;
			final TextView tv2 = ltv2;

			LinearLayout ll = new LinearLayout(this);
			ll.setId(++id);
			RelativeLayout.LayoutParams lllp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			lllp.addRule(RelativeLayout.BELOW, id-1);
			lllp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			_setMargins(lllp,0,30,20,10);
			ll.setLayoutParams(lllp);
				// dummy space
				// RelativeLayout rldummy = new RelativeLayout(this);
				// LinearLayout.LayoutParams rldummylp = new LinearLayout.LayoutParams(0,  LayoutParams.WRAP_CONTENT);
				// rldummylp.weight = 1.0f;
				// rldummy.setLayoutParams(rldummylp);
				// ll.addView(rldummy);
			    // button ok
				setButtonShape(btnok, 1, BTNCOLOR);
				_setPadding(btnok,30, mnButtonOffset, 30, 0);
				LinearLayout.LayoutParams boklp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				// boklp.weight = 0.8f;
				btnok.setLayoutParams(boklp);
				String sbtnlab = MimopayStuff.sChannel.equals("dpoint") ? getStringLang(STLN_BTNGOTOPUP) : getStringLang(STLN_BTNSENDSMS);
				btnok.setText(sbtnlab);
				// btnok.setGravity(Gravity.CENTER);
				btnok.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				btnok.setTextAppearance(MimopayActivity.this, getTextAppearance());
				btnok.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
				btnok.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					if(MimopayStuff.sChannel.equals("dpoint")) {
						// yg old HttpPostCode -> alsDenomValue
						//jprintf("setupAirtimeXLResult2UI'mHttppostCode: " + mCore.mHttppostCode);
						//mCore.alsDenomValue = new ArrayList<String>(); mCore.alsDenomValue.add(mCore.mHttppostCode);
						//mCore.mOldHttppostCode = mCore.mHttppostCode;
						mCore.mHttppostCode = et.getText().toString();
						mCore.executeBtnAction();
					} else {
						msSmsContent = fskode;
						msSmsNumber = fsenderphno;
						if(getUserSmsStatus() == 2) {
							smsHasBeenSent();
							mSendSMSStockAppHandler = null;
							dismissInboxCheck();
						} else {
							if(cbduallsim != null && cbduallsim.isChecked()) {
								if(mSendSMSStockAppHandler == null) {
									Intent sendIntent = new Intent(Intent.ACTION_VIEW);
									sendIntent.putExtra("sms_body", fskode);
									sendIntent.putExtra("address", fsenderphno);
									sendIntent.setType("vnd.android-dir/mms-sms");
									startActivityForResult(sendIntent, 0);
								} else {
									Toast.makeText(getApplicationContext(), getStringLang(STLN_PLEASEWAIT), Toast.LENGTH_SHORT).show();
								}
							} else {
								mCore.sendSMS(fskode, fsenderphno);
							}
						}
					}
				}});
				btnok.setEnabled(false);
				ll.addView(btnok);
				// button cancel
				Button btncancel = new Button(this);
				setButtonShape(btncancel, 2, BTNCOLOR2);
				_setPadding(btncancel,30, mnButtonOffset, 30, 0);
				LinearLayout.LayoutParams bcanlp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				// bcanlp.weight = 0.8f;
				btncancel.setLayoutParams(bcanlp);
				btncancel.setText(getStringLang(STLN_BTNCANCEL));
				btncancel.setGravity(Gravity.CENTER);
				btncancel.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				btncancel.setTextAppearance(MimopayActivity.this, getTextAppearance());
				btncancel.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
				btncancel.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					mInfoResult = "SUCCESS";
					//mAlsResult = new ArrayList<String>();
					mAlsResult.clear();
					String spaytype = mCore.alsBtnAction.get(mCore.mAlsActiveIdx);
					mAlsResult.add(spaytype);
					//mAlsResult.add(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
					mAlsResult.add(mXLStatusInfo);	//mXLReferenceId);
					mAlsResult.add(fskode);
					mAlsResult.add(fsenderphno);
					String sphonenum = mCore.mHttppostPhoneNumber;
					mAlsResult.add(sphonenum);
					//mAlsResult.add(mCore.mHttppostPhoneNumber);
					saveLastUsedPhoneNumber(spaytype, sphonenum);
					finish();
				}});
				ll.addView(btncancel);
			rl.addView(ll);
			//rl.addView(inBetweenHorizontalSpace(++spaceid, 10, id));

			if(cbduallsim != null) {
				cbduallsim.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					if(cbduallsim.isChecked()) {
						tv2.setText(getStringLang(STLN_SMSSTOCKAPP));
						btnok.setText(getStringLang(STLN_BTNNEXT));
					} else {
						tv2.setText(getStringLang(STLN_AUTOSEND));
						btnok.setText(getStringLang(STLN_BTNSENDSMS));
					}
				}});
			}

		ScrollView sv = new ScrollView(this);
		RelativeLayout.LayoutParams svlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  LayoutParams.WRAP_CONTENT);
		svlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		svlp.addRule(RelativeLayout.CENTER_VERTICAL);
		sv.setLayoutParams(svlp);
		sv.setVerticalScrollBarEnabled(false);
		sv.setHorizontalScrollBarEnabled(false);
        sv.addView(rl);
        mainlayout.addView(sv);
        setContentView(mainlayout, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}

	private String getMyPhoneNumber()
	{
		TelephonyManager telpmgr = null;
		telpmgr = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE); 
		String s = telpmgr.getLine1Number();
		jprintf("getMyPhoneNumber: " + s);
		return s;
	}

	private boolean isActivityFinishing()
	{
		return isFinishing();
	}

	private void setupAlertDialog(String smsg, final String sfirstbtn, final String ssecondbtn, final MimopayInterface mi)
	{
        RelativeLayout mainlayout = new RelativeLayout(this);
		//mainlayout.setPadding(25,20,25,20);
		GradientDrawable rlshape =  new GradientDrawable();
		rlshape.setCornerRadius(4);
		rlshape.setColor(Color.WHITE);
		rlshape.setStroke(3, Color.DKGRAY);
		setBkgndCompat(mainlayout, rlshape);

			// form at center
		    RelativeLayout rl = new RelativeLayout(this);
			RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			rllp.addRule(RelativeLayout.CENTER_IN_PARENT);
			rl.setLayoutParams(rllp);

			int id = 0;

		    // title
		    TextView tvtitle = new TextView(this);
			tvtitle.setId(++id);
			RelativeLayout.LayoutParams tvtitlelp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			_setMargins(tvtitlelp,20,10,0,10);
			tvtitle.setLayoutParams(tvtitlelp);
			tvtitle.setText(getPaymentMethodTitle(MimopayStuff.sChannel));
			tvtitle.setTextAppearance(MimopayActivity.this, getTextAppearanceBig());
			tvtitle.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
		    tvtitle.setTextColor(TEXT_COLOR);
		    rl.addView(tvtitle);

			// ImageView ivlogo = new ImageView(this);
			// ivlogo.setId(++id);
			// RelativeLayout.LayoutParams ivlogolp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			// ivlogolp.setMargins(15,15,0,15);
			// ivlogo.setLayoutParams(ivlogolp);
			// Bitmap mlbmp = mCore.getBitmapInternal(mCore.mMimopayLogoUrl);
			// if(mlbmp != null) {
			// 	ivlogo.setImageBitmap(mlbmp);
			// }
			// rl.addView(ivlogo);

		    TextView tv = new TextView(this);
			tv.setId(++id);
			RelativeLayout.LayoutParams tvlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			tvlp.addRule(RelativeLayout.BELOW, id-1);
			_setMargins(tvlp,20,0,20,0);
			tv.setLayoutParams(tvlp);
			String st = smsg;
			// if(mCore.mnRespondStatusCode == -1) {
			// 	st += (". " + mCore.msExceptionMessage);
			// }
		    tv.setText(st);
			tv.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
			tv.setTextAppearance(MimopayActivity.this, getTextAppearance());
		    tv.setTextColor(TEXT_COLOR);
		    rl.addView(tv);

			LinearLayout ll = new LinearLayout(this);
			ll.setId(++id);
			RelativeLayout.LayoutParams lllp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			lllp.addRule(RelativeLayout.BELOW, id-1);
			lllp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			_setMargins(lllp,0,10,20,10);
			ll.setLayoutParams(lllp);
				// dummy space
				// RelativeLayout rldummy = new RelativeLayout(this);
				// LinearLayout.LayoutParams rldummylp = new LinearLayout.LayoutParams(0,  LayoutParams.WRAP_CONTENT);
				// rldummylp.weight = 1.0f;
				// rldummy.setLayoutParams(rldummylp);
				// ll.addView(rldummy);
			    // button ok
				Button btnok = new Button(this);
				int it = ssecondbtn != null ? 1 : 0;
				setButtonShape(btnok, it, BTNCOLOR);
				_setPadding(btnok,30, mnButtonOffset, 30, 0);
				LinearLayout.LayoutParams boklp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				// boklp.weight = 1.0f;
				btnok.setLayoutParams(boklp);
				btnok.setText(sfirstbtn);
				btnok.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				btnok.setTextAppearance(MimopayActivity.this, getTextAppearance());
				btnok.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
				btnok.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
					if(mi != null) {
						mi.onReturn(sfirstbtn, null);
					}
				}});
				ll.addView(btnok);
				if(ssecondbtn != null) {
					// buttton cancel
					Button btncancel = new Button(this);
					setButtonShape(btncancel, 2, 0xFF1565C0);
					_setPadding(btncancel,30, mnButtonOffset, 30, 0);
					LinearLayout.LayoutParams bcanlp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					// bcanlp.weight = 1.0f;
					btncancel.setLayoutParams(bcanlp);
					btncancel.setText(ssecondbtn);
					btncancel.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
					btncancel.setTextAppearance(MimopayActivity.this, getTextAppearance());
					btncancel.setTextColor(MimopayActivity.this.getResources().getColor(android.R.color.background_light));
					btncancel.setOnClickListener(new OnClickListener() { @Override public void onClick(View v) {
						if(mi != null) {
							mi.onReturn(ssecondbtn, null);
						}
					}});
					ll.addView(btncancel);
				}
			rl.addView(ll);

        mainlayout.addView(rl);
        setContentView(mainlayout, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}

	private void setupProgressDialog(String smsg)
	{
        RelativeLayout mainlayout = new RelativeLayout(this);
		_setPadding(mainlayout,25,20,25,20);
		GradientDrawable rlshape =  new GradientDrawable();
		rlshape.setCornerRadius(4);
		rlshape.setColor(Color.WHITE);
		rlshape.setStroke(2, WNDSTKCOLOR);
		setBkgndCompat(mainlayout, rlshape);

			ProgressBar pb = new ProgressBar(this, null, android.R.attr.progressBarStyle);
			RelativeLayout.LayoutParams pblp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			_setMargins(pblp,0,10,0,10);
			pblp.addRule(RelativeLayout.CENTER_IN_PARENT);
			pb.setLayoutParams(pblp);
			pb.setIndeterminate(true);
			pb.getIndeterminateDrawable().setColorFilter(BTNCOLOR, Mode.MULTIPLY);

        mainlayout.addView(pb);

        // for measurement
        if(mnTextAppearSmaller == 0) {
			CTextView tvwait = new CTextView(this);
			RelativeLayout.LayoutParams tvwaitlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			_setMargins(tvwaitlp,5,5,0,0);
			tvwait.setLayoutParams(tvwaitlp);
			tvwait.setText("text");
			tvwait.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
			tvwait.setTextAppearance(MimopayActivity.this, getTextAppearance());
			tvwait.setTextColor(Color.WHITE);
			mainlayout.addView(tvwait);
		}

		if(mnButtonOffset == 0) {
			Button btn = new Button(this);
			setButtonShape(btn, 1, Color.WHITE);
			RelativeLayout.LayoutParams blp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			blp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			_setMargins(blp,0,5,5,0);
			btn.setLayoutParams(blp);
			btn.setText("test");
			btn.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
			btn.setTextAppearance(MimopayActivity.this, getTextAppearance());
			btn.setTextColor(Color.WHITE);
			mainlayout.addView(btn);
		}

        setContentView(mainlayout, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}
	
	public class MimopayActivityCore extends MimopayCore
	{
		@Override public void onError(final String serr)
		{
			if(isActivityFinishing()) {
				jprintf("Error Notification Cancelled");
				return;
			}

			String smsg = "UserCanceled";

			if(serr.equals("MerchantLoadUrlNull")) {
				smsg = getStringLang(STLN_MERCURLNULL);
			} else if(serr.equals("UnsupportedPaymentMethod")) {
				smsg = getStringLang(STLN_PYMETNOTSUPP);
			} else if(serr.equals("UnspecifiedChannelRequest")) {
				smsg = getStringLang(STLN_UNAVAILPYCH);
			} else if(serr.equals("ErrorHTTP404NotFound")) {
				smsg = getStringLang(STLN_ERROR404);
			} else if(serr.equals("ErrorUnderMaintenance")) {
				smsg = getStringLang(STLN_UNDMAINTNC);
			} else if(serr.equals("ErrorInvalidPhoneNumber")) {
				smsg = getStringLang(STLN_INVALPHNUM);
			} if(serr.equals("ErrorConnectingToMimopayServer")) {
				smsg = getStringLang(STLN_ERRORCONN);
			} else if(serr.equals("ErrorValidatingVoucherCode")) {
				smsg = getStringLang(STLN_ERRORVALID);
			} else if(serr.equals("ErrorInternetConnectionProblem")) {
				smsg = getStringLang(STLN_16DGTCOPIED);
			}

			final String fsmsg = smsg;
			final String fsfirst = getStringLang(STLN_BTNDONE);
			//final String fssecond = ssecond;
			final MimopayInterface flmi = new MimopayInterface() { public void onReturn(String info, ArrayList<String> params) {
				mInfoResult = "ERROR";
				mAlsResult.clear();
				mAlsResult.add(serr);
				mAlsResult.add(Integer.toString(mnRespondStatusCode));
				if(mnRespondStatusCode == -1) {
					mAlsResult.add(msExceptionMessage);
				}
				finish();
			}};

			runOnUiThread(new Runnable() { public void run() {
				setupAlertDialog(fsmsg, fsfirst, null, flmi);
			}});
		}

		@Override public void onProgress(boolean start, String cmd)
		{
			String msg = "";
			if(start) {
				if(cmd.equals("validating")) {
					msg = getStringLang(STLN_VALIDATING);
				} else if(cmd.equals("connecting")) {
					msg = getStringLang(STLN_CONNECTING);
				} else if(cmd.equals("sendsms")) {
					msg = getStringLang(STLN_SENDINGSMS);
				} else if(cmd.equals("waitsms")) {
					// ini sudah tidak di pakai lagi
					// msg = "Please wait while reading SMS...";
					// msg = "Mohon menunggu, sedang membaca SMS balasan...";
					msg = null;
				}

				if(msg != null) setupProgressDialog(msg);
				return;
			}
		}

		private boolean isChannelReady()
		{
			String s = alsChannelStatus.get(mCore.mAlsActiveIdx);
			if(s.equals("1")) return true;
			return false;
		}

		@Override public void onMerchantPaymentMethodRetrieved()
		{
			runOnUiThread(new Runnable() { public void run() {
				boolean b;
				String s;
				if(MimopayStuff.sPaymentMethod.equals("Topup")) {
					s = MimopayStuff.sChannel;
					if(s.equals("smartfren") || s.equals("sevelin") || s.equals("upoint_hrn")) {
						b = mCore.setChannelActiveIndex(s);
						if(!b) { onError("UnspecifiedChannelRequest"); return; }
						if(!isChannelReady()) { onError("ErrorUnderMaintenance"); return; }
						setupTopUpVoucherUI();
					} else {
						jprintf("Unsupported Payment Method");
						onError("UnsupportedPaymentMethod");
					}
				} else if(MimopayStuff.sPaymentMethod.equals("Airtime")) {
					if(MimopayStuff.sChannel.equals("upoint")) {
						mCore.mAlsActiveIdx = 0;
						if(!isChannelReady()) { onError("ErrorUnderMaintenance"); return; }
						if(mPhoneNumber != null) {
							mCore.mHttppostCode = mAirtimeValue;
							mCore.mHttppostPhoneNumber = mPhoneNumber;
							mCore.executeBtnAction();
						} else if(MimopayStuff.sAmount.equals("0") == false) {
							mCore.mHttppostCode = MimopayStuff.sAmount;
							s = getMyPhoneNumber();
							if(s == null) mbCannotSMS = true;
							setupUserPhoneNumberUI();
						} else {
							setupAirtimeDenomUI();
						}
					} else if(MimopayStuff.sChannel.equals("mpoint")) {
						b = mCore.setChannelActiveIndex("mpoint_airtime");
						if(!b) { onError("UnspecifiedChannelRequest"); return; }
						if(!isChannelReady()) { onError("ErrorUnderMaintenance"); return; }
						if(mPhoneNumber != null) {
							mCore.mHttppostCode = mAirtimeValue;
							mCore.mHttppostPhoneNumber = mPhoneNumber;
							mbCannotSMS = false;
							mCore.waitSMS(60);	// 60 seconds
							mCore.executeBtnAction();
						} else if(MimopayStuff.sAmount.equals("0") == false) {
							mCore.mHttppostCode = MimopayStuff.sAmount;
							mCore.mAlsActiveIdx = 0;
							s = getMyPhoneNumber();
							if(s == null) mbCannotSMS = true;
							setupUserPhoneNumberUI();
						} else {
							setupAirtimeDenomUI();
						}
					} else if(MimopayStuff.sChannel.equals("dpoint")) {
						b = mCore.setChannelActiveIndex("dpoint_airtime");
						if(!b) { onError("UnspecifiedChannelRequest"); return; }
						if(!isChannelReady()) { onError("ErrorUnderMaintenance"); return; }
						if(mPhoneNumber != null) {
							mCore.mHttppostCode = mAirtimeValue;
							mCore.mHttppostPhoneNumber = mPhoneNumber;
							if(b) mbCannotSMS = false;
						} else if(MimopayStuff.sAmount.equals("0") == false) {
							mCore.mAlsActiveIdx = 0;
							mCore.mHttppostCode = MimopayStuff.sAmount;
							s = getMyPhoneNumber();
							if(s == null) mbCannotSMS = true;
							setupUserPhoneNumberUI();
						} else {
							setupAirtimeDenomUI();
						}
					} else if(MimopayStuff.sChannel.equals("celcom")) {
						mCore.mAlsActiveIdx = 0;
						if(!isChannelReady()) { onError("ErrorUnderMaintenance"); return; }
						if(mPhoneNumber != null) {
							mCore.mHttppostCode = mAirtimeValue;
							mCore.mHttppostPhoneNumber = mPhoneNumber;
							mCore.executeBtnAction();
						} else if(MimopayStuff.sAmount.equals("0") == false) {
							mCore.mHttppostCode = MimopayStuff.sAmount;
							s = getMyPhoneNumber();
							if(s == null) mbCannotSMS = true;
							setupUserPhoneNumberUI();
						} else {
							setupAirtimeDenomUI();
						}
					} else {
						jprintf("Unsupported Channel of Payment Method: " + MimopayStuff.sPaymentMethod);
					}
				} else if(MimopayStuff.sPaymentMethod.equals("XL")) {
					if(MimopayStuff.sChannel.equals("xl_airtime")) {
						b = mCore.setChannelActiveIndex("xl_airtime");
						if(!b) { onError("UnspecifiedChannelRequest"); return; }
						if(!isChannelReady()) { onError("ErrorUnderMaintenance"); return; }
						if(mPhoneNumber != null) {
							mCore.mHttppostCode = mAirtimeValue;
							mCore.mHttppostPhoneNumber = mPhoneNumber;
							mbCannotSMS = false;
							mCore.waitSMS(60);	// 30 seconds
							mCore.executeBtnAction();
						} else if(MimopayStuff.sAmount.equals("0") == false) {
							mCore.mHttppostCode = MimopayStuff.sAmount;
							mCore.mAlsActiveIdx = 0;
							s = getMyPhoneNumber();
							if(s == null) mbCannotSMS = true;
							setupUserPhoneNumberUI();
						} else {
							setupAirtimeDenomUI();
						}
					} else if(MimopayStuff.sChannel.equals("xl_hrn")) {
						b = mCore.setChannelActiveIndex("xl_hrn");
						if(!b) { onError("UnspecifiedChannelRequest"); return; }
						if(!isChannelReady()) { onError("ErrorUnderMaintenance"); return; }
						setupTopUpVoucherUI();
					} else {
						jprintf("Unsupported Payment Method");
						onError("UnsupportedPaymentMethod");
					}
				} else if(MimopayStuff.sPaymentMethod.equals("ATM")) {
					if(MimopayStuff.sAmount.equals("0")) {
						mCore.mAlsActiveIdx = 0;
						if(!isChannelReady()) { onError("ErrorUnderMaintenance"); return; }
						setupDenomUI(); 
					} else {
						if(MimopayStuff.sChannel.equals("atm_bca")) {
							b = mCore.setChannelActiveIndex("atm_bca");
							if(!b) { onError("UnspecifiedChannelRequest"); return; }
							if(!isChannelReady()) { onError("ErrorUnderMaintenance"); return; }
							setupAtmChoosenUI();
						} else if(MimopayStuff.sChannel.equals("atm_bersama")) {
							b = mCore.setChannelActiveIndex("atm_bersama");
							if(!b) { onError("UnspecifiedChannelRequest"); return; }
							if(!isChannelReady()) { onError("ErrorUnderMaintenance"); return; }
							setupAtmChoosenUI();
						} else {
							jprintf("Unsupported Payment Method");
							onError("UnsupportedPaymentMethod");
						}
					}
				} else if(MimopayStuff.sPaymentMethod.equals("VnTelco")) {
					b = mCore.setChannelActiveIndex("topup_vn");
					if(!b) { onError("UnspecifiedChannelRequest"); return; }
					if(!isChannelReady()) { onError("ErrorUnderMaintenance"); return; }
					setupVnTelcoUI();
				} else if(MimopayStuff.sPaymentMethod.equals("indosat")) {
					if(MimopayStuff.sChannel.equals("indosat_airtime")) {
						b = mCore.setChannelActiveIndex("indosat_airtime");
						if(!b) { onError("UnspecifiedChannelRequest"); return; }
						if(!isChannelReady()) { onError("ErrorUnderMaintenance"); return; }
						if(mPhoneNumber != null) {
							mCore.mHttppostCode = mAirtimeValue;
							mCore.mHttppostPhoneNumber = mPhoneNumber;
							mbCannotSMS = false;
							mCore.waitSMS(60);	// 30 seconds
							mCore.executeBtnAction();
						} else if(MimopayStuff.sAmount.equals("0") == false) {
							mCore.mHttppostCode = MimopayStuff.sAmount;
							mCore.mAlsActiveIdx = 0;
							s = getMyPhoneNumber();
							if(s == null) mbCannotSMS = true;
							setupUserPhoneNumberUI();
						} else {
							setupAirtimeDenomUI();
						}
					} else {
						jprintf("Unsupported Payment Method");
						onError("UnsupportedPaymentMethod");
					}
				} else {
					jprintf("Unsupported Payment Method");
					onError("UnsupportedPaymentMethod");
				}
			}});
		}

		@Override public void onResultUI(String channel, ArrayList<String> params)
		{
			nBtnActionCnt++;
			if(channel.equals("smartfren") || channel.equals("sevelin") || channel.equals("xl_hrn") || channel.equals("dpoint_airtime_charge") || channel.equals("upoint_hrn") || channel.equals("topup_vn")) {
				final String ftopupResult = params.get(0);
				final String ftransId = params.get(1);
				//final String fmsg = params.get(2);
				runOnUiThread(new Runnable() { public void run() {
					setupTopUpResultUI(ftopupResult, ftransId);	//, fmsg);
				}});
			} else if(channel.indexOf("atm_") != -1) {
				final String fcompanyCode = params.get(0);
				final String ftotalBill = params.get(1);
				final String ftransId = params.get(2);
				runOnUiThread(new Runnable() { public void run() {
					setupDenomAtmResultUI(fcompanyCode, ftotalBill, ftransId);
				}});
			} else if(channel.equals("upoint_airtime")) {
				final String fsmsContent = params.get(0);
				final String fsmsNumber = params.get(1);
				final String fcompanyCode = params.get(2);
				runOnUiThread(new Runnable() { public void run() {
					setupAirtimeUPointResultUI(fsmsContent, fsmsNumber, fcompanyCode);
				}});
			} else if(channel.equals("xl_airtime")) {
				final String statinfo = params.get(0);
				final String statmsg = params.get(1);
				//jprintf(String.format("statinfo=%s, statmsg=%s", statinfo, statmsg));
				runOnUiThread(new Runnable() { public void run() {
					setupAirtimeXLResult1UI(statinfo, statmsg);
				}});
			} else if(channel.equals("mpoint_airtime")) {
				final String statinfo = params.get(0);
				final String statmsg = params.get(1);
				//jprintf("returned_mpoint_airtime: " + refid);
				runOnUiThread(new Runnable() { public void run() {
					setupAirtimeXLResult1UI(statinfo, statmsg);
				}});
			} else if(channel.equals("indosat_airtime")) {
				final String statinfo = params.get(0);
				final String statmsg = params.get(1);
				//jprintf(String.format("statinfo=%s, statmsg=%s", statinfo, statmsg));
				runOnUiThread(new Runnable() { public void run() {
					setupAirtimeXLResult1UI(statinfo, statmsg);
				}});
			} else if(channel.equals("dpoint_airtime")) {
				boolean b = mCore.setChannelActiveIndex("dpoint_airtime");
				if(b) {
					alsUrls = new ArrayList<String>();
					alsUrls.add(params.get(0));		//postUrl
					String oldpaytype = alsBtnAction.get(mAlsActiveIdx);
					alsBtnAction = new ArrayList<String>();
					alsBtnAction.add(params.get(1));		//btnAction
					alsBtnAction.add(oldpaytype);
					alsTextEditName = new ArrayList<String>();
					alsTextEditName.add(params.get(2));		//code
					alsDenomKey = new ArrayList<String>();
					alsDenomKey.add(params.get(3));		//mdn
					alsDenomPost = new ArrayList<String>();
					alsDenomPost.add(params.get(4));		//denom
					alsDenomValue = new ArrayList<String>();
					alsDenomValue.add(params.get(5));		// denom value
					printAls();
					runOnUiThread(new Runnable() { public void run() {
						//setupTopUpVoucherUI();
						setupAirtimeXLResult1UI("OK", "Success");
					}});
				} else {
					onError("UnspecifiedChannelRequest");
				}
			} else if(channel.equals("celcom_airtime")) {
				for(int i=0;i<params.size();i++) {
					jprintf(String.format("get[%d]: %s",i,params.get(i)));
				}
				final String fsmsContent = params.get(0);
				final String fsmsNumber = params.get(1);
				runOnUiThread(new Runnable() { public void run() {
					setupAirtimeUPointResultUI(fsmsContent, fsmsNumber, "");	// celcom no need companyCode
				}});
			}
		}
		
		@Override public void onSmsCompleted(boolean success, ArrayList<String> alsSms)
		{
			String sop = alsSms.get(0);

			if(sop.equals("sent")) {
				//if(isActivityFinishing()) {
				//	jprintf("Error Notification Cancelled");
				//	return;
				//}
				if(success) {
					String stitle = "";
					final String smsmessage = alsSms.get(1);
					final String dstphonenum = alsSms.get(2);
					if(MimopayStuff.sChannel.equals("upoint")) {
						stitle = "UPoint";
					} else if(MimopayStuff.sChannel.equals("xl_airtime")) {
						stitle = "XL";
					} else if(MimopayStuff.sChannel.equals("mpoint")) {
						stitle = "MPoint";
					} else if(MimopayStuff.sChannel.equals("dpoint")) {
						stitle = "DPoint";
					} else if(MimopayStuff.sChannel.equals("celcom")) {
						stitle = "Celcom";
					}
					final String fstitle = stitle;
					if(!isActivityFinishing()) {
						runOnUiThread(new Runnable() { public void run() {
							String smessage="",stitle="";
							smessage = getStringLang(STLN_WIILRECVSMS);//STLN_SENDINGSMS);
							stitle = getStringLang(STLN_BTNDONE);
							setupAlertDialog(smessage, stitle,
							  null, new MimopayInterface() { public void onReturn(String info, ArrayList<String> params) {
								mInfoResult = "SUCCESS";
								//mAlsResult = new ArrayList<String>();
								mAlsResult.clear();
								String spaytype = mCore.alsBtnAction.get(mCore.mAlsActiveIdx);
								mAlsResult.add(spaytype);
								//mAlsResult.add(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
								mAlsResult.add(smsmessage);
								mAlsResult.add(dstphonenum);
								if(MimopayStuff.sChannel.equals("xl_airtime")) {
									mAlsResult.add(mXLStatusInfo);	//mXLReferenceId);
								}
								String sphonenum = mCore.mHttppostPhoneNumber;
								mAlsResult.add(sphonenum);
								//mAlsResult.add(mCore.mHttppostPhoneNumber);
								saveLastUsedPhoneNumber(spaytype, sphonenum);
								finish();
							}});
						}});
					} else {
					}
					return;
				}
				if(!isActivityFinishing()) {
					Toast.makeText(getApplicationContext(), getStringLang(STLN_FAILSMS), Toast.LENGTH_LONG).show();
				}
			} else if(sop.equals("read")) {
				if(MimopayStuff.sChannel.equals("xl_airtime")) {
					processXLRepliedSMS(success, alsSms.get(1));
				} else if(MimopayStuff.sChannel.equals("indosat_airtime")) {
					processIndosatRepliedSMS(success, alsSms.get(1));
				} else if(MimopayStuff.sChannel.equals("mpoint")) {
					processMPointRepliedSMS(success, alsSms.get(1));
				} else if(MimopayStuff.sChannel.equals("dpoint")) {
					processDPointRepliedSMS(success, alsSms.get(1));
				}
			}
		}
	}

	private void dismissInboxCheck()
	{
		if(mStockSMSDlg == null) return;
		try { 
			mStockSMSDlg.dismiss();
			mStockSMSDlg = null;
		} catch (IllegalArgumentException e) {}
	}

	@SuppressWarnings("deprecation")
	private int getUserSmsStatus()
	{
		Uri mSmsinboxQueryUri = Uri.parse("content://sms");
		Cursor cursor = getContentResolver().query(
			mSmsinboxQueryUri, new String[] { "_id", "thread_id", "address", "person", "date", "body", "type" }, null, null, null);
		startManagingCursor(cursor);
		String[] columns = new String[] { "address", "person", "date", "body", "type" };
		if(cursor.getCount() > 0) {
			String count = Integer.toString(cursor.getCount());
			while(cursor.moveToNext()) {
				String address = cursor.getString(cursor.getColumnIndex(columns[0]));
				String msg = cursor.getString(cursor.getColumnIndex(columns[3]));
				String lowmsg = msg.toLowerCase();
				String type = cursor.getString(cursor.getColumnIndex(columns[4]));
				//jprintf(String.format("address:%s msg:%s lowmsg:%s, type:%s", address, msg, lowmsg, type));
				if((address != null && address.equals(msSmsNumber)) || (msg != null && msg.equals(msSmsContent))) {
					//jprintf("found: " + lowmsg + " " + type);
					if(lowmsg.indexOf(msSmsContent) != -1) {
						return Integer.parseInt(type);
					} //else { jprintf("getUserSmsStatus'stat3"); }
				} //else { jprintf("getUserSmsStatus'stat2"); }
			}
		} else { jprintf("getUserSmsStatus'stat1"); }
		return 0;
	}

    @Override protected void onActivityResult (int requestCode, int resultCode, Intent data)
	{
        if(resultCode == Activity.RESULT_OK || resultCode == Activity.RESULT_CANCELED) {
			mnReadStockAppSMS = 0;
			mSendSMSStockAppHandler = new Handler();
			mSendSMSStockAppHandler.postDelayed(mSendSMSStockAppRunnable, 100);
			initStockSMSProgDlg(new OnCancelListener() { @Override public void onCancel(DialogInterface dialog) {
				mSendSMSStockAppHandler = null;
				jprintf("checking sms inbox stopped");
			}});
		    mStockSMSDlg.show();
        }
    }

	private void smsHasBeenSent()
	{
		String smessage="",stitle="";
		smessage = getStringLang(STLN_SMSSENT);
		stitle = getStringLang(STLN_BTNDONE);
		setupAlertDialog(smessage, stitle,
		  null, new MimopayInterface() { public void onReturn(String info, ArrayList<String> params) {
			mInfoResult = "SUCCESS";
			//mAlsResult = new ArrayList<String>();
			mAlsResult.clear();
			String spaytype = mCore.alsBtnAction.get(mCore.mAlsActiveIdx);
			mAlsResult.add(spaytype);
			//mAlsResult.add(mCore.alsBtnAction.get(mCore.mAlsActiveIdx));
			mAlsResult.add(msSmsContent);
			mAlsResult.add(msSmsNumber);
			String sphonenum = mCore.mHttppostPhoneNumber;
			mAlsResult.add(sphonenum);
			//mAlsResult.add(mCore.mHttppostPhoneNumber);
			saveLastUsedPhoneNumber(spaytype, sphonenum);
			finish();
		}});
	}

	private int mnReadStockAppSMS = 0;
	private Dialog mStockSMSDlg = null;
	private Handler mSendSMSStockAppHandler = null;
	private Runnable mSendSMSStockAppRunnable = new Runnable() { @Override public void run()
	{
		if(mSendSMSStockAppHandler == null) return;
		if(mnReadStockAppSMS < 30) {
			jprintf("checking sms inbox: " + Integer.toString(mnReadStockAppSMS));
			mnReadStockAppSMS++;
			if(getUserSmsStatus() == 2) {	// 2 means sent folder
				smsHasBeenSent();
				mSendSMSStockAppHandler = null;
				dismissInboxCheck();
			} else {
				mSendSMSStockAppHandler.postDelayed(mSendSMSStockAppRunnable, 1000);
			}
		} else {
			mSendSMSStockAppHandler = null;
			dismissInboxCheck();
			Toast.makeText(getApplicationContext(), getStringLang(STLN_SMSMAYFAIL), Toast.LENGTH_LONG).show();
		}
	}};

	private void initStockSMSProgDlg(OnCancelListener listener)
	{
		if(mStockSMSDlg == null) {
			int id = 0;
			// form at center
			RelativeLayout rl = new RelativeLayout(this);
			_setPadding(rl,15,10,15,10);
			RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			rllp.addRule(RelativeLayout.CENTER_IN_PARENT);
			rl.setLayoutParams(rllp);
			GradientDrawable rlshape =  new GradientDrawable();
			rlshape.setCornerRadius(9);
			rlshape.setColor(Color.WHITE);
			rlshape.setStroke(2, WNDSTKCOLOR);
			setBkgndCompat(rl, rlshape);
			LinearLayout ell = new LinearLayout(this);
			ell.setId(++id);
			_setPadding(ell,5,5,5,5);
			RelativeLayout.LayoutParams elllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			elllp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			elllp.addRule(RelativeLayout.CENTER_VERTICAL);
			ell.setLayoutParams(elllp);
			ProgressBar pb = new ProgressBar(this, null, android.R.attr.progressBarStyle);
			LinearLayout.LayoutParams pblp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			_setMargins(pblp,0,0,20,0);
			pblp.gravity = Gravity.CENTER;
			pb.setLayoutParams(pblp);
			pb.setIndeterminate(true);
			pb.getIndeterminateDrawable().setColorFilter(BTNCOLOR, Mode.MULTIPLY);
			ell.addView(pb);
			TextView tvwait = new TextView(this);
			LinearLayout.LayoutParams tvwaitlp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			tvwaitlp.gravity = Gravity.CENTER;
			tvwait.setLayoutParams(tvwaitlp);
			String smsg = getStringLang(STLN_WAITREPSMS);
			tvwait.setText(smsg);
			tvwait.setTextColor(TEXT_COLOR);
			ell.addView(tvwait);
			rl.addView(ell);
			mStockSMSDlg = new Dialog(this);
			mStockSMSDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
			mStockSMSDlg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		    mStockSMSDlg.setContentView(rl, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		}
		mStockSMSDlg.setOnCancelListener(listener);
		mStockSMSDlg.show();
	}

	@Override protected void onStart() { super.onStart(); }
	@Override protected void onStop() { super.onStop(); }
	@Override protected void onResume() { super.onResume(); }
	@Override protected void onPause() { super.onPause(); }

	@Override protected void onDestroy()
	{
		super.onDestroy();
		dismissInboxCheck();
		mSendSMSStockAppHandler = null;
		if(Mimopay.mMi != null) {
			jprintf("size="+Integer.toString(mAlsResult.size()));
			int j = 4 - mAlsResult.size();
			for(int i=0;i<j;i++) {
				mAlsResult.add("");
			}
			mAlsResult.add("PaymentSubmitStage_"+Integer.toString(nBtnActionCnt));
			Mimopay.mMi.onReturn(mInfoResult, mAlsResult);
		}
	}

	private Bitmap getRoundedCornerBitmap(Bitmap bitmap, int bkgndcolor)
	{
		if(bitmap == null) {
			return null;
		}

		int w = bitmap.getWidth();
		int h = bitmap.getHeight();
		final int href = 70;
		if(h > href) {
			while(h > href) { w = ((w*9)/10); h = ((h*9)/10); }
		} else if(h < href) {
			while(h < href) { w = ((w*10)/9); h = ((h*10)/9); }
		}
		Bitmap bmp = Bitmap.createScaledBitmap(bitmap, w, h, true);
		bitmap = bmp;
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
		if(output == null) {
			return null;	// bisa jadi masalah memory
		}
		Canvas canvas = new Canvas(output);
		final int color = bkgndcolor;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPx = 4;
		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		return output;
	}

	private Bitmap getInverted(Bitmap src)
	{
		if(src == null) {
			return null;
		}
		Bitmap output = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());
		if(output == null) {
			return null; // bisa jadi krn masalah memory
		}
		int A, R, G, B;
		int pixelColor;
		int height = src.getHeight();
		int width = src.getWidth();
		
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				pixelColor = src.getPixel(x, y);
				A = Color.alpha(pixelColor);				
				R = 255 - Color.red(pixelColor);
				G = 255 - Color.green(pixelColor);
				B = 255 - Color.blue(pixelColor);
				output.setPixel(x, y, Color.argb(A, R, G, B));
			}
		}
		
		return output;
	}
}

