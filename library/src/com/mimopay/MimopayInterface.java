package com.mimopay;

import java.util.ArrayList;

public interface MimopayInterface
{
	// deprecaed since v2.5
	public void onReturn(String info, ArrayList<String> params);
	/*public void onSdkReturn(
		boolean bSuccess,		// transaction status
		String sPaymentMethod,	// smartfren, sevelin, atm_bca, atm_bersama, ...
		String sReason,			// UserCanceled, ErrorConnectiongToServer, ...
		String[] sExtras		// status & message from server, etc..
	);*/
}

