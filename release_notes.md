	 v1.0
			initial release
	
	 v1.1
			1. fix bug, upoint result not reported to onReturn
			2. avoid fix integer value on mAlsActiveIdx to avoid error
			3. centralized mimopay stuff, create new class MimopayStuff
			4. keep UnsupportedPaymentMethod, should update the document
			5. introduce new error, UnspecifiedChannelRequest
			6. fix unproportional UI with different device screen size
			7. custom radio buttons, works under froyo to jellybean
			8. add marquee effect on device that has small screen size
			9. allow wifi-only device keep continue with UPoint transaction
			10. new scenario on UPoint. allow user to SMS with other device
			11. add autosendsms flag to UPoint quiet mode
			12. add save last result
			13. disable my own debug log, provide method enableLogcat is someone
				wants to see it
	
	 v1.2
			1. remove 'msg' in topup onResultUI, which is actually unused
			2. fix bug when start enableGateway :(
			3. fix onReturn when _recreate
			4. fix onReturn of upoint & xl
			5. Now supoort XL, airtime and voucher
			6. introduce new error, ErrorHTTP404NotFound, error return 404
			7. problem solved, entities-base.properties dan entities-full.properties
			8. encrypted secretKey provided
	
	 v1.2.1
			1. fix bug when SDK running without storage (sdcard). Since it unable to download the logo
				then all logos replaced by text
	
	 v1.2.2
			1. add scrollable to all UI form
	
	 v1.2.3
			1. fix next button, when some denominations are overlap screen size
	
	 v1.2.4
			1. fix BadTokenException
			2. ATM working, but UI mode only
	
	 v1.2.5
			1. UI improved
			2. All words translated to bahasa
			3. Add description on top up channels, including XL voucher
	
	 v1.2.6
			1. ATMs (BCA & Bersama) Quiet Mode is now available
	
	 v1.2.7
			1. All logos will be shown, even without SDCARD
	
	 v1.2.8
			1. remove unused debug info
			2. fix bug on XL, for android honeycomb above
	
	 v1.2.9
			1. build-in UI improved.
			2. standardize onReturn's info, all in capital letter
	
	 v1.3.0
			1. fix minor bug
			2. In the sample, rename lib to libs.
			3. increase connection time-out
			4. remove retry
	
	 v1.3.1
			1. re-increase connection time-out, twice then before
	
	 v1.3.2
			1. last result saved to internal
	
	 v1.3.3
			1. atm bersama working now
	
	 v1.3.4
			1. logcat other important vars for troubleshooting purpose
			2. denom values, voucher codes, and phone number validation check
	
	 v2.0
			1. improve a lot of things: it looks (UI), process speed, and size
	
	 v2.1
			1. error handling on phone number
			2. normalize amount value
			3. document, highlight
	
	 v2.2
			1. on auto SMS send, LANJUT -> Kirim SMS
			2. support dual sim, by switch to stock message app, upoint & XL
			3. minor bug fix on XL airtime
			4. Last used phone number, user no need to re-type. It remember the last number type
			5. auto disable log when switch to gateway (production)
	
	 v2.3
			1. now suppor custom language, see CustomLang.java
			2. add mpoint airtime payment method (maxis)
			3. add dpoint airtime payment method (digi)
			3. several minor bug fixed
	
	 v2.4
			1. fix minor bug, on topup payment methods return
	
	 v2.5
			1. standardize all onReturn 'info' and 'params'. Please refer to the document
	
	 v2.6
			1. add new function: executeUPointAirtime(String amount)
			2. add new function: executeXLAirtime(String amount)
	
	 v2.7
			1. allow dev to get the http status code or java exception status
			2. bug fix on generating APIkey on the final URL
			3. add 2 functions for complete payment for Digi (DPoint)
			4. add filter on productName field to have safe URL
	
	 v2.8
			1. bug fixed, ErrorInvalidPhoneNumber raised because of the total digit input of phone number
			2. for digi, it allow to use country code however still should not use any non-numeric character such as '+' character
	
	 v2.8.1
			1. minor change on displaying fixed denom on airtime and atm
	
	 v2.9
			1. new payment method, upoint voucher
			2. new payment method, celcom airtime
			3. add fix amount for all airtime payment
			4. add atm also with fix amount
			5. bug fix, exclude 10% on all non-upoint payment methods
	
	 v2.9.1
			1. make sure it use https on production
	
	 v2.9.2
			1. add new function, executeUPointAirtime(String amount, String phoneNumber, boolean autosendsms, String upointItem)
	
	 v3.0
			1. add new payment method, Vietnam telco, VnTelco
			2. improve UI look
			3. add new error, ErrorChannelIsNotReady, issued when channelStatus = 0
	
	 v3.1
			1. improve UI look
			2. remove ErrorChannelIsNotReady, actually it is maintenance mode
	
	 v3.2
			1. re-improve UI look. all logo not used anymore
			2. improved speed also
			3. fix minor bug on vietnam telco payment method
	
	 v3.2.1
			1. fix minor bug on tsel upoint
	
	 v3.2.2
			1. fix dpoint on receiving sms shortcode
			2. add new payment method indosat airtime 
	
	 v3.2.3
			1. additional return info, at index-4, payment submit stage
			2. fix layout
			3. add denom amount. denomAmount just for show, but keep submit denomValue
			4. add disclaimer info from backend
			5. fix error message sentences. remove the extra technical info like: length=0 index=0
			6. fix bug on enableGateway on the first initialization
			7. dynamic UP. now can be updated on backend
